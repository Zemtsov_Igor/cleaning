export const services:any = [
  {
    name: 'Уборка квартир',
    link: '/uslugi/uborka-kvartir',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Генеральная уборка',
    link: '/uslugi/uborka-kvartir/generalnaya',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Поддерживающая уборка',
    link: '/uslugi/uborka-kvartir/podderzhivayushchaya',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Уборка после ремонта',
    link: '/uslugi/uborka-kvartir/posle-remonta',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Ежедневная уборка',
    link: '/uslugi/uborka-kvartir/ezhednevnaya',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Срочная уборка',
    link: '/uslugi/uborka-kvartir/srochnaya',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Уборка однокомнатной квартиры',
    link: '/uslugi/uborka-kvartir/odnokomnatnoj',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Уборка двухкомнатной квартиры',
    link: '/uslugi/uborka-kvartir/dvuhkomnatnoj',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Уборка трехкомнатной квартиры',
    link: '/uslugi/uborka-kvartir/trekhkomnatnoj',
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Уборка коттеджей',
    link: '/uslugi/uborka-kottedzhej',
    service_type: 'home_cleaning',
  },
  {
    name: 'Генеральная уборка',
    link: '/uslugi/uborka-kottedzhej/generalnaya',
    service_type: 'home_cleaning',
  },
  {
    name: 'Поддерживающая уборка',
    link: '/uslugi/uborka-kottedzhej/podderzhivayushchaya',
    service_type: 'home_cleaning',
  },
  {
    name: 'Уборка после ремонта',
    link: '/uslugi/uborka-kottedzhej/posle-remonta',
    service_type: 'home_cleaning',
  },
  {
    name: 'Срочная уборка',
    link: '/uslugi/uborka-kottedzhej/srochnaya',
    service_type: 'home_cleaning',
  },
  {
    name: 'Уборка домов',
    link: '/uslugi/uborka-kottedzhej/chastnyh-domov',
    service_type: 'home_cleaning',
  },
  {
    name: 'Уборка таунхаусов',
    link: '/uslugi/uborka-kottedzhej/taunhausov',
    service_type: 'home_cleaning',
  },
  {
    name: 'Уборка офисов',
    link: '/uslugi/uborka-ofisov',
    service_type: 'office_cleaning',
  },
  {
    name: 'Ежедневная уборка',
    link: '/uslugi/uborka-ofisov/ezhednevnaya',
    service_type: 'office_cleaning',
  },
  {
    name: 'Поддерживающая уборка',
    link: '/uslugi/uborka-ofisov/podderzhivayushchaya',
    service_type: 'office_cleaning',
  },
  {
    name: 'Генеральная уборка',
    link: '/uslugi/uborka-ofisov/generalnaya',
    service_type: 'office_cleaning',
  },
  {
    name: 'Уборка после ремонта',
    link: '/uslugi/uborka-ofisov/posle-remonta',
    service_type: 'office_cleaning',
  },
  {
    name: 'Химчистка',
    link: '/uslugi/himchistka',
    service_type: 'dry_cleaning',
  },
  {
    name: 'Химчистка с выездом',
    link: '/uslugi/himchistka/s-vyezdom',
    service_type: 'dry_cleaning',
  },
  {
    name: 'Химчистка мягкой мебели',
    link: '/uslugi/himchistka/myagkoj-mebeli',
    service_type: 'dry_cleaning',
  },
  {
    name: 'Химчистка штор',
    link: '/uslugi/himchistka/shtor',
    service_type: 'dry_cleaning',
  },
  {
    name: 'Химчистка ковров',
    link: '/uslugi/himchistka/kovrov',
    service_type: 'dry_cleaning',
  },
  {
    name: 'Мойка окон',
    link: '/uslugi/moyka-okon',
    service_type: 'window_cleaning',
  },
  {
    name: 'Мойка витрин',
    link: '/uslugi/moyka-okon/vitrin',
    service_type: 'window_cleaning',
  },
  {
    name: 'Мойка фасадов',
    link: '/uslugi/moyka-okon/fasadov',
    service_type: 'window_cleaning',
  },
  {
    name: 'Мойка окон альпинистами',
    link: '/uslugi/moyka-okon/alpinistami',
    service_type: 'window_cleaning',
  },
];

export const otherServicesService = (service_type:string, link:string) => {
  const otherServices = services.filter((service:any) => service.service_type === service_type).filter((service:any) => service.link !== link);
  return otherServices;
};
