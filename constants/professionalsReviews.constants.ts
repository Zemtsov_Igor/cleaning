import { customerReviews } from './customerReviews.constants';
import { shuffle } from '../utils/shuffle';

export const professionalsReviews:any = [
  {
    name: 'Юлия Б.',
    avatar: {
      src: '/static/images/professionals/1.webp',
      alt: 'Юлия Б. Клинер'
    },
    rating: 100,
    premium: true,
    jobsCompleted: 324,
    service_type: 'office_cleaning',
  },
  {
    name: 'Ольга А.',
    avatar: {
      src: '/static/images/professionals/2.webp',
      alt: 'Ольга А. Клинер'
    },
    rating: 94,
    premium: false,
    jobsCompleted: 78,
    service_type: 'office_cleaning',
  },
  {
    name: 'Анастасия К.',
    avatar: {
      src: '/static/images/professionals/3.webp',
      alt: 'Анастасия К. Клинер'
    },
    rating: 95,
    premium: true,
    jobsCompleted: 156,
    service_type: 'office_cleaning',
  },
  {
    name: 'Дмитрий Б.',
    avatar: {
      src: '/static/images/professionals/4.webp',
      alt: 'Дмитрий Б. Клинер'
    },
    rating: 89,
    premium: false,
    jobsCompleted: 183,
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Олег К.',
    avatar: {
      src: '/static/images/professionals/5.webp',
      alt: 'Олег К. Клинер'
    },
    rating: 97,
    premium: true,
    jobsCompleted: 273,
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Мария О.',
    avatar: {
      src: '/static/images/professionals/6.webp',
      alt: 'Мария О. Клинер'
    },
    rating: 97,
    premium: false,
    jobsCompleted: 98,
    service_type: 'apartment_cleaning',
  },
  {
    name: 'Марина Н.',
    avatar: {
      src: '/static/images/professionals/7.webp',
      alt: 'Марина Н. Клинер'
    },
    rating: 100,
    premium: true,
    jobsCompleted: 294,
    service_type: 'home_cleaning',
  },
  {
    name: 'Елена З.',
    avatar: {
      src: '/static/images/professionals/8.webp',
      alt: 'Елена З. Клинер'
    },
    rating: 86,
    premium: false,
    jobsCompleted: 183,
    service_type: 'home_cleaning',
  },
  {
    name: 'Оксана Г.',
    avatar: {
      src: '/static/images/professionals/9.webp',
      alt: 'Оксана Г. Клинер'
    },
    rating: 98,
    premium: true,
    jobsCompleted: 391,
    service_type: 'home_cleaning',
  },
  {
    name: 'Кристина Е.',
    avatar: {
      src: '/static/images/professionals/10.webp',
      alt: 'Кристина Е. Клинер'
    },
    rating: 84,
    premium: false,
    jobsCompleted: 193,
    service_type: 'window_cleaning',
  },
  {
    name: 'Ольга Б.',
    avatar: {
      src: '/static/images/professionals/11.webp',
      alt: 'Ольга Б. Клинер'
    },
    rating: 100,
    premium: true,
    jobsCompleted: 293,
    service_type: 'window_cleaning',
  },
  {
    name: 'Николай Р.',
    avatar: {
      src: '/static/images/professionals/12.webp',
      alt: 'Николай Р. Клинер'
    },
    rating: 94,
    premium: false,
    jobsCompleted: 183,
    service_type: 'window_cleaning',
  },
  {
    name: 'Никита О.',
    avatar: {
      src: '/static/images/professionals/13.webp',
      alt: 'Никита О. Клинер'
    },
    rating: 92,
    premium: true,
    jobsCompleted: 283,
    service_type: 'dry_cleaning',
  },
  {
    name: 'Александр В.',
    avatar: {
      src: '/static/images/professionals/14.webp',
      alt: 'Александр В. Клинер'
    },
    rating: 97,
    premium: true,
    jobsCompleted: 381,
    service_type: 'dry_cleaning',
  },
  {
    name: 'Илья Б.',
    avatar: {
      src: '/static/images/professionals/15.webp',
      alt: 'Илья Б. Клинер'
    },
    rating: 86,
    premium: false,
    jobsCompleted: 293,
    service_type: 'dry_cleaning',
  },
];

export const professionalsReviewsService = (service_type:any) => {
  const comments = shuffle(customerReviews.filter((review:any) => review.service_type === service_type)).slice(0, 3);
  const reviews = professionalsReviews.filter((review:any) => review.service_type === service_type).map((review:any) => {
    review.comment = comments[Math.floor(Math.random() * comments.length)].comment;
    return review;
  });

  return reviews;
};