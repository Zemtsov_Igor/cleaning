export const apartmentTypes:any = [
  { hours: '2 часа', alias:'Студия', name: 'Студия' },
  { hours: '3 часа', alias:'1 комната', name: '1 комната' },
  { hours: '4 часа', alias:'2 комнаты', name: '2 комнаты'},
  { hours: '5 часов', alias:'3 комнаты', name: '3 комнаты'},
  { hours: '6 часов', alias:'4 комнаты', name: '4 комнаты'},
  { hours: '7 часов', alias:'5 комнат', name: '5 комнат'}
];

export const maxApartmentNumber:number = 5;
export const apartmentTypesStr:string = JSON.stringify(apartmentTypes);
