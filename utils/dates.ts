import moment from 'moment';
import 'moment-timezone';
import 'moment/locale/ru';

export const getDateStart = () => {
  const now = moment();
  return now.format('D MMMM, YYYY');
}

export const getCreatedAt = () => {
  const days:any = [-3, -2, -1, 0];
  const day:any = days[Math.floor(Math.random()*days.length)];
  const now = moment().add(day, 'd');
  return now.format('D MMMM, YYYY');
}