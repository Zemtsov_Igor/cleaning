import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { ErrorComponent } from '../styles/errorComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

const data:any = {
  settings: {
    title: 'Страница не найдена my-cleaning | Уборка домов, квартир, офисов в Москве',
    description: 'Страница не найдена Уборка квартир в Москве — цены на клининговые услуги My-cleaning. Работаем без выходных. Наш телефон в Москве: +7 (495) 885-72-79.',
    keywords: null,
    bgImage: '/static/images/bg/404.webp',
    name: 'Страница не найдена',
  }
}

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <ErrorComponent>
          <div className="background">
            <div className="content__container">
              <div className="content__container-header">Ой, похоже мы что-то упустили!</div>
              <div className="content__container-subtitle">Почему бы вам не посетить нашу <a href="/">главную страницу</a>, пока мы тут все почистим?</div>
            </div>
          </div>
        </ErrorComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
