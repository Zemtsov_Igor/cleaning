import React, { Component } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { GuaranteeComponent } from '../styles/guaranteeComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>Гарантия счастья</title>
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <GuaranteeComponent>
          <div className="our-guarantee -guarantee">
            <div className="our-guarantee-hero flex-rows">
              <div className="our-guarantee-hero-box flex-rows centered">
                <h1>Удобный Гарантия счастья</h1>
                <div className="subtitle">Ваше счастье - наша цель. Если вы недовольны, мы постараемся исправить это.</div>
              </div>
            </div>

            <div className="our-guarantee-pro section flex-rows centered">
              <div className="our-guarantee-pro-container text-block">
                <div className="title">Ваш опыт имеет значение</div>
                <div className="subtitle">
                  Handy всегда стремится подобрать для вас подходящего профессионала для вас и вашего дома. Если вы не удовлетворены качеством услуги, которую вы забронировали и оплатили непосредственно на платформе Handy, мы вышлем еще одного профессионала без дополнительной оплаты.
                  для вашего следующего бронирования.
                </div>
              </div>
            </div>

            <div className="our-guarantee-pro-review section flex-rows-reverse to-cols axis-start cross-centered">
              <div className="pro-review-box-small hide-small"></div>
              <div className="hide-small">
                <div className="pro-review-box flex-rows axis-start cross-centered">
                  <Amp.AmpImg
                    specName="default"
                    key={Buffer.from(Math.random().toString()).toString('base64')}
                    src='/static/images/pro-pic-first.png'
                    className="trust-icon img"
                    layout="flex-item"
                    alt='Pro pic first'
                  />
                  <div className="pro-name">Дарья К.</div>
                  <div className="pro-city">г. Москва</div>
                  <div className="pro-stars flex-cols centered">
                    <div className="rating-stars-display flex-cols cross-centered" data-rating="4.5">
                      <div className="star">
                        <div className="fa fa-star"></div>
                      </div>
                      <div className="star">
                        <div className="fa fa-star"></div>
                      </div>
                      <div className="star">
                        <div className="fa fa-star"></div>
                      </div>
                      <div className="star">
                        <div className="fa fa-star"></div>
                      </div>
                      <div className="star">
                        <div className="fa fa-star fa-star-o star-under"></div>
                        <div className="fa fa-star star-over" style={{ width: '50.0%' }}></div>
                      </div>
                    </div>
                    <div className="pro-rating-number">4.5</div>
                  </div>

                  <div className="customer-review-line"></div>
                  <div className="customer-review">Любимый отзыв клиентов:</div>
                  <div className="customer-review-statement">Я был так удивлен, когда вернулся домой с милым лебединым полотенцем на моей кровати! Люблю внимание к деталям.</div>
                </div>
              </div>

              <div className="pro-review-box flex-rows axis-start cross-centered">
                <Amp.AmpImg
                  specName="default"
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  src='/static/images/pro-pic-second.png'
                  className="trust-icon img"
                  layout="flex-item"
                  alt='Pro pic second'
                />
                <div className="pro-name">Ирина Г.</div>
                <div className="pro-city">г. Москва</div>
                <div className="pro-stars flex-cols centered">
                  <div className="rating-stars-display flex-cols cross-centered" data-rating="5">
                    <div className="star">
                      <div className="fa fa-star"></div>
                    </div>
                    <div className="star">
                      <div className="fa fa-star"></div>
                    </div>
                    <div className="star">
                      <div className="fa fa-star"></div>
                    </div>
                    <div className="star">
                      <div className="fa fa-star"></div>
                    </div>
                    <div className="star">
                      <div className="fa fa-star fa-star-o star-under"></div>
                      <div className="fa fa-star star-over" style={{ width: '100%' }}></div>
                    </div>
                  </div>
                  <div className="pro-rating-number">5</div>
                </div>
                <div className="customer-review-line"></div>
                <div className="customer-review">Любимый отзыв клиентов:</div>
                <div className="customer-review-statement">Спасибо вам за все, что вы для нас делаете. Мы очень ценим ваши таланты уборщика и организационное волшебство.</div>
              </div>

              <div className="description text-block">
                <div className="title bold">Профессионалы, которых вы хотите</div>
                <div className="subtitle">
                  Услуги по уборке и разнорабочему, заказанные и оплаченные непосредственно через платформу Handy, выполняются <a href="/trust-and-safety">проверенные профессионалы</a> которых высоко оценивают такие клиенты, как вы. А для уборки ваш любимый профессионал может возвращаться снова и снова, чтобы убирать ваш дом так, как вам нравится.
                </div>
              </div>
            </div>

            <div className="our-guarantee-pros-rectangle section flex-rows centered">
              <div className="our-guarantee-container-pros text-block">
                <div className="title">Мы вас прикрыли</div>
                <div className="subtitle">
                  В редких случаях повреждения, Хэнди всегда к вашим услугам. Заказы, сделанные и оплаченные непосредственно на платформе Handy, застрахованы. <a target="_blank" href="/terms#happiness-guarantee">Узнать больше</a>
                </div>
              </div>
            </div>

            <div className="our-guarantee-contact-us flex-rows centered section">
              <div className="title">Если вы недовольны, просто сообщите нам, и мы постараемся исправить это.</div>
              <a target="_blank" className="btn big-button btn-invert-brand-color" href="/kontakty">Связаться с нами</a>
            </div>

            <div className="our-guarantee-ready-to-book flex-rows centered section">
              <div className="title">Готовы забронировать?</div>
              <div className="flex-rows to-cols">
                <a className="btn big-button btn-secondary call-to-action" href="/uslugi/uborka-kvartir">Закажите уборщицу</a>
                <a className="btn big-button btn-invert-brand-color call-to-action" href="/uslugi">Все услуги</a>
              </div>
            </div>

          </div>
        </GuaranteeComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
