import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { HelpComponent } from '../styles/helpComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>Уборка и уборка дома Услуги разнорабочего | Удобный</title>
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <HelpComponent>

        </HelpComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
