import React, { Component } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { PageComponent } from '../styles/pageComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

interface AmpIframeNewProps extends Amp.AmpIframeProps {
  width?: any
  height?: any
  layout?: any
};

function AmpIframeNew(props: AmpIframeNewProps) {
  return (
    <Amp.AmpIframe {...props} />
  )
};

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>Контакты в Москве</title>
          <meta name="description" content="Уборка - от компании «my-cleaning». Закажите профессиональную уборку по доступной цене! Наши контактные данные.  Работаем без выходных. Наш телефон +7 (495) 885-72-79" />
          <link rel="amphtml" href={`https://amp.my-cleaning.ru/kontakty`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <PageComponent>
          <div className="quote-bg">
            <div className="grid-container">
              <h1>Контакты в Москве</h1>
            </div>
          </div>

          <div className="blank-background -contacts">
            <div className="grid-container">
              <div className="grid-x align-center">

                <div className="grid-x cell small-12 medium-10">

                  <div className="cell small-12 medium-6">

                    <div className="contacts-col-item">
                      <span className="contacts-col-item__text">Email</span>
                      <div className="contacts-col-item__wrap">
                        <a href="mailto:feedback@my-cleaning.ru" className="contacts-col-item__link">feedback@my-cleaning.ru</a>
                      </div>
                    </div>

                    <div className="contacts-col-item">
                      <span className="contacts-col-item__text">Телефон</span>
                      <div className="contacts-col-item__wrap">
                        <a href="tel:+74958857279" className="contacts-col-item__link">+7 (495) 885-72-79</a>
                      </div>
                    </div>

                  </div>

                  <div className="cell small-12 xlarge-6">

                    <div className="contacts-col-item">
                      <span className="contacts-col-item__text">Email для сотрудничества</span>
                      <div className="contacts-col-item__wrap">
                        <a href="mailto:feedback@my-cleaning.ru" className="contacts-col-item__link">feedback@my-cleaning.ru</a>
                      </div>
                    </div>

                    <div className="contacts-col-item">
                      <span className="contacts-col-item__text">Адрес</span>
                      <div className="contacts-col-item__wrap">
                        <span className="contacts-col-item__link">121108, г. Москва, Ул. Минская д.1 Г, корп. 2, офис 29</span>
                      </div>
                    </div>

                  </div>

                  <div className="cell small-12">
                    <div className="map">
                      <AmpIframeNew
                        width="600"
                        height="400"
                        layout="responsive"
                        sandbox="allow-scripts allow-same-origin allow-popups"
                        frameborder="0"
                        src="https://yandex.ru/map-widget/v1/?z=12&ol=biz&oid=99795559837"
                      />
                    </div>
                  </div>

                </div>

              </div>
            </div>
          </div>

        </PageComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
