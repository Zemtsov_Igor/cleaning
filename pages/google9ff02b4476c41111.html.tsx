import React from 'react';

const getGoogle = () => `google-site-verification: google9ff02b4476c41111.html`;

class Sitemap extends React.Component {
  public static async getInitialProps({res}:any) {
    res.setHeader('Content-Type', 'text/plain');
    res.write(getGoogle());
    res.end();
  }
}

export default Sitemap;