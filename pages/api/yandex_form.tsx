import Mailgun from 'mailgun.js';
const FormData = require('form-data');

const USERNAME = 'api';
const DOMAIN = 'my-cleaning.ru';
const API_KEY = 'f30dc7df8c81762da68119809c40a224-e49cc42c-79ca591c';

const mailgun = new Mailgun(FormData);

const mg = mailgun.client({username: USERNAME, key: API_KEY});

export default async function handler(req:any, res:any) {
  if (req.method === 'POST') {
    const formData = {
      name: req.body.name,
      phone: req.body.phone,
    };

    const messData = {
      from: 'MyCleaning <mail@my-cleaning.ru>',
      to: ['zemtsovigor@yahoo.com'],
      subject: 'Заявка турбо страницы my-cleaning.ru',
      html: `<h1>Здравствуйте, мы получили заявку!</h1><br><h3>Имя: ${formData.name}</h3><br><h3>телефон: ${formData.phone}</h3>`,
    };

    mg.messages.create(DOMAIN, messData)
      .then((msg: any) => {
        console.log('msg', msg);
        res.statusCode = 200;
        res.end(JSON.stringify(
          [
            {
              "field": "description",
              "value": "Ваша заявка успешно отправлена!"
            }
          ]
        ));
      })
      .catch((err: any) => {
        console.log('err', err);
        res.statusCode = err.status;
        res.end(JSON.stringify(
          [
            {
              "field": "description",
              "value": "При отправки вашей заявки произошла ошибка!"
            }
          ]
        ));
      });
  } else {
    res.statusCode = 200;
    res.end();
  }
}
