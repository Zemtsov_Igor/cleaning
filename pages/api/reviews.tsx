import { customerReviews } from '../../constants/customerReviews.constants';
import { shuffle } from '../../utils/shuffle';

export default async function get(req:any, res:any) {
  const reqParams:any = {
    page: req.query.page || 1,
    length: req.query.length || 5,
    limit: req.query.limit || customerReviews.length,
    service_type: req.query.service_type
  };

  const items = customerReviews.filter((review:any) => review.service_type === reqParams.service_type).slice(0, reqParams.limit).slice((reqParams.page - 1) * reqParams.length, reqParams.page * reqParams.length);

  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify({
    items: shuffle(items)
  }))
}