import React from 'react';

const getYandex = () => `<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>Verification: b8fcdda118121a2c</body>
</html>`;

class Sitemap extends React.Component {
  public static async getInitialProps({res}:any) {
    res.setHeader('Content-Type', 'text/html');
    res.write(getYandex());
    res.end();
  }
}

export default Sitemap;