import React from 'react';

const getRobots = () => `User-agent: *
Sitemap: https://my-cleaning.ru/sitemap.xml
`;

class Sitemap extends React.Component {
  public static async getInitialProps({res}:any) {
    res.setHeader('Content-Type', 'text/plain');
    res.write(getRobots());
    res.end();
  }
}

export default Sitemap;