import React from 'react';
import {uborkaKvartir} from './uslugi/uborka-kvartir';
import {uborkaKvartirGeneralnaya} from './uslugi/uborka-kvartir/generalnaya';
import {uborkaKvartirPodderzhivayushchaya} from "./uslugi/uborka-kvartir/podderzhivayushchaya";
import {uborkaKvartirPosleRemonta} from "./uslugi/uborka-kvartir/posle-remonta";
import {uborkaKvartirEzhednevnaya} from "./uslugi/uborka-kvartir/ezhednevnaya";
import {uborkaKvartirSrochnaya} from "./uslugi/uborka-kvartir/srochnaya";
import {home} from "./uslugi/uborka-kottedzhej";

import {himchistka} from "./uslugi/himchistka";

import {uborkaKvartirOdnokomnatnoj} from "./uslugi/uborka-kvartir/odnokomnatnoj";
import {uborkaKvartirDvuhkomnatnoj} from "./uslugi/uborka-kvartir/dvuhkomnatnoj";
import {uborkaKvartirTrekhkomnatnoj} from "./uslugi/uborka-kvartir/trekhkomnatnoj";
import {himchistkaSVyezdom} from "./uslugi/himchistka/s-vyezdom";
import {himchistkaMyagkojMebeli} from "./uslugi/himchistka/myagkoj-mebeli";
import {himchistkaShtor} from "./uslugi/himchistka/shtor";
import {himchistkaKovrov} from "./uslugi/himchistka/kovrov";
import {homeGeneralnaya} from "./uslugi/uborka-kottedzhej/generalnaya";
import {homePodderzhivayushchaya} from "./uslugi/uborka-kottedzhej/podderzhivayushchaya";
import {homePosleRemonta} from "./uslugi/uborka-kottedzhej/posle-remonta";
import {homeSrochnaya} from "./uslugi/uborka-kottedzhej/srochnaya";
import {homeChastnyhDomov} from "./uslugi/uborka-kottedzhej/chastnyh-domov";
import {homeTaunhausov} from "./uslugi/uborka-kottedzhej/taunhausov";
import {ofisov} from "./uslugi/uborka-ofisov";
import {ofisovEzhednevnaya} from "./uslugi/uborka-ofisov/ezhednevnaya";
import {ofisovPodderzhivayushchaya} from "./uslugi/uborka-ofisov/podderzhivayushchaya";
import {ofisovGeneralnaya} from "./uslugi/uborka-ofisov/generalnaya";
import {ofisovPosleRemonta} from "./uslugi/uborka-ofisov/posle-remonta";
import {moykaOkon} from "./uslugi/moyka-okon";
import {moykaOkonVitrin} from "./uslugi/moyka-okon/vitrin";
import {moykaOkonAlpinistami} from "./uslugi/moyka-okon/alpinistami";
import {moykaOkonFasadov} from "./uslugi/moyka-okon/fasadov";
import {dezinfekciya} from "./uslugi/dezinfekciya";
import {uborkaPomeshchenij} from "./uslugi/uborka-pomeshchenij";

const getTurboRss = () => `<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" xmlns:turbo="http://turbo.yandex.ru" version="2.0">
  <channel>
    <title>Клинговая компания my-cleaning | Уборка домов, квартир, офисов в Москве</title>
    <link>https://my-cleaning.ru/</link>
    <description>Уборка квартир в Москве — цены на клининговые услуги My-cleaning. Работаем без выходных. Наш телефон в Москве: +7 (495) 885-72-79.</description>
    <language>ru</language>
    <turbo:analytics type="Yandex" id="79309012"></turbo:analytics>
    <item turbo="true">
      <turbo:extendedHtml>true</turbo:extendedHtml>
      <link>https://my-cleaning.ru/</link>
      <turbo:source>https://my-cleaning.ru/</turbo:source>
      <title>Клинговая компания my-cleaning | Уборка домов, квартир, офисов в Москве</title>
      <turbo:topic>Уборка квартир в Москве — цены на клининговые услуги My-cleaning. Работаем без выходных. Наш телефон в Москве: +7 (495) 885-72-79.</turbo:topic>
      <pubDate>Thu, 21 Jul 2021 14:34:50 +0300</pubDate>
      <author>My Cleaning</author>
      <turbo:content>
        <![CDATA[
          <header>
            <h1 class="main_title">Закажите профессиональную уборку в Москве</h1>
          </header>
          <main role="main" class="page-frame">
            <div class="home-page">
              <section class="grid-container">
                <div class="grid-x">
                  <div class="cell home__title">Закажите профессиональную уборку в Москве</div>
                </div>
              </section>
              <section class="section header-buttons">
                <div class="grid-container show-for-small full">
                  <div class="grid-x">
                    <div class="cell nav-button hover-cleaner-green">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/uborka-kvartir"></a>
                      <div class="spray-icon"></div>
                      <div class="button-title">Уборка квартир</div>
                      <i class="far fa-angle-right button-arrow"></i>
                    </div>
                    <div class="cell nav-button hover-handyman-teal">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/uborka-kottedzhej"></a>
                      <div class="wrench-icon"></div>
                      <div class="button-title">Уборка коттеджей</div>
                      <i class="far fa-angle-right button-arrow"></i>
                    </div>
                    <div class="cell nav-button hover-handyman-teals">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/uborka-ofisov"></a>
                      <div class="wrenchs-icon"></div>
                      <div class="button-title">Уборка офисов</div>
                      <i class="far fa-angle-right button-arrow"></i>
                    </div>
                    <div class="cell nav-button hover-perfect-blue">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/himchistka"></a>
                      <div class="shop-icon"></div>
                      <div class="button-title">Химчистка</div>
                      <i class="far fa-angle-right button-arrow"></i>
                    </div>
                  </div>
                </div>
                <div class="grid-container hide-for-small">
                  <div class="grid-x grid-margin-x">
                    <div class="cell medium-3 nav-button hover-cleaner-green">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/uborka-kvartir"></a>
                      <div class="spray-icon"></div>
                      <div class="button-title">Уборка квартир</div>
                    </div>
                    <div class="cell medium-3 nav-button hover-cleaner-teal">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/uborka-kottedzhej"></a>
                      <div class="wrench-icon"></div>
                      <div class="button-title">Уборка коттеджей</div>
                    </div>
                    <div class="cell medium-3 nav-button hover-cleaner-teals">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/uborka-ofisov"></a>
                      <div class="wrenchs-icon"></div>
                      <div class="button-title">Уборка офисов</div>
                    </div>
                    <div class="cell medium-3 nav-button hover-cleaner-blue">
                      <a class="card-link" href="https://my-cleaning.ru/uslugi/himchistka"></a>
                      <div class="shop-icon"></div>
                      <div class="button-title">Химчистка</div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="section header-form">
                <div class="grid-container">
                  <div class="grid-x">
                    <div class="cell small-12">
                      <div class="cell header-form-title">
                        <h2>Уборка квартиры</h2>
                        <p class="p">Закажите безопасную уборку квартиры в Москве</p>
                      </div>
                      <div class="cell grid-x">
                        <div class="form-container small-12">
                          <form
                            data-type="dynamic-form"
                            end_point="https://my-cleaning.ru/api/yandex_form"
                          >
                            <div type="result-block">
                              <span
                                type="text"
                                class="small-12 columns when-page-errors"
                                field="description"
                              ></span>
                            </div>
                            <div type="input-block">
                              <span
                                type="input"
                                name="name"
                                label="Имя"
                                input-type="text"
                                placeholder="Имя"
                                required="true"
                              ></span>
                              
                              <span
                                type="select"
                                name="type"
                                label="Тип"
                                value="studija"
                              >
                                <span
                                  type="option"
                                  value="studija"
                                  text="Студия">
                                </span>
                                <span
                                  type="option"
                                  value="1komnata"
                                  text="1 комната">
                                </span>
                                <span
                                  type="option"
                                  value="2komnaty"
                                  text="2 комнаты">
                                </span>
                                <span
                                  type="option"
                                  value="3komnaty"
                                  text="3 комнаты">
                                </span>
                                <span
                                  type="option"
                                  value="4komnaty"
                                  text="4 комнаты">
                                </span>
                                <span
                                  type="option"
                                  value="5komnat"
                                  text="5 комнат">
                                </span>
                              </span>
                              <span
                                type="input"
                                name="phone"
                                label="Телефон"
                                input-type="text"
                                placeholder="+7 (999) 999-99-99"
                                required="true"
                              ></span>
                              
                              <button
                                type="submit"
                                text="Рассчитать стоимость"
                              ></button>
                            </div>
                          </form>
                          <div class="cell term_description">
                            <p>Нажимая «Рассчитать стоимость», я даю <a href="https://my-cleaning.ru/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="https://my-cleaning.ru/terms" target="_blank">условия использования сайта</a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="section section-empty">
                <div class="grid-container">
                  <div class="align-center trusted-wrap">
                    <div class="grid-x align-center trusted-container">
                      <div class="cell text-center small-12 large-10 trusted-name">
                        <h2>Преимущества заказа услуг клининга в нашей компании</h2>
                        <h3 class="section-sub-header">Почему заказывают уборку в нашей компании?</h3>
                      </div>
                      <div class="cell grid-x grid-margin-x grid-padding-y small-12 trusted-capabilities">
                        <div class="cell capability small-12 medium-6">
                          <div class="capability__title">
                            <span class="icon">
                              <img src="https://my-cleaning.ru/static/images/experienced_icon.svg" alt="Опыт" class="img" />
                            </span>
                            <span class="text">Опыт</span>
                          </div>
                          <div class="capability__description">
                            <p>
                              С 2012 года наша компания предоставляет людям услуги по профессиональной уборке помещений. За свою историю профессионалы по уборке помогли очистить миллионы домов и квартир в Москве. На нашем сайте вы можете
                              оставить заявку на уборку квартиры, дома и других помещений.
                            </p>
                          </div>
                        </div>
                        <div class="cell capability small-12 medium-6">
                          <div class="capability__title">
                            <span class="icon">
                              <img src="https://my-cleaning.ru/static/images/reliable_icon.svg" alt="Надежность" class="img" />
                            </span>
                            <span class="text">Надежность</span>
                          </div>
                          <div class="capability__description">
                            <p>
                              Когда вы заказываете услуги уборки на нашем сайте, вы можете не сомневаться в том, что услуги будут оказаны в необходимое для Вас время. Мы поможем вернуть вашу спальню, ванную, кухню, гостиную и другие помещения
                              в их первоначальное состояние.
                            </p>
                          </div>
                        </div>
                        <div class="cell capability small-12 medium-6">
                          <div class="capability__title">
                            <span class="icon">
                              <img src="https://my-cleaning.ru/static/images/convenient_icon.svg" alt="Удобство" class="img" />
                            </span>
                            <span class="text">Удобство</span>
                          </div>
                          <div class="capability__description">
                            <p>
                              Работа допоздна мешает вам поддерживать порядок в квартире? Вы слишком заняты тем, что водите детей в школу, на тренировки? Мы знаем, что жизнь может быть сумасшедшей и непредсказуемой, и когда вы устали и
                              перегружены работой, последнее, что кто-то хочет делать, это убирать свой дом. Какой бы ни была причина, по которой вы ищете помощь по уборке дома или квартиры мы поможем вам.
                            </p>
                          </div>
                        </div>
                        <div class="cell capability small-12 medium-6">
                          <div class="capability__title">
                            <span class="icon">
                              <img src="https://my-cleaning.ru/static/images/flexible_icon.svg" alt="Гибкость" class="img" />
                            </span>
                            <span class="text">Гибкость</span>
                          </div>
                          <div class="capability__description">
                            <p>
                              На нашем сайте легко планировать и переносить бронирования уборок. Просто откройте форму записи и выберите наиболее удобные для вас дату и время. Или, может быть, вашему дому нужна генеральная уборка - вы можете
                              использовать приложение, чтобы добавить к бронированию дополнительные услуги, такие как стирка, уборка в шкафах, в холодильнике и т. д. Сделайте нашу команду вашим помощником номер один по уборке квартиры, дома
                              или офиса.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="row">
                <div class="grid-container">
                  <div class="grid-x how-it-works">
                    <div class="cell small-12 medium-12 text-center">
                      <h2><span>Закажите уборку квартиры в Москве</span></h2>
                    </div>
                    <div class="cell grid-x how-it-works__media-cards-container">
                      <div class="cell grid-x small-12 large-4 align-center text-center media-card">
                        <div class="cell">
                          <img src="https://my-cleaning.ru/static/images/setup_plan_icon.svg" alt="Индивидуальный план клининговой уборки квартиры" class="img" />
                        </div>
                        <div class="cell small-10">
                          <h3>Индивидуальный план клининговой уборки квартиры</h3>
                          <p>
                            Будние дни или выходные, утро, день или вечер — вы составляете график влажной уборки, ориентируясь в первую очередь на себя. Также вы можете пропускать или переносить день удобки. Во избежание штрафов рекомендуем
                            сообщать не позднее, чем за сутки.
                          </p>
                        </div>
                      </div>
                      <div class="cell grid-x small-12 large-4 align-center text-center media-card">
                        <div class="cell">
                          <img src="https://my-cleaning.ru/static/images/manage_online_icon.svg" alt="Проверка процесса влажной уборки квартиры с мобильного девайса" class="img" />
                        </div>
                        <div class="cell small-10">
                          <h3>Проверка процесса влажной уборки квартиры с мобильного девайса</h3>
                          <p>Если вам неудобно присутствовать при клининговой уборке лично, рекомендуем воспользоваться услугой онлайн-проверки. Используйте для этого телефон, планшет или ноутбук — что вам будет удобно.</p>
                        </div>
                      </div>
                      <div class="cell grid-x small-12 large-4 align-center text-center media-card">
                        <div class="cell">
                          <img src="https://my-cleaning.ru/static/images/pro_female_icon.svg" alt="Время на себя и близких" class="img" />
                        </div>
                        <div class="cell small-10">
                          <h3>Время на себя и близких</h3>
                          <p>
                            Самостоятельная уборка квартиры требует много сил и времени, а клининг освободить его. Заказ профессиональной влажной уборки в нашей клининговой компании — отличная возможность вспомнить старое хобби, освоить новое
                            или провести время со своими детьми.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="section">
                <div class="grid-container">
                  <div class="grid-x align-center">
                    <div class="grid-x align-center small-12 medium-10 faq">
                      <div class="cell text-center faq__title"><h3>Часто задаваемые вопросы</h3></div>
                      <div data-block="accordion">
                        <div data-block="item" data-title="Какой клинер приедет ко мне?">
                           <p>Мы располагаем обширной сетью опытных уборщиков с самым высоким рейтингом. В зависимости от времени и даты вашего бронирования услуг по уборке мы выберем лучшего клинера.</p>
                        </div>
                        <div data-block="item" data-title="Могу ли я пропустить или перенести бронирование?">
                           <p>Вы можете перенести любое бронирование без штрафных санкций, если сделаете это как минимум на 24 часа раньше запланированного времени начала.</p>
                        </div>
                        <div data-block="item" data-title="Сколько стоят услуги по уборке дома">
                           <p>Стоимость услуг по уборке дома зависит от площади - укажите, сколько у вас спален и ванных комнат, в форму выше, и мы сразу же предоставим вам расчет стоимости. Мы уверены, что вы найдете цены чрезвычайно разумными, учитывая высокий уровень услуги клининга в нашей компании.</p>
                        </div>
                        <div data-block="item" data-title="Включает ли уборка стирка?">
                           <p>Если у вас дома есть стиральная машина и / или сушилка, ваш клинер сможет выстирать и сложить белье за вас! Обязательно добавьте указание об этой услуге в дополнения во время оформления заказа, чтобы клинер мог рассчитать правильно время для уборки и стирки (это добавит дополнительный час к вашему бронированию).</p>
                        </div>
                        <div data-block="item" data-title="Сколько времени должна длиться уборка дома?">
                           <p>Время по уборке полностью зависит от площади вашего дома! Минимальная продолжительность бронирования составляет 3 часа, но вы всегда можете запросить более длительное время, если это необходимо. В зависимости от количества спален и ванных комнат, при оформлении заказа определяется оптимальное время на уборку в зависимости от площади вашего дома или квартиры.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="section footer-form">
                <div class="grid-container">
                  <div class="grid-x">
                    <div class="cell small-12">
                      <div class="cell header-form-title"><h2>Заказать уборку квартиры в Москве</h2></div>
                      <div class="cell grid-x">
                        <div class="form-footer-container small-12">
                        
                          <form
                            data-type="dynamic-form"
                            end_point="https://my-cleaning.ru/api/yandex_form"
                          >
                            <div type="result-block">
                              <span
                                type="text"
                                class="small-12 columns when-page-errors"
                                field="description"
                              ></span>
                            </div>
                            <div type="input-block">
                              <span
                                type="input"
                                name="name"
                                label="Имя"
                                input-type="text"
                                placeholder="Имя"
                                required="true"
                              ></span>
                              
                              <span
                                type="select"
                                name="type"
                                label="Тип"
                                value="studija"
                              >
                                <span
                                  type="option"
                                  value="studija"
                                  text="Студия">
                                </span>
                                <span
                                  type="option"
                                  value="1komnata"
                                  text="1 комната">
                                </span>
                                <span
                                  type="option"
                                  value="2komnaty"
                                  text="2 комнаты">
                                </span>
                                <span
                                  type="option"
                                  value="3komnaty"
                                  text="3 комнаты">
                                </span>
                                <span
                                  type="option"
                                  value="4komnaty"
                                  text="4 комнаты">
                                </span>
                                <span
                                  type="option"
                                  value="5komnat"
                                  text="5 комнат">
                                </span>
                              </span>
                              <span
                                type="input"
                                name="phone"
                                label="Телефон"
                                input-type="text"
                                placeholder="+7 (999) 999-99-99"
                                required="true"
                              ></span>
                              
                              <button
                                type="submit"
                                text="Рассчитать стоимость"
                              ></button>
                            </div>
                          </form>
                          <div class="cell term_description">
                            <p>Нажимая «Рассчитать стоимость», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </main>
          <footer class="footer-container">
            <div class="grid-container">
              <div class="grid-x align-center footer">
                <div class="cell grid-x footer__navigation">
                  <div class="cell grid-x small-5 nav hide-for-small">
                    <ul class="menu vertical">
                      <li><a href="/uslugi">Все услуги</a></li>
                      <li><a href="/uslugi/uborka-kvartir">Уборка квартир</a></li>
                      <li><a href="/uslugi/uborka-kottedzhej">Уборка коттеджей</a></li>
                    </ul>
                  </div>
                  <div class="cell grid-x small-4 nav hide-for-small">
                    <ul class="menu vertical">
                      <li><a href="/uslugi/uborka-ofisov">Уборка офисов</a></li>
                      <li><a href="/uslugi/himchistka">Химчистка</a></li>
                      <li><a href="/uslugi/moyka-okon">Мойка окон</a></li>
                    </ul>
                  </div>
                  <div class="cell grid-x small-3 nav hide-for-small">
                    <ul class="menu vertical">
                      <li><a href="/uslugi/dezinfekciya">Дезинфекция</a></li>
                      <li><a href="/uslugi/uborka-pomeshchenij">Уборка помещений</a></li>
                    </ul>
                  </div>
                  <div class="cell grid-x small-12 nav show-for-small">
                    
                    <div data-block="accordion">
                      <div data-block="item" data-title="Уборка квартир">
                        <ul class="footer-mobile-answer-menu">
                          <li><a class="trackable" href="/uslugi/uborka-kvartir/generalnaya">Генеральная уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-kvartir/podderzhivayushchaya">Поддерживающая уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-kvartir/posle-remonta">Уборка после ремонта</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-kvartir/ezhednevnaya">Ежедневная уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-kvartir/srochnaya">Срочная уборка</a></li>
                        </ul>
                      </div>
                      <div data-block="item" data-title="Уборка коттеджей">
                        <ul class="footer-mobile-answer-menu">
                          <li><a class="trackable" href="/uslugi/uborka-kottedzhej/generalnaya">Генеральная уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-kottedzhej/podderzhivayushchaya">Поддерживающая уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-kottedzhej/posle-remonta">Уборка после ремонта</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-kottedzhej/srochnaya">Срочная уборка</a></li>
                        </ul>
                      </div>
                      <div data-block="item" data-title="Мойка окон">
                        <ul class="footer-mobile-answer-menu">
                          <li><a class="trackable" href="/uslugi/moyka-okon/vitrin">Мойка витрин</a></li>
                          <li><a class="trackable" href="/uslugi/moyka-okon/fasadov">Мойка фасадов</a></li>
                          <li><a class="trackable" href="/uslugi/moyka-okon/alpinistami">Мойка альпинистами</a></li>
                        </ul>
                      </div>
                      <div data-block="item" data-title="Уборка офисов">
                        <ul class="footer-mobile-answer-menu">
                          <li><a class="trackable" href="/uslugi/uborka-ofisov/ezhednevnaya">Ежедневная уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-ofisov/podderzhivayushchaya">Поддерживающая уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-ofisov/generalnaya">Генеральная уборка</a></li>
                          <li><a class="trackable" href="/uslugi/uborka-ofisov/posle-remonta">Уборка после ремонта</a></li>
                        </ul>
                      </div>
                      <div data-block="item" data-title="Химчистка">
                        <ul class="footer-mobile-answer-menu">
                          <li><a class="trackable" href="/uslugi/himchistka/s-vyezdom">Химчистка с выездом</a></li>
                          <li><a class="trackable" href="/uslugi/himchistka/myagkoj-mebeli">Химчистка мягкой мебели</a></li>
                          <li><a class="trackable" href="/uslugi/himchistka/shtor">Химчистка штор</a></li>
                          <li><a class="trackable" href="/uslugi/himchistka/kovrov">Химчистка ковров</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="cell grid-x footer__interlinks">
                  <div class="cell grid-x headline">Другие услуги</div>
                  <div class="cell grid-x medium-5">
                    <ul class="menu vertical">
                      <li><a href="/uslugi/uborka-kvartir/odnokomnatnoj">Уборка однокомнатной квартиры</a></li>
                      <li><a href="/uslugi/uborka-kvartir/dvuhkomnatnoj">Уборка двухкомнатной квартиры</a></li>
                    </ul>
                  </div>
                  <div class="cell grid-x medium-4">
                    <ul class="menu vertical">
                      <li><a href="/uslugi/uborka-kvartir/trekhkomnatnoj">Уборка трехкомнатной квартиры</a></li>
                      <li><a href="/uslugi/uborka-kottedzhej/chastnyh-domov">Уборка домов</a></li>
                    </ul>
                  </div>
                  <div class="cell grid-x medium-3">
                    <ul class="menu vertical">
                      <li><a href="/uslugi/uborka-kottedzhej/taunhausov">Уборка таунхаусов</a></li>
                    </ul>
                  </div>
                </div>
                <div class="cell grid-x footer__lower-footer">
                  <div class="cell grid-x medium-shrink lower-menu">
                    <ul class="menu">
                      <li><a class="trackable" href="/kontakty">Контакты</a></li>
                      <li><a class="trackable" href="/privacy">Политика конфиденциальности</a></li>
                      <li><a class="trackable" href="/terms">Условия использования</a></li>
                      <li><a class="trackable" href="/sitemap">Карта сайта</a></li>
                    </ul>
                  </div>
                  <div class="cell grid-x medium-auto align-right copy">2021 my-cleaning. Все права защищены.</div>
                </div>
                <div class="cell footer__term_description"><p>Данный сайт носит информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 ГК РФ.</p></div>
              </div>
            </div>
          </footer>
        ]]>
      </turbo:content>
    </item>
    ${uborkaKvartir}
    ${uborkaKvartirGeneralnaya}
    ${uborkaKvartirPodderzhivayushchaya}
    ${uborkaKvartirPosleRemonta}
    ${uborkaKvartirEzhednevnaya}
    ${uborkaKvartirSrochnaya}
    ${home}
    ${homeGeneralnaya}
    ${homePodderzhivayushchaya}
    ${homePosleRemonta}
    ${homeSrochnaya}
    ${ofisov}
    ${ofisovEzhednevnaya}
    ${ofisovPodderzhivayushchaya}
    ${ofisovGeneralnaya}
    ${ofisovPosleRemonta}
    ${himchistka}
    ${himchistkaSVyezdom}
    ${himchistkaMyagkojMebeli}
    ${himchistkaShtor}
    ${himchistkaKovrov}
    ${moykaOkon}
    ${moykaOkonVitrin}
    ${moykaOkonFasadov}
    ${moykaOkonAlpinistami}
    ${dezinfekciya}
    ${uborkaPomeshchenij}
    ${uborkaKvartirOdnokomnatnoj}
    ${uborkaKvartirDvuhkomnatnoj}
    ${uborkaKvartirTrekhkomnatnoj}
    ${homeChastnyhDomov}
    ${homeTaunhausov}
  </channel>
</rss>`;

class TurboRss extends React.Component {
  public static async getInitialProps({res}:any) {
    res.setHeader('Content-Type', 'text/xml');
    res.write(getTurboRss());
    res.end();
  }
}

export default TurboRss;
