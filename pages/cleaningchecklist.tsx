import React, { Component } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { PageComponent } from '../styles/pageComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>Уборка и уборка дома Услуги разнорабочего | Удобный</title>
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <PageComponent>
          <div className="gradient-background">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="cell small-12 large-10">
                  <div className="grid-x align-center content-container">
                    <div className="cell small-11">
                      <h1>Что вам нужно убрать?</h1>
                      <h4 className="subheader">Ниже приведены рекомендации по базовой уборке, но вам следует поговорить со своим профессионалом о том, что вы хотели бы сделать в своем доме.</h4>

                      <div className="grid-x align-center">
                        <div className="cell small-12">
                          <div className="grid-x promo-description-row">
                            <div className="small-12 medium-6 promo-description-row-right">
                              <Amp.AmpImg
                                specName="default"
                                key={Buffer.from(Math.random().toString()).toString('base64')}
                                src='/static/images/bedroom.webp'
                                className="img"
                                layout="flex-item"
                                alt='Bedroom'
                              />
                            </div>
                            <div className="small-12 medium-6 promo-description-row-left">
                              <div className="header-container">
                                <h3 className="promo-description-list-item-title">Спальня, гостиная &amp; Общие области</h3>
                              </div>
                              <ul className="promo-benefit-list">
                                <li>Удалите пыль со всех доступных поверхностей</li>
                                <li>Протрите все зеркала и стеклянную фурнитуру.</li>
                                <li>Очистите все поверхности пола.</li>
                                <li>Вывоз мусора и переработка</li>
                              </ul>
                            </div>
                          </div>
                          <div className="grid-x promo-description-row">
                            <div className="small-12 medium-6 promo-description-row-right">
                              <Amp.AmpImg
                                specName="default"
                                key={Buffer.from(Math.random().toString()).toString('base64')}
                                src='/static/images/bathroom.webp'
                                className="img"
                                layout="flex-item"
                                alt='Bathroom'
                              />
                            </div>
                            <div className="small-12 medium-6 promo-description-row-left">
                              <div className="header-container">
                                <h3 className="promo-description-list-item-title">Ванные комнаты</h3>
                              </div>
                              <ul className="promo-benefit-list">
                                <li>Вымойте и продезинфицируйте туалет, душ, ванну и раковину.</li>
                                <li>Удалите пыль со всех доступных поверхностей</li>
                                <li>Протрите все зеркала и стеклянную фурнитуру.</li>
                                <li>Очистите все поверхности пола.</li>
                                <li>Вывоз мусора и переработка</li>
                              </ul>
                            </div>
                          </div>
                          <div className="grid-x promo-description-row">
                            <div className="small-12 medium-6 promo-description-row-right">
                              <Amp.AmpImg
                                specName="default"
                                key={Buffer.from(Math.random().toString()).toString('base64')}
                                src='/static/images/kitchen.webp'
                                className="img"
                                layout="flex-item"
                                alt='Kitchen'
                              />
                            </div>
                            <div className="small-12 medium-6 promo-description-row-left">
                              <div className="header-container">
                                <h3 className="promo-description-list-item-title">Кухня</h3>
                              </div>
                              <ul className="promo-benefit-list">
                                <li>Удалите пыль со всех доступных поверхностей</li>
                                <li>Слейте воду из раковины и загрузите в посудомоечную машину грязную посуду.</li>
                                <li>Протрите внешнюю поверхность плиты, духовки и холодильника.</li>
                                <li>Очистите все поверхности пола.</li>
                                <li>Вывоз мусора и переработка</li>
                              </ul>
                            </div>
                          </div>
                          <div className="grid-x promo-description-row">
                            <div className="small-12 medium-6 promo-description-row-right">
                              <Amp.AmpImg
                                specName="default"
                                key={Buffer.from(Math.random().toString()).toString('base64')}
                                src='/static/images/extras.webp'
                                className="img"
                                layout="flex-item"
                                alt='Laundry'
                              />
                            </div>
                            <div className="small-12 medium-6 promo-description-row-left">
                              <div className="header-container">
                                <h3 className="promo-description-list-item-title">Дополнительно <span className="small-promo-description-title">(по требованию)</span></h3>
                                <p>Для более глубокой очистки рассмотрите возможность добавления одной или нескольких дополнительных чистящих средств. Большинство дополнительных услуг по уборке добавляют к вашему бронированию полчаса времени и стоимость.</p>
                              </div>
                              <ul className="promo-benefit-list">
                                <li>Внутри шкафов</li>
                                <li>Внутри холодильника</li>
                                <li>Внутри духовки</li>
                                <li>Стирка и сушка белья</li>
                                <li>Внутренние окна</li>
                              </ul>
                            </div>
                          </div>
                          <div className="grid-x promo-description-row">
                            <div className="small-12">
                              <p className="cleaning-exceptions">Следующие услуги в настоящее время не предлагаются:</p>
                              <p className="cleaning-exceptions">Внешняя очистка окон, удаление глубоких пятен, заражение, удаление плесени, удаление насекомых.</p>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </PageComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
