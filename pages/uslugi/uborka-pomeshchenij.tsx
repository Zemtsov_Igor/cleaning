import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../styles/grid'
import { GlobalStyle } from '../../styles/global'
import { CenterFormComponent } from '../../styles/centerFormComponent.Styles';

import MainLayout from '../../components/Layouts/MainLayout/MainLayout';
import Header from '../../components/Header/Header';
import Footer, {turboFooter} from '../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../components/Content/GetStarted/GetStarted';
import Prices, {turboPrices} from '../../components/Content/Prices/Prices';

import { pricesService } from '../../constants/pricesServices.constants';
import CenterForm, {turboCenterForm} from "../../components/Content/Forms/CenterForm/CenterForm";

const data:any = {
  settings: {
    title: 'Уборка помещений в Москве и Московоской области в компании my-cleaning',
    description: 'Уборка помещений в Москве и области от компании «my-cleaning». Закажите профессиональную уборку по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-pomeshchenij-bg.webp',
    name: 'Уборка помещений',
    rating: 0,
    url: '/uslugi/uborka-pomeshchenij',
    sku: 27,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Уборка помещений',
      description: {
        title: null,
        text: [
          'Для того, чтобы содержать в порядке помещения квартиры, дома, офиса, а также другие производственные помещения  приложить огромное количество усилий. Постепенная чистка каждой поверхности, регулярное удаление не только серьёзных загрязнений, но и обыкновенной пыли, мойка и очищение дверей и окон, неизбежно контактирующих с внешней средой, всегда связаны с необходимостью иметь в распоряжении большое количество времени, специальные составы. Предварительно придётся составить график уборки, сдвинуть мебель, подготовить соответствующие инструменты и приспособления. Все данные действия Вы можете с легкостью доверить сотрудникам профессиональной клининговой компании. Мы оказываем услуги по клинингу помещений любой сложности и любого уровня загрязнений.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Заказать уборку',
      place: false
    }
  },
  included: {
    title: 'Что входит в уборку помещений?',
    description: 'Специализированная уборка помещений в Москве сегодня – оптимальное решение, позволяющее быстро получить эффективный результат при минимуме самостоятельных усилий. В перечень услуг профессиональной уборки входят следующие процедуры:',
    items: [
      {
        icon: {
          src: '/static/images/included/uborka-pomeshchenij-1.webp',
          alt: 'Общий клининг помещенийжилого типа'
        },
        title: '',
        description: '',
        list: [
          'Профессиональный  клининг помещений жилого типа, домов и квартир, включая химчистку, влажную и сухую уборку, чистку мебели и поверхностей;',
          'Уборка производственных  помещений и офисов, аккуратная уборка техники, офисной мебели, полов, окон и витражей, чистка систем вентиляции.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-pomeshchenij-2.webp',
          alt: 'Специализированные услуги по дезинфекции'
        },
        title: '',
        description: '',
        list: [
          'Специализированные услуги по дезинфекции, в рамках уборки медицинских организаций или жилых помещений после санитарной обработки;',
          'Услуги по уборке после ремонта помещения, вывоз и уборка строительного мусора, например, после замены окон или дверей в квартире.'
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Как можно заказать уборку?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы оказываем услуги по уборке в помещениях разного назначения: жилых и производственных помещениях, офисах, складских помещениях и на любых других объектах заказчиков.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши клинеры работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится уборка?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка в квартире или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  prices: {
    title: 'Цены на уборку помещений',
    description: '',
    items: pricesService('premises_cleaning'),
    type: 'premises',
    className: ''
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Уборка помещений'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <CenterFormComponent role="main">
          {/* header bg */}
          <meta itemProp="image" content={data.settings.bgImage}/>
          <div
            id="top"
            className="quote-bg row"
            style={{backgroundImage: `url(${data.settings.bgImage})`}}
          />

          {/* form */}
          <CenterForm data={{settings: data.settings}} />

          {/* disclaimer */}
          <div className="disclaimer">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="cell small-12 small-centered xlarge-6">
                  Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                </div>
              </div>
            </div>
          </div>

          {/* head description */}
          <div className="head__description">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div itemProp="description" className="cell small-12 small-centered xlarge-8">
                  {
                    data.settings.form.description.text.map((item: string) => (
                      <p>{item}</p>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Affordable Luxury */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Клининговая компания оказывает услуги по уборке помещений любых назначений  в Москве и Московской области. Наши специалисты гарантируют при этом высокое качество всех доступных работ, оперативность, аккуратность, соблюдение сроков, безопасность для любого имущества заказчика.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </CenterFormComponent>

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const uborkaPomeshchenij:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 12 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                    <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboCenterForm({settings: data.settings})}
          ${turboPrices(data.prices)}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Клининговая компания оказывает услуги по уборке помещений любых назначений  в Москве и Московской области. Наши специалисты гарантируют при этом высокое качество всех доступных работ, оперативность, аккуратность, соблюдение сроков, безопасность для любого имущества заказчика.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
