import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../styles/grid'
import { GlobalStyle } from '../../styles/global'
import { ServicesComponent } from '../../styles/servicesComponent.Styles';

import MainLayout from '../../components/Layouts/MainLayout/MainLayout';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

const breadcrumbsPath:any = [
  {
    title: 'Все услуги'
  }
];

export default class Services extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/ItemList'}>
        <Head>
          <title itemProp="name">Все услуги</title>
          <meta name="description" content="Все услуги - от компании «my-cleaning». Закажите профессиональную уборку по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79" />
          <link rel="amphtml" href={`https://amp.my-cleaning.ru/uslugi`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <ServicesComponent role="main">
          <section>
            <div className="hero-section">
              <h1>Клининговые услуги в Москве</h1>
            </div>

            <div className="grid-container">
              <div className="grid-x grid-margin-x service-categories-container">

                {/* Left menu */}
                <div className="cell medium-3 stickybar hide-for-small">
                  <h2 className="all-categories">Все категории</h2>
                  <div className="cell categories-list" >
                    <ul>
                      <li>
                        <span className="categories-list__item">
                          <span
                            role="button"
                            tabIndex={0}
                            className="service-category-link"
                            data-anchor="#home_cleaning"
                          >
                            Уборка квартир
                          </span>
                        </span>
                      </li>
                      <li>
                        <span className="categories-list__item">
                          <span
                            role="button"
                            tabIndex={0}
                            className="service-category-link"
                            data-anchor="#house_cleaning"
                          >
                            Уборка коттеджей
                          </span>
                        </span>
                      </li>
                      <li>
                        <span className="categories-list__item">
                          <span
                            role="button"
                            tabIndex={0}
                            className="service-category-link"
                            data-anchor="#office_cleaning"
                          >
                            Уборка офисов
                          </span>
                        </span>
                      </li>
                      <li>
                        <span className="categories-list__item">
                          <span
                            role="button"
                            tabIndex={0}
                            className="service-category-link"
                            data-anchor="#himchistka_cleaning"
                          >
                            Химчистка
                          </span>
                        </span>
                      </li>
                      <li>
                        <span className="categories-list__item">
                          <span
                            role="button"
                            tabIndex={0}
                            className="service-category-link"
                            data-anchor="#okon_cleaning"
                          >
                            Мойка окон
                          </span>
                        </span>
                      </li>
                      <li>
                        <span className="categories-list__item">
                          <span
                            role="button"
                            tabIndex={0}
                            className="service-category-link"
                            data-anchor="#dezinfekciya_cleaning"
                          >
                            Дезинфекция
                          </span>
                        </span>
                      </li>
                      <li>
                        <span className="categories-list__item">
                          <span
                            role="button"
                            tabIndex={0}
                            className="service-category-link"
                            data-anchor="#room_cleaning"
                          >
                            Уборка помещений
                          </span>
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>

              {/* Left menu for mobile */}
                <div className="cell small-12 scrolling-wrapper stickybar show-for-small">
                  <div className="scrollable-item">
                    <span className="categories-list__item">
                      <span
                        role="button"
                        tabIndex={0}
                        className="service-category-link"
                        data-anchor="#home_cleaning"
                      >
                        Уборка квартир
                      </span>
                    </span>
                  </div>
                  <div className="scrollable-item">
                    <span className="categories-list__item">
                      <span
                        role="button"
                        tabIndex={0}
                        className="service-category-link"
                        data-anchor="#house_cleaning"
                      >
                        Уборка коттеджей
                      </span>
                    </span>
                  </div>
                  <div className="scrollable-item">
                    <span className="categories-list__item">
                      <span
                        role="button"
                        tabIndex={0}
                        className="service-category-link"
                        data-anchor="#office_cleaning"
                      >
                        Уборка офисов
                      </span>
                    </span>
                  </div>
                  <div className="scrollable-item">
                    <span className="categories-list__item">
                      <span
                        role="button"
                        tabIndex={0}
                        className="service-category-link"
                        data-anchor="#himchistka_cleaning"
                      >
                        Химчистка
                      </span>
                    </span>
                  </div>
                  <div className="scrollable-item">
                    <span className="categories-list__item">
                      <span
                        role="button"
                        tabIndex={0}
                        className="service-category-link"
                        data-anchor="#okon_cleaning"
                      >
                        Мойка окон
                      </span>
                    </span>
                  </div>
                  <div className="scrollable-item">
                    <span className="categories-list__item">
                      <span
                        role="button"
                        tabIndex={0}
                        className="service-category-link"
                        data-anchor="#dezinfekciya_cleaning"
                      >
                        Дезинфекция
                      </span>
                    </span>
                  </div>
                  <div className="scrollable-item">
                    <span className="categories-list__item">
                      <span
                        role="button"
                        tabIndex={0}
                        className="service-category-link"
                        data-anchor="#room_cleaning"
                      >
                        Уборка помещений
                      </span>
                    </span>
                  </div>
                </div>

                {/* Categories */}
                <div className="cell small-12 medium-9 category-list-block">
                  <div className="grid-x">

                    <div className="cell small-12">
                      <div className="category-anchor" id="home_cleaning" />
                      <h3 className="category-header">Уборка квартир</h3>
                    </div>

                    <div className="cell small-12">
                      <div className="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 services-grid">
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="1" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel="nofollow"
                              href="/uslugi/uborka-kvartir"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="1" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/uborka-kvartir.webp" />
                              <img
                                src="/static/images/uslugi/uborka-kvartir.webp"
                                className="img"
                                alt="Уборка квартир"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Уборка квартир
                            </div>
                          </div>
                        </div>

                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="2" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel="nofollow"
                              href="/uslugi/uborka-kvartir/generalnaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="2" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/generalnaya-kvartir.webp" />
                              <img
                                src="/static/images/uslugi/generalnaya-kvartir.webp"
                                className="img"
                                alt="Генеральная уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Генеральная уборка
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="3" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel="nofollow"
                              href="/uslugi/uborka-kvartir/podderzhivayushchaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="3" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/podderzhivayushchaya-kvartir.webp"/>
                              <img
                                src="/static/images/uslugi/podderzhivayushchaya-kvartir.webp"
                                className="img"
                                alt="Поддерживающая уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Поддерживающая уборка
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="4" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel="nofollow"
                              href="/uslugi/uborka-kvartir/posle-remonta"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="4" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/posle-remonta-kvartir.webp"/>
                              <img
                                src="/static/images/uslugi/posle-remonta-kvartir.webp"
                                className="img"
                                alt="Уборка после ремонта"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Уборка после ремонта
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="5" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel="nofollow"
                              href="/uslugi/uborka-kvartir/ezhednevnaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="5" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/ezhednevnaya-kvartir.webp"></meta>
                              <img
                                src="/static/images/uslugi/ezhednevnaya-kvartir.webp"
                                className="img"
                                alt="Ежедневная уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Ежедневная уборка
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="6" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel="nofollow"
                              href="/uslugi/uborka-kvartir/srochnaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="6" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/srochnaya-kvartir.webp"></meta>
                              <img
                                src="/static/images/uslugi/srochnaya-kvartir.webp"
                                className="img"
                                alt="Срочная уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Срочная уборка
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="cell small-12 services-list">

                      <div className="grid-x grid-margin-x">
                        <div
                          className="cell small-12 medium-4"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="28" />
                          <span
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <meta itemProp="sku" content="28" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <a
                              className="service-link"
                              rel=""
                              href="/uslugi/uborka-kvartir/odnokomnatnoj"
                              itemProp="url"
                            >
                              <span itemProp="name">Уборка однокомнатной квартиры</span>
                            </a>
                          </span>
                        </div>

                        <div
                          className="cell small-12 medium-4"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="29" />
                          <span
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <meta itemProp="sku" content="29" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <a
                              className="service-link"
                              rel=""
                              href="/uslugi/uborka-kvartir/dvuhkomnatnoj"
                              itemProp="url"
                            >
                              <span itemProp="name">Уборка двухкомнатной квартиры</span>
                            </a>
                          </span>
                        </div>

                        <div
                          className="cell small-12 medium-4"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="30" />
                          <span
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <meta itemProp="sku" content="30" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <a
                              className="service-link"
                              rel=""
                              href="/uslugi/uborka-kvartir/trekhkomnatnoj"
                              itemProp="url"
                            >
                              <span itemProp="name">Уборка трехкомнатной квартиры</span>
                            </a>
                          </span>
                        </div>

                      </div>
                    </div>

                    <div className="cell small-12">
                      <div className="category-anchor" id="house_cleaning" />
                      <h3 className="category-header">Уборка коттеджей</h3>
                    </div>

                    <div className="cell small-12">
                      <div className="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 services-grid">
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="7" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-kottedzhej"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="7" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/uborka-kottedzhej.webp"/>
                              <img
                                src="/static/images/uslugi/uborka-kottedzhej.webp"
                                className="img"
                                alt="Уборка коттеджей"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Уборка коттеджей
                            </div>
                          </div>
                        </div>

                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="8" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-kottedzhej/generalnaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="8" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/generalnaya-kottedzhej.webp"/>
                              <img
                                src="/static/images/uslugi/generalnaya-kottedzhej.webp"
                                className="img"
                                alt="Генеральная уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Генеральная уборка
                            </div>
                          </div>
                        </div>

                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="9" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-kottedzhej/podderzhivayushchaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="9" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/podderzhivayushchaya-kottedzhej.webp" />
                              <img
                                src="/static/images/uslugi/podderzhivayushchaya-kottedzhej.webp"
                                className="img"
                                alt="Поддерживающая уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Поддерживающая уборка
                            </div>
                          </div>
                        </div>

                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="10" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-kottedzhej/posle-remonta"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="10" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/posle-remonta-kottedzhej.webp"/>
                              <img
                                src="/static/images/uslugi/posle-remonta-kottedzhej.webp"
                                className="img"
                                alt="Уборка после ремонта"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Уборка после ремонта
                            </div>
                          </div>
                        </div>

                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="11" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-kottedzhej/srochnaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="11" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/srochnaya-kottedzhej.webp"/>
                              <img
                                src="/static/images/uslugi/srochnaya-kottedzhej.webp"
                                className="img"
                                alt="Срочная уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Срочная уборка
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="cell small-12 services-list">
                      <div className="grid-x grid-margin-x">
                        <div
                          className="cell small-12 medium-4"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="31" />
                          <span
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <meta itemProp="sku" content="31" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <a
                              className="service-link"
                              rel=""
                              href="/uslugi/uborka-kottedzhej/chastnyh-domov"
                              itemProp="url"
                            >
                              <span itemProp="name">Уборка домов</span>
                            </a>
                          </span>
                        </div>

                        <div
                          className="cell small-12 medium-4"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="32" />
                          <span
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <meta itemProp="sku" content="32" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <a
                              className="service-link"
                              rel=""
                              href="/uslugi/uborka-kottedzhej/taunhausov"
                              itemProp="url"
                            >
                              <span itemProp="name">Уборка таунхаусов</span>
                            </a>
                          </span>
                        </div>

                      </div>
                    </div>

                    <div className="cell small-12">

                      <div className="category-anchor" id="office_cleaning" />
                      <h3 className="category-header">Уборка офисов</h3>
                    </div>
                    <div className="cell small-12">
                      <div className="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 services-grid">
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="12" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-ofisov"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="12" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/uborka-ofisov.webp"/>
                              <img
                                src="/static/images/uslugi/uborka-ofisov.webp"
                                className="img"
                                alt="Уборка офисов"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Уборка офисов
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="13" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-ofisov/ezhednevnaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="13" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/ezhednevnaya-ofisov.webp"/>
                              <img
                                src="/static/images/uslugi/ezhednevnaya-ofisov.webp"
                                className="img"
                                alt="Ежедневная уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Ежедневная уборка
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="14" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-ofisov/podderzhivayushchaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="14" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/podderzhivayushchaya-ofisov.webp"/>
                              <img
                                src="/static/images/uslugi/podderzhivayushchaya-ofisov.webp"
                                className="img"
                                alt="Поддерживающая уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Поддерживающая уборка
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="15" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-ofisov/generalnaya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="15" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/generalnaya-ofisov.webp"/>
                              <img
                                src="/static/images/uslugi/generalnaya-ofisov.webp"
                                className="img"
                                alt="Генеральная уборка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Генеральная уборка
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="16" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-ofisov/posle-remonta"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="16" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/posle-remont-ofisov.web"/>
                              <img
                                src="/static/images/uslugi/posle-remont-ofisov.webp"
                                className="img"
                                alt="Уборка после ремонта"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Уборка после ремонта
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="cell small-12 services-list">
                      </div>

                    </div>

                    <div className="cell small-12">
                      <div className="category-anchor" id="himchistka_cleaning" />
                      <h3 className="category-header">Химчистка</h3>
                    </div>
                    <div className="cell small-12">
                      <div className="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 services-grid">
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="17" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/himchistka"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="17" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/himchistka.webp"/>
                              <img
                                src="/static/images/uslugi/himchistka.webp"
                                className="img"
                                alt="Химчистка"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Химчистка
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="18" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/himchistka/s-vyezdom"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="18" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/s-vyezdom-himchistka.webp"/>
                              <img
                                src="/static/images/uslugi/s-vyezdom-himchistka.webp"
                                className="img"
                                alt="Химчистка с выездом"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Химчистка с выездом
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="19" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/himchistka/myagkoj-mebeli"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="19" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/myagkoj-mebeli-himchistka.webp"/>
                              <img
                                src="/static/images/uslugi/myagkoj-mebeli-himchistka.webp"
                                className="img"
                                alt="Химчистка мягкой мебели"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Химчистка мягкой мебели
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="20" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/himchistka/shtor"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="20" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/shtor-himchistka.webp"/>
                              <img
                                src="/static/images/uslugi/shtor-himchistka.webp"
                                className="img"
                                alt="Химчистка штор"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Химчистка штор
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="21" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/himchistka/kovrov"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="21" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/kovrov-himchistka.webp"/>
                              <img
                                src="/static/images/uslugi/kovrov-himchistka.webp"
                                className="img"
                                alt="Химчистка ковров"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Химчистка ковров
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="cell small-12 services-list">
                      </div>

                    </div>

                    <div className="cell small-12">
                      <div className="category-anchor" id="okon_cleaning" />
                      <h3 className="category-header">Мойка окон</h3>
                    </div>
                    <div className="cell small-12">
                      <div className="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 services-grid">
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="22" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/moyka-okon"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="22" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/moyka-okon.webp"/>
                              <img
                                src="/static/images/uslugi/moyka-okon.webp"
                                className="img"
                                alt="Мойка окон"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Мойка окон
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="23" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/moyka-okon/vitrin"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="23" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/vitrin-okon.webp"/>
                              <img
                                src="/static/images/uslugi/vitrin-okon.webp"
                                className="img"
                                alt="Мойка витрин"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Мойка витрин
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="24" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/moyka-okon/fasadov"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="24" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/fasadov-okon.webp"/>
                              <img
                                src="/static/images/uslugi/fasadov-okon.webp"
                                className="img"
                                alt="Мойка фасадов"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Мойка фасадов
                            </div>
                          </div>
                        </div>
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="25" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/moyka-okon/alpinistami"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="25" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/alpinistami-okon.webp"/>
                              <img
                                src="/static/images/uslugi/alpinistami-okon.webp"
                                className="img"
                                alt="Мойка альпинистами"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Мойка альпинистами
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="cell small-12 services-list">
                      </div>

                    </div>

                    <div className="cell small-12">
                      <div className="category-anchor" id="dezinfekciya_cleaning" />
                      <h3 className="category-header">Дезинфекция</h3>
                    </div>
                    <div className="cell small-12">
                      <div className="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 services-grid">
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="26" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/dezinfekciya"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="26" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/dezinfekciya.webp"/>
                              <img
                                src="/static/images/uslugi/dezinfekciya.webp"
                                className="img"
                                alt="Дезинфекция"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Дезинфекция
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div className="cell small-12 services-list">
                    </div>

                    <div className="cell small-12">
                      <div className="category-anchor" id="room_cleaning" />
                      <h3 className="category-header">Уборка помещений</h3>
                    </div>
                    <div className="cell small-12">
                      <div className="grid-x grid-margin-x small-up-1 medium-up-2 large-up-3 services-grid">
                        <div
                          className="cell card card-shadow"
                          itemProp="itemListElement"
                          itemScope
                          itemType="http://schema.org/ListItem"
                        >
                          <meta itemProp="position" content="27" />

                          <div
                            className="card"
                            itemProp="item"
                            itemScope
                            itemType="http://schema.org/Product"
                          >
                            <a
                              className="card-link"
                              rel=""
                              href="/uslugi/uborka-pomeshchenij"
                              itemProp="url"
                            />
                            <meta itemProp="sku" content="27" />
                            <meta itemProp="mpn" content="CLEANING" />
                            <meta itemProp="brand" content="My Cleaning" />
                            <div itemProp="aggregateRating" itemScope itemType="http://schema.org/AggregateRating">
                              <meta itemProp="ratingValue" content="4.5" />
                              <meta itemProp="reviewCount" content="5" />
                            </div>
                            <div className="thumbnail">
                              <meta itemProp="image" content="/static/images/uslugi/uborka-pomeshchenij.webp"/>
                              <img
                                src="/static/images/uslugi/uborka-pomeshchenij.webp"
                                className="img"
                                alt="Уборка помещений"
                              />
                            </div>
                            <div
                              className="title"
                              itemProp="name"
                            >
                              Уборка помещений
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div className="cell small-12 services-list">
                    </div>

                  </div>
                </div>
              </div>
            </div>

          </section>
        </ServicesComponent>

        {/* Breadcrumbs */}
        <Breadcrumbs path={breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};
