import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';

import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';

import {otherServicesService} from "../../../constants/otherServices.constants";
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";
import OtherServices, {turboOtherServices} from "../../../components/Content/OtherServices/OtherServices";
import {pricesService} from "../../../constants/pricesServices.constants";
import Prices, {turboPrices} from "../../../components/Content/Prices/Prices";

const data:any = {
  settings: {
    title: 'Мойка окон альпинистами в Москве и Подмосковье – заказать мытье окон в компании My-cleaning',
    description: 'Мойка окон альпинистами от компании «my-cleaning». Закажите мойку окон альпинистами по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/moyka-okon-alpinistami-bg.webp',
    name: 'Мойка окон альпинистами в Москве и Подмосковье',
    rating: customerCleaningRating('window_cleaning') / customerCleaningReviewsCount('window_cleaning'),
    url: '/uslugi/moyka-okon/alpinistami',
    sku: 25,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Мойка окон альпинистами',
      description: {
        title: null,
        text: [
          'Профессиональные альпинисты в современных условиях проводят целый перечень различных работ, необходимых для поддержания общего внешнего вида зданий и офисных помещений, а также для профилактики быстрого износа или порчи поверхностей. Регулярная уборка подразумевает как мойку окон в квартире, так и работы в зданиях, где расположены бизнес-центры, коммерческие офисы, государственные организации.',
          'Специалисты клининговой компании, занимающиеся промышленным альпинизмом, осуществляют сложные работы по мойке окон на значительной высоте, когда нет возможности очищения поверхности другими способами.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: false
    }
  },
  included: {
    title: 'Что входит в мойку окон альпинистами?',
    description: 'На сегодняшний день промышленными альпинистами в Москве и Московской области производятся следующие виды по мойке окон и фасадов зданий:',
    items: [
      {
        icon: {
          src: '/static/images/included/moyka-okon-alpinistami-1.webp',
          alt: 'Мойка окон любых размеров и форм'
        },
        title: '',
        description: '',
        list: [
          'Очистка окон любых размеров и форм;',
          'Чистка наружных поверхностей остекления, крытых веранд, лоджий и балконов в жилых многоквартирных и загородных домах;',
          'Обработка сложных стеклянных конструкций, куполов, декоративных фигур.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/moyka-okon-alpinistami-2.webp',
          alt: 'Чистка витражей'
        },
        title: '',
        description: '',
        list: [
          'Мойка витражных окон;',
          'Обработка и чистка фасадов зданий для ликвидации загрязнений и предотвращения разрушений вследствие внешних негативных причин.'
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на мойку окон',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('window_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('window_cleaning'),
    },
    items: customerCleaningReviews('window_cleaning'),
    turboItems: customerCleaningTurboReviews('window_cleaning', 5),
    service_type: 'window_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по мойке окон',
    items: otherServicesService('window_cleaning', '/uslugi/moyka-okon/alpinistami'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на мойку окон альпинистами',
    description: '',
    items: pricesService('alpinistami_cleaning'),
    type: 'alpinistami',
    className: '',
    url: '/uslugi/moyka-okon/alpinistami'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Мойка окон альпинистами'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <h2>Особенности ценообразования на мойку окон альпинистами</h2>
                    <p>Прежде чем заказать мойку окон альпинистами, необходимо учесть нюансы, которые влияют на окончательную стоимость предоставляемых услуг:</p>
                    <ul>
                      <li>высота здания;</li>
                      <li>наличие выступающих элементов;</li>
                      <li>степень загрязнения;</li>
                      <li>места крепления страховки промышленных альпинистов;</li>
                      <li>площадь поверхности окон;</li>
                      <li>температура воздуха и погодные условия.</li>
                    </ul>
                    <p>Таким образом, при мойке фасадов и окон альпинистами, цена всегда соответствует выполненной работе. Наши клиенты оплачивают только те услуги, которые мы им предоставляем без малейших переплат.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Мойка окон альпинистами проводится специалистами нашей клининговой компании в соответствии с пожеланиями заказчика. При этом строго соблюдаются сроки, отслеживается качество и детали работ, все работы ведутся с обеспечением мер безопасности, с учётом технологии сложных работ.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>

        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const moykaOkonAlpinistami:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 22 Jul 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboPrices(data.prices)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <h2>Особенности ценообразования на мойку окон альпинистами</h2>
                    <p>Прежде чем заказать мойку окон альпинистами, необходимо учесть нюансы, которые влияют на окончательную стоимость предоставляемых услуг:</p>
                    <ul>
                      <li>высота здания;</li>
                      <li>наличие выступающих элементов;</li>
                      <li>степень загрязнения;</li>
                      <li>места крепления страховки промышленных альпинистов;</li>
                      <li>площадь поверхности окон;</li>
                      <li>температура воздуха и погодные условия.</li>
                    </ul>
                    <p>Таким образом, при мойке фасадов и окон альпинистами, цена всегда соответствует выполненной работе. Наши клиенты оплачивают только те услуги, которые мы им предоставляем без малейших переплат.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
          ${turboReviews(data.reviews, data.settings.url)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Мойка окон альпинистами проводится специалистами нашей клининговой компании в соответствии с пожеланиями заказчика. При этом строго соблюдаются сроки, отслеживается качество и детали работ, все работы ведутся с обеспечением мер безопасности, с учётом технологии сложных работ.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
