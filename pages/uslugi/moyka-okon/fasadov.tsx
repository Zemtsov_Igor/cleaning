import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';

import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';

import {otherServicesService} from "../../../constants/otherServices.constants";
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";
import OtherServices, {turboOtherServices} from "../../../components/Content/OtherServices/OtherServices";
import {pricesService} from "../../../constants/pricesServices.constants";
import Prices from "../../../components/Content/Prices/Prices";

const data:any = {
  settings: {
    title: 'Мойка фасадов в Москве | Клининговая компания my-cleaning',
    description: 'Мойка фасада от компании «my-cleaning». Закажите профессиональную мойку фасада по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/moyka-okon-fasadov-bg.webp',
    name: 'Мойка фасадов',
    rating: customerCleaningRating('window_cleaning') / customerCleaningReviewsCount('window_cleaning'),
    url: '/uslugi/moyka-okon/fasadov',
    sku: 24,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Мойка фасадов',
      description: {
        title: null,
        text: [
          'Поддержание чистоты и уборка необходимы не только внутри домов и офисов, в жилых и рабочих пространствах, но и снаружи. Регулярная чистка окон, дверей, фасадов зданий осуществляется сегодня в Москве с применением специализированных методов и сложного оборудования, инструментов и практических навыков подобных работ. На этажах, находящихся выше первого, на уровне нескольких метров от земли, затруднительны работы даже с использованием телескопических ручек, поэтому в большинстве случаев необходимы услуги высотной мойки, автовышки или альпинистов.',
          'Мойка фасадов производится клининговой компанией моющими средствами и специальными составами для быстрой очистки сложных поверхностей, стёкол и зеркал. Материалы для обработки фасадов комплектуются с учётом того, чтобы сохранить структуру, цвет и прочность поверхности фасадов зданий. Внутренняя часть работ, чистка открываемых створок в офисе реализуется внутри здания. После этого выбирается оптимальный способ наружных работ, привлекаются альпинисты для высотных работ, в некоторых ситуациях оптимальным решением становится профессиональная мойка с автовышки.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: false
    }
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на мойку окон',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('window_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('window_cleaning'),
    },
    items: customerCleaningReviews('window_cleaning'),
    turboItems: customerCleaningTurboReviews('window_cleaning', 5),
    service_type: 'window_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по мойке окон',
    items: otherServicesService('window_cleaning', '/uslugi/moyka-okon/fasadov'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на мойку фасадов',
    description: '',
    items: pricesService('alpinistami_cleaning'),
    type: 'alpinistami',
    className: '',
    url: '/uslugi/moyka-okon/fasadov'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Мойка фасадов'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Заказать такие работы по мойке фасадов в Москве сегодня можно в нашей компании, где соблюдаются сроки, отслеживается качество работ, разработаны доступные цены и есть возможность обратиться к специалистам в любое удобное время.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>

        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const moykaOkonFasadov:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 22 Jul 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboReviews(data.reviews, data.settings.url)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Заказать такие работы по мойке фасадов в Москве сегодня можно в нашей компании, где соблюдаются сроки, отслеживается качество работ, разработаны доступные цены и есть возможность обратиться к специалистам в любое удобное время.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;

