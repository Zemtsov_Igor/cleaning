import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';

import {
  customerCleaningReviewsCount,
  customerCleaningReviews,
  customerCleaningTurboReviews, customerCleaningRating
} from '../../../constants/customerReviews.constants';

import {otherServicesService} from "../../../constants/otherServices.constants";
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";
import OtherServices, {turboOtherServices} from "../../../components/Content/OtherServices/OtherServices";
import {pricesService} from "../../../constants/pricesServices.constants";
import Prices from "../../../components/Content/Prices/Prices";

const data:any = {
  settings: {
    title: 'Мойка витрин в Москве | my-cleaning',
    description: 'Мойка витрин в Москве от компании «my-cleaning». Закажите мойку витрин и окон по доступной цене! Работаем без выходных. Наш телефон в Москве +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/moyka-okon-vitrin-bg.webp',
    name: 'Мойка витрин',
    rating: customerCleaningRating('window_cleaning') / customerCleaningReviewsCount('window_cleaning'),
    url: '/uslugi/moyka-okon/vitrin',
    sku: 23,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Мойка витрин',
      description: {
        title: 'Кому требуется услуга мойки витрин?',
        text: [
          'Витрина — лицо магазина, ведь покупатели в первую очередь смотрят на разложенные внутри товары. Пятна, разводы и грязь на стекле порой мешают рассмотреть содержимое и портят имидж вашего бизнеса. Процесс мойки витрин нередко осложняется высотой витрины, ее труднодоступностью, а также плохой погодой (если ваш магазин расположен на улице). Чтобы получить гарантию постоянной чистоты всех стендов и стеллажей в вашем магазине, закажите услуги компании My cleaning.',
          'Услуга включает в себя очищение всех типов витрин, включая открытые и закрытые, зеркальные, холодильные и так далее.'
        ],
        subText: [
          'Чаще всего ее заказывают владельцы:'
        ]
      },
      capabilities: [
        'Торговых центров и расположенных в них магазинов;',
        'Кафе, ресторанов, баров и кофеен;',
        'Гостиниц и офисов;',
        'Автосалонов.'
      ],
      btnText: 'Узнать цену',
      place: false
    }
  },
  included: {
    title: 'Почему стоит воспользоваться услугой мойки витрин?',
    description: '',
    items: [
      {
        icon: {
          src: '/static/images/included/moyka-okon-vitrin-1.webp',
          alt: 'Экономия бюджета'
        },
        title: 'Экономия бюджета',
        description: 'Для профессиональной мойки витрин нужны специальное оборудование и безвредные средства. Покупать их — это ударить по бюджету магазина. В стоимость услуги уже входит стоимость средств химии и профессиональное оборудование.',
        list: []
      },
      {
        icon: {
          src: '/static/images/included/moyka-okon-vitrin-2.webp',
          alt: 'Профессиональное оборудование и чистящие составы'
        },
        title: 'Профессиональное оборудование и чистящие составы',
        description: 'Мы используем деионизированную воду. Она не оставляет разводов и удаляет атмосферные загрязнения.',
        list: []
      },
      {
        icon: {
          src: '/static/images/included/moyka-okon-vitrin-3.webp',
          alt: 'Экономия сил и времени'
        },
        title: 'Экономия сил и времени',
        description: 'Самостоятельное и качественное мытье витрин требует внимания к каждому квадратному метру. Это большие временные затраты. Предлагаем заказать эту услугу в нашей клининговой компании, чтобы направить свои силы на семью или работу.',
        list: []
      }
    ],
    className: 'with_btn even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на мойку окон',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('window_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('window_cleaning'),
    },
    items: customerCleaningReviews('window_cleaning'),
    turboItems: customerCleaningTurboReviews('window_cleaning', 5),
    service_type: 'window_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по мойке окон',
    items: otherServicesService('window_cleaning', '/uslugi/moyka-okon/vitrin'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на мойку витрин',
    description: '',
    items: pricesService('window_cleaning'),
    type: 'window',
    className: '',
    url: '/uslugi/moyka-okon/vitrin'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Мойка витрин'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Мы работаем безвредными и эффективными средствами,  которые не оставляют разводов и удаляет атмосферные загрязнения. Чтобы узнать стоимость услуги, позвоните по телефону,  указанному на сайте. Наш специалист проконсультирует вас.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>

        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const moykaOkonVitrin:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 22 Jul 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboReviews(data.reviews, data.settings.url)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Мы работаем безвредными и эффективными средствами,  которые не оставляют разводов и удаляет атмосферные загрязнения. Чтобы узнать стоимость услуги, позвоните по телефону,  указанному на сайте. Наш специалист проконсультирует вас.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
