import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';

import {
  customerCleaningReviewsCount,
  customerCleaningReviews,
  customerCleaningTurboReviews, customerCleaningRating
} from '../../../constants/customerReviews.constants';
import { pricesService } from '../../../constants/pricesServices.constants';

import {otherServicesService} from "../../../constants/otherServices.constants";
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";
import OtherServices, {turboOtherServices} from "../../../components/Content/OtherServices/OtherServices";

const data:any = {
  settings: {
    title: 'Мойка окон в Москве и области – цены в My-cleaning',
    description: 'Мойка окон в Москве от компании «my-cleaning». Закажите профессиональную мойку окон по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/moyka-okon-bg.webp',
    name: 'Мытье окон в квартире или доме в Москве',
    rating: customerCleaningRating('window_cleaning') / customerCleaningReviewsCount('window_cleaning'),
    url: '/uslugi/moyka-okon',
    sku: 22,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Мойка окон',
      description: {
        title: null,
        text: [
          'Мойка окон – комплексный процесс, для которого необходимы не только чистящие средства, специальные инструменты и дополнительные приспособления, но и навыки соответствующих работ для того, чтобы уборка не заняла слишком много времени. Доверьте мытье окон в Вашей квартире клининговой компании My-cleaning по доступным ценам: окна в квартире от 1900 р; окна и балконы от 2900 р.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: false
    }
  },
  included: {
    title: 'Что входит в мойку окон?',
    description: 'Сегодня в Москве легко воспользоваться профессиональной помощью клининговой компании, чтобы мойка окон в квартире была проведена в сжатые сроки и максимально качественно. В перечень доступных услуг при этом входит:',
    items: [
      {
        icon: {
          src: '/static/images/included/moyka-okon-1.webp',
          alt: 'Мойка окон независимо от их количества и типа остекления'
        },
        title: '',
        description: '',
        list: [
          'Мойка окон независимо от их количества и типа остекления;',
          'Чистка лоджий и балконов.',
        ]
      },
      {
        icon: {
          src: '/static/images/included/moyka-okon-2.webp',
          alt: 'Уборка откосов, подоконников'
        },
        title: '',
        description: '',
        list: [
          'Уборка откосов, подоконников;',
          'Чистка москитных сеток.'
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на мойку окон',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('window_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('window_cleaning'),
    },
    items: customerCleaningReviews('window_cleaning'),
    turboItems: customerCleaningTurboReviews('window_cleaning', 5),
    service_type: 'window_cleaning',
    className: 'with_btn'
  },
  faq: {
    title: 'Вопрос-ответ:',
    items: [
      {
        title: 'Какие моющие средства и инвентарь используются, чтобы помыть окна?',
        text: 'Мы используем профессиональную химию для уборки и мойки окон. Наши клинеры обучены правильному нанесению и удалению чистящих средств.<br>Мы используем:',
        list: [
          'Моющее средство для стёкол;',
          'Универсальное моющее средство для рам, откосов, подоконников и москитных сеток;',
          'Складное ведро;',
          'Телескопическую штангу;',
          'Шубку, скребок и сгон;',
          'Салфетки и губки;',
        ]
      },
      {
        title: 'Есть ли услуга по мойке балкона?',
        text: 'Да, конечно. Наши сотрудники не моют отдельно балконное остекление. Для балкона вы можете заказать услугу «Окна и балконы».',
        list: []
      },
      {
        title: 'Сколько стоит услуга мытья окон?',
        text: 'Стоимость определятся для каждого конкретного помещения, и зависит от степени загрязнения, размера окон и сложности. Более подробные цены вы сможете найти в нашем прайсе. Также предусмотрен выезд специалиста  для предварительного расчета цены услуги.',
        list: []
      },
      {
        title: 'Как я могу заказать мойку окон?',
        text: 'Оформить заказ на мойку окон можно по телефону или оставить заявку онлайн на сайте и наш менеджер с вами свяжется.',
        list: []
      },
    ],
    className: ''
  },
  otherServices: {
    title: 'Другие услуги по мойке окон',
    items: otherServicesService('window_cleaning', '/uslugi/moyka-okon'),
    className: 'even-section'
  },
  prices: {
    title: 'Стоимость мытья окон',
    description: 'Цена не зависит от количества окон в квартире',
    items: pricesService('window_cleaning'),
    type: 'window',
    className: '',
    url: '/uslugi/moyka-okon'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Мойка окон'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Вместе с мойкой окон можно заказать сопутствующую уборку в каждой конкретной квартире или в отдельно взятой комнате. Окна моются снаружи и изнутри в обязательном порядке. Услуги по мойке окон оказываются независимо от времени года и температурного режима, но в холодное время года чистка и мойка окон и комплектующих производится только с внутренней стороны.</p>
                    <p>Услуги профессиональных специалистов стоят недорого, заявку легко оформить в любое удобное время, итоговая цена зависит от того, какой перечень услуг Вы выбрали. Специалисты по клинингу проводят работы по мойке окон любой сложности быстро и эффективно, а также с гарантией качества оказанных услуг.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>

        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const moykaOkon:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 22 Jul 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboPrices(data.prices)}
          ${turboReviews(data.reviews, data.settings.url)}
          ${turboFaq(data.faq)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Вместе с мойкой окон можно заказать сопутствующую уборку в каждой конкретной квартире или в отдельно взятой комнате. Окна моются снаружи и изнутри в обязательном порядке. Услуги по мойке окон оказываются независимо от времени года и температурного режима, но в холодное время года чистка и мойка окон и комплектующих производится только с внутренней стороны.</p>
                    <p>Услуги профессиональных специалистов стоят недорого, заявку легко оформить в любое удобное время, итоговая цена зависит от того, какой перечень услуг Вы выбрали. Специалисты по клинингу проводят работы по мойке окон любой сложности быстро и эффективно, а также с гарантией качества оказанных услуг.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
