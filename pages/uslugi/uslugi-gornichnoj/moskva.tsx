import React, { Component, ReactElement } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';
import * as AmpHelpers from 'react-amphtml/helpers';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { OtherPagesComponent } from '../../../styles/otherPagesComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer from '../../../components/Footer/Footer';
import Breadcrumbs from '../../../components/Breadcrumbs/Breadcrumbs';
import GetStarted from '../../../components/Content/GetStarted/GetStarted';
import Professionals from '../../../components/Content/Professionals/Professionals';
import RecentJobs from '../../../components/Content/RecentJobs/RecentJobs';
import Statistics from '../../../components/Content/Statistics/Statistics';
import Guarantee from '../../../components/Content/Guarantee/Guarantee';
import WantToWork from '../../../components/Content/WantToWork/WantToWork';

import { apartmentTypes, maxApartmentNumber } from '../../../constants/types.constants';
import { recentJobsHomeCleaning } from '../../../constants/recentJobs.constants'
import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';

const apartmentType = 0;

const data:any = {
  howWeWorking: {
    title: 'Как мы работаем',
    items: [
      {
        icon: {
          src: '/static/images/setup_plan_icon.svg',
          alt: 'Handy plan'
        },
        title: 'Составьте план уборки',
        text: 'Выберите еженедельный, двухнедельный или ежемесячный план уборки. Мы планируем ваши повторяющиеся бронирования, чтобы упростить задачу, но не волнуйтесь, вы всегда можете перенести это, если что-то изменится.'
      },
      {
        icon: {
          src: '/static/images/manage_online_icon.svg',
          alt: 'Handy plan'
        },
        title: 'Управляйте всем онлайн',
        text: 'Перенести график, связаться со своим профессионалом и дать чаевые - одним касанием.'
      },
      {
        icon: {
          src: '/static/images/pro_female_icon.svg',
          alt: 'Handy plan'
        },
        title: 'Устройтесь поудобнее и расслабьтесь',
        text: 'Опытный, полностью оборудованный специалист по уборке прибудет вовремя.'
      }
    ],
    className: 'even-section'
  },
  professionals: {
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('apartment_cleaning'),
    className: ''
  },
  statistics: {
    title: 'Статистика по Москве',
    items: [
      {
        title: 'Активные профессионалы в Москве',
        count: '234'
      },
      {
        title: 'Отзывы о услугах горничной в Москве',
        count: '43'
      },
      {
        title: 'Положительные отзывы',
        count: '85%'
      }
    ],
    className: ''
  },
  guarantee: {
    title: 'Статистика по Москве',
    description: 'Ваше счастье - наша цель. Если вы недовольны, <a class="media-card__link" href="/guarantee"> мы приложим все усилия, чтобы исправить это </a>.',
    className: ''
  },
  wantToWork: {
    title: 'Хотите работать с нами?',
    description: 'Зарабатывайте до 6000р в час. Наши лучшие профессионалы зарабатывают более 100000 р в неделю. Вы сами выбираете, когда и сколько вы хотите работать. Постройте свой собственный график.',
    btn: 'Начать',
    className: ''
  },
  reliable: {
    title: 'Самое надежное имя в сфере уборки дома',
    subTitle: 'Вот как это работает: просто зайдите на наш сайт или в приложение и сообщите нам свой почтовый индекс и размер вашего дома, квартиры или квартиры. Затем Handy свяжет вас с высококлассным специалистом по уборке дома, который поможет снять стресс. из твоей жизни.',
    items: [
      {
        icon: {
          src: '/static/images/experienced_icon.svg',
          alt: 'Handy plan'
        },
        title: 'Опытный',
        text: 'С 2012 года Handy предоставляет людям услуги по профессиональной уборке и другим домашним услугам. За свою короткую историю профессионалы по уборке дома с помощью платформы Handy помогли очистить миллионы домов и квартир. в Нью-Йорке, Сан-Франциско, Бостоне, Чикаго, Лос-Анджелесе, Лондоне и других городах. Handy - это универсальный магазин, где вам нужна профессиональная помощь по уборке дома.'
      },
      {
        icon: {
          src: '/static/images/reliable_icon.svg',
          alt: 'Handy plan'
        },
        title: 'Надежный',
        text: 'Когда вы назначаете услуги уборщицы или горничной через Handy, чтобы они приходили к вам домой, вы можете не сомневаться, зная, что кто-то будет в вашем доме и готов к работе в указанное вами время. Помогут вернуть твои спальни, ванные комнаты, кухня, гостиная и многое другое в совершенно новом состоянии, которое вы помните, когда впервые въехали!'
      },
      {
        icon: {
          src: '/static/images/convenient_icon.svg',
          alt: 'Handy plan'
        },
        title: 'Удобный',
        text: 'Поздние ночи на работе мешают вам поддерживать порядок в квартире? Вы слишком заняты тем, что водите детей в школу, на тренировки и на игровые вечеринки, чтобы пылесосить пол? Мы знаем, что жизнь может быть сумасшедшей и непредсказуемой, и когда вы устали и перегружены работой, последнее, что кто-то хочет делать, это убирать свой дом. Какой бы ни была причина, по которой вы ищете помощь по уборке дома, Handy поможет вам.'
      },
      {
        icon: {
          src: '/static/images/flexible_icon.svg',
          alt: 'Handy plan'
        },
        title: 'Гибкий',
        text: 'С Handy легко планировать и переносить бронирования. Просто откройте приложение Handy и выберите наиболее удобные для вас дату и время. Или, может быть, вашему дому нужна особо глубокая уборка с особым вниманием - вы можете использовать приложение, чтобы добавить к бронированию дополнительные услуги, такие как стирка, в шкафах, в холодильнике и т. д. Сделайте Handy вашим поставщиком услуг по уборке №1, если вам нужна уборка дома, Услуги горничной в Москвеы или горничная. сервис или больше!'
      }
    ],
    className: 'with_btn'
  },
  included: {
    title: 'Что входит в уборку дома?',
    description: 'Вот что вы можете ожидать от уборки дома от профессионалов Handy. Загрузите приложение, чтобы поделиться подробностями и инструкциями по очистке!',
    items: [
      {
        icon: {
          src: '/static/images/bedroom.webp',
          alt: 'Handy plan'
        },
        title: 'Спальня, гостиная и Общие области',
        description: 'Платформа Handy соединяет вас с лучшими специалистами по уборке квартир в вашем районе. Компания Handy была основана в 2012 году и позволила миллионам клиентов познакомиться с дешевыми профессионалами по уборке квартир, которые используются на регулярной основе. Однако ваши потребности могут быть другими. Вместо обычной уборки вам могут потребоваться услуги по уборке квартиры только время от времени. Возможно, вам нужно подготовиться к вечеринке, а у вас нет времени на уборку ванной. Возможно, вечеринка произошла вчера вечером, и вы столкнулись с большим беспорядком, чем вы ожидали!',
        list: []
      },
      {
        icon: {
          src: '/static/images/bedroom.webp',
          alt: 'Handy plan'
        },
        title: 'Уборка ванной',
        description: '',
        list: [
          'Вымойте и продезинфицируйте туалет, душ, ванну и раковину.',
          'Удалите пыль со всех доступных поверхностей',
          'Протрите все зеркала и стеклянную фурнитуру.',
          'Очистите все поверхности пола.',
          'Вывоз мусора и переработка'
        ]
      },
      {
        icon: {
          src: '/static/images/bedroom.webp',
          alt: 'Handy plan'
        },
        title: 'Уборка кухни',
        description: '',
        list: [
          'Удалите пыль со всех доступных поверхностей',
          'Слейте воду из раковины и загрузите в посудомоечную машину грязную посуду',
          'Протрите внешнюю поверхность плиты, духовки и холодильника.',
          'Очистите все поверхности пола.',
          'Вывоз мусора и переработка'
        ]
      },
      {
        icon: {
          src: '/static/images/bedroom.webp',
          alt: 'Handy plan'
        },
        title: 'Дополнительно',
        description: 'Для более глубокой очистки рассмотрите возможность добавления одной или нескольких дополнительных чистящих средств. Большинство дополнительных услуг по уборке добавляют к вашему бронированию полчаса времени и стоимость.',
        list: [
          'Внутри шкафов',
          'Внутри холодильника',
          'Внутри духовки',
          'Стирка и стирка белья &amp; сухой',
          'Внутренние окна'
        ]
      }
    ],
    className: 'even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Как можно заказать уборку?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы делаем уборку в квартирах разной планировки, площади и количества комнат, в частных домах, в офисах, везде, где требуются наши услуги.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится уборка?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка в квартире или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  recentJobs: {
    title: 'Недавние запросы в Москве',
    items: recentJobsHomeCleaning
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Услуги горничной в Москве'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>Услуги горничной в Москве</title>
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <OtherPagesComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <div
                className="cell service-image"
                style={{backgroundImage: 'url(/static/images/home-cleaning-bg.jpg)'}}
              ></div>
            </div>

          </div>

          {/* State */}
          <Amp.AmpState specName="amp-state" id="apartmentTypes">{apartmentTypes}</Amp.AmpState>

          {/* head */}
          <section className="head-container">
            <div className="grid-container">
              <div className="grid-x align-center head">
                <div className="grid-x head">
                  <div className="cell small-12 large-7">
                    <div className="cell head__title">
                      <h1>Услуги горничной в Москве</h1>
                    </div>

                    <div className="cell grid-x how-it-works__media-cards-container">
                      {
                        data.howWeWorking.items.map((item:any) => (
                          <div
                            key={Buffer.from(Math.random().toString()).toString('base64')}
                            className="media-object align-middle media-card"
                          >
                            <div className="media-object-section media-card__logo">
                              <Amp.AmpImg
                                specName="default"
                                key={Buffer.from(Math.random().toString()).toString('base64')}
                                src={item.icon.src}
                                className="img"
                                layout="flex-item"
                                alt={item.icon.alt}
                              />
                            </div>
                            <div className="media-object-section main-section">
                              <h3>{item.title}</h3>
                              <p dangerouslySetInnerHTML={{ __html: item.text }} />
                            </div>
                          </div>
                        ))
                      }
                    </div>

                  </div>
                  <div className="cell small-12 large-offset-1 large-4">
                    <div className="cell grid-x">
                      <div className="quote-form-container">
                        <div>
                          <Amp.Form
                            specName="FORM [method=GET]"
                            className="quote-form"
                            acceptCharset="UTF-8"
                            action-xhr="/api/form"
                            action="/api/form"
                            method="GET"
                            target="_blank"
                            custom-validation-reporting="show-first-on-submit"
                            id="headerForm"
                            on="submit-success: headerForm.clear, headerFormSuccess.toggleClass(class='-visible'); submit-error: headerFormError.toggleClass(class='-visible')"
                          >
                            <div className="grid-container">
                              <div className="grid-x grid-padding-x">
                                <h3>Услуги горничной в Москве</h3>
                                <div className="cell">

                                  <div id="headerFormError" className="small-12 columns error">
                                    <i className="fab fa fa-warning"></i>
                                    При отправки вашей заявки произошла ошибка!
                                  </div>

                                  <div id="headerFormSuccess" className="small-12 columns success">
                                    <i className="fal fa-check"></i>
                                    Ваша заявка успешно отправлена!
                                  </div>

                                  <div className="cell small-12">
                                    <input
                                      placeholder="Имя"
                                      type="text"
                                      name="name"
                                      required
                                    />
                                  </div>

                                  <div className="cell small-12">
                                    <Amp.Input
                                      specName="default"
                                      placeholder="Телефон"
                                      type="tel"
                                      name="phone"
                                      mask="+\7_(000)_000-00-00"
                                      mask-output="alphanumeric"
                                      required
                                    />
                                  </div>

                                  <div className="cell small-12">
                                    <input
                                      placeholder="E-mail"
                                      type="email"
                                      name="email"
                                    />
                                  </div>

                                  <h4>Расскажи нам о своем месте</h4>
                                  <div>
                                    <div className="cell grid-x small-12 quote-form__details">
                                      <Amp.Span
                                        className="cell small-2 decrement"
                                        role="button"
                                        tabIndex={0}
                                        on="tap:AMP.setState({ apartmentType : ((apartmentType <= 0) ? 0 : apartmentType - 1) })"
                                      >−</Amp.Span>
                                      <AmpHelpers.Bind text="apartmentTypes[apartmentType].name">
                                        {(props): ReactElement => <span className="cell auto description" {...props} >{apartmentTypes[apartmentType].name}</span>}
                                      </AmpHelpers.Bind>
                                      <Amp.Span
                                        className="cell small-2 increment"
                                        role="button"
                                        tabIndex={0}
                                        on={`tap:AMP.setState({ apartmentType : ((apartmentType < ${maxApartmentNumber}) ? apartmentType + 1 : ${maxApartmentNumber}) })`}
                                      >+</Amp.Span>
                                      <AmpHelpers.Bind value="apartmentType">
                                        {(props): ReactElement => <input type="hidden" name="apartmentType" value={apartmentType} {...props} />}
                                      </AmpHelpers.Bind>
                                    </div>
                                  </div>
                                  <div className="quote-form__questions"></div>
                                  <div>
                                    <h5>Мы рекомендуем&nbsp;
                                      <AmpHelpers.Bind text="apartmentTypes[apartmentType].hours">
                                        {(props): ReactElement => <span {...props}>{apartmentTypes[apartmentType].hours}</span>}
                                      </AmpHelpers.Bind>
                                    </h5>
                                    <div className="cell small-12 select-wrapper">
                                      <select
                                        name="hours"
                                      >
                                        {
                                          apartmentTypes.map((type:any) => (
                                            <option key={Buffer.from(Math.random().toString()).toString('base64')} value={type.hours}>{type.hours}</option>
                                          ))
                                        }
                                      </select>
                                    </div>
                                  </div>
                                  <h5>Когда вы хотите, чтобы мы приехали?</h5>
                                  <div className="cell small-12">
                                    <div className="dropdown-date-selector">
                                      <Amp.AmpDatePicker
                                        specName="amp-date-picker[type=single][mode=overlay]"
                                        locale="ru"
                                        mode="overlay"
                                        type="single"
                                        layout="container"
                                        input-selector="[name=date_start]"
                                        format="ddd MMM DD"
                                        date="P0D"
                                      >
                                        <input type="text" name="date_start" placeholder="Дата"/>
                                      </Amp.AmpDatePicker>
                                    </div>
                                  </div>
                                  <div className="cell small-12">
                                    <input
                                      placeholder="Время"
                                      type="time"
                                      name="time_start"
                                      defaultValue="07:00"
                                    />
                                  </div>
                                </div>
                                <div className="cell small-12 quote-form__button">
                                  <button
                                    className="btn btn-primary"
                                    type="submit"
                                  >
                                    Узнать цену
                                  </button>
                                </div>
                              </div>
                            </div>
                          </Amp.Form>
                          <p className="grid-container quote-form-description">
                            Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </section>

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-12 align-center">
                  <div className="cell page_text__content">
                    <h2>Услуги горничной в Москве</h2>
                    <p>С 2012 года Handy предоставляет доступ к надежным, экономичным и высокопрофессиональным услугам горничной в Лондоне. Горничные, заказанные через платформу Handy, приходят к вам домой со всем своим оборудованием в руках.</p>
                    <p>Лучшие услуги горничной в Лондоне гарантируют, что даже в ваше отсутствие вы будете знать, что за вашим домом или квартирой хорошо ухаживают. Горничные, заказанные через Handy, могут в короткие сроки подготовить ваш дом к появлению гостей или просто для того, чтобы вы чувствовали себя расслабленно и комфортно в приятной и чистой обстановке.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section left' }} />

            </div>
          </div>

          {/* Statistics */}
          <Statistics data={data.statistics} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-12 align-center">
                  <div className="cell page_text__content">
                    <h2>Услуги горничной в Москве</h2>
                    <p>С 2012 года Handy предоставляет доступ к надежным, экономичным и высокопрофессиональным услугам горничной в Лондоне. Горничные, заказанные через платформу Handy, приходят к вам домой со всем своим оборудованием в руках.</p>
                    <p>Лучшие услуги горничной в Лондоне гарантируют, что даже в ваше отсутствие вы будете знать, что за вашим домом или квартирой хорошо ухаживают. Горничные, заказанные через Handy, могут в короткие сроки подготовить ваш дом к появлению гостей или просто для того, чтобы вы чувствовали себя расслабленно и комфортно в приятной и чистой обстановке.</p>

                    <h2>Услуги горничной в Москве</h2>
                    <p>С 2012 года Handy предоставляет доступ к надежным, экономичным и высокопрофессиональным услугам горничной в Лондоне. Горничные, заказанные через платформу Handy, приходят к вам домой со всем своим оборудованием в руках.</p>
                    <p>Лучшие услуги горничной в Лондоне гарантируют, что даже в ваше отсутствие вы будете знать, что за вашим домом или квартирой хорошо ухаживают. Горничные, заказанные через Handy, могут в короткие сроки подготовить ваш дом к появлению гостей или просто для того, чтобы вы чувствовали себя расслабленно и комфортно в приятной и чистой обстановке.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Recent Job */}
          <RecentJobs data={data.recentJobs} />

          {/* Guarantee */}
          <Guarantee data={data.guarantee} />

          {/* WantToWork */}
          <WantToWork data={data.wantToWork} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-12 align-center">
                  <div className="cell page_text__content">
                    <h2>Услуги горничной в Москве</h2>
                    <p>С 2012 года Handy предоставляет доступ к надежным, экономичным и высокопрофессиональным услугам горничной в Лондоне. Горничные, заказанные через платформу Handy, приходят к вам домой со всем своим оборудованием в руках.</p>
                    <p>Лучшие услуги горничной в Лондоне гарантируют, что даже в ваше отсутствие вы будете знать, что за вашим домом или квартирой хорошо ухаживают. Горничные, заказанные через Handy, могут в короткие сроки подготовить ваш дом к появлению гостей или просто для того, чтобы вы чувствовали себя расслабленно и комфортно в приятной и чистой обстановке.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

        {/* Other Services */}
          <section className="other_services">
            <div className="grid-container">
              <div className="grid-x align-center foot">
                <div className="cell grid-x grid-margin-x medium-margin-collapse grid-margin-y small-12 foot__cards-container cards">
                    <div className="cell cards__title">
                      <h3>Популярные услуги в Москве</h3>
                    </div>
                    <div className="cell large-auto cards__list">
                      <ul>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                      </ul>
                    </div>
                    <div className="cell large-auto cards__list">
                      <ul>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                      </ul>
                    </div>
                    <div className="cell large-auto cards__list">
                      <ul>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                        <li>
                          <p>
                            <a href="/uslugi/uslugi-gornichnoj/moskva">Услуги горничной в Москве</a>
                          </p>
                        </li>
                      </ul>
                    </div>
                    <div className="cell cards__link">
                      <a href="/uslugi/moskva">Больше услуг</a>
                    </div>

                </div>
              </div>
            </div>

            <br />
          </section>


        </OtherPagesComponent>

        {/* readcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};
