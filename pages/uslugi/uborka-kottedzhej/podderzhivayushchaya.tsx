import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { CenterFormComponent } from '../../../styles/centerFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Why, {turboWhy} from '../../../components/Content/Why/Why';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import OtherServices, {turboOtherServices} from '../../../components/Content/OtherServices/OtherServices';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';

import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';
import { otherServicesService } from '../../../constants/otherServices.constants';
import { pricesService } from '../../../constants/pricesServices.constants';

import CenterForm, {turboCenterForm} from "../../../components/Content/Forms/CenterForm/CenterForm";

const data:any = {
  settings: {
    title: 'Поддерживающая  уборка коттеджа заказать в компании My-cleaning',
    description: 'Поддерживающая уборка коттеджей от компании «my-cleaning». Закажите профессиональную уборку Вашего дома по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-kottedzhej-podderzhivayushchaya-bg.webp',
    name: 'Поддерживающая уборка коттеджей в Москве и Московской области',
    rating: customerCleaningRating('home_cleaning') / customerCleaningReviewsCount('home_cleaning'),
    url: '/uslugi/uborka-kottedzhej/podderzhivayushchaya',
    sku: 9,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: null,
      description: {
        title: null,
        text: [
          'Поддержание чистоты коттеджа или загородного дома  – постоянная потребность, продиктованная не только соображениями обязательной гигиены, но и необходимостью следить за внешним видом помещения. В особенной степени это относится к домам и коттеджам, где жилая и дополнительная площади достаточно велики по сравнению с обычными квартирами, и требуют не только больше времени для качественной уборки, но и специальных составов, профессионального оборудования, приспособлений и наличия большого количества свободного времени, которыми владельцы располагают далеко не всегда.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: true
    }
  },
  why: {
    title: 'Почему выбирают нас? ',
    description: '',
    items: [
      {
        text: 'Вам не нужно повторять однотипные заказы, а ваш коттедж всегда будет чистым и опрятным.'
      },
      {
        text: 'Вы не забудете о следующем визите наших сотрудников. Вам не придется звонить нам для того, чтобы передать важную информацию.'
      },
      {
        text: 'Вы доверяете уборку лучшим специалистам с профессиональным оборудованием. Благодаря этому вы получаете гарантию чистоты.'
      }
    ],
    className: 'with_btn'
  },
  included: {
    title: 'Что входит в поддерживающую уборку коттеджей?',
    description: 'Поддерживающая уборка дома сегодня – задача для лучших специалистов по уборке в Москве. Что входит в поддерживающую уборку коттеджей и загородных домов? Можно обозначить целый перечень профессиональных услуг:',
    items: [
      {
        icon: {
          src: '/static/images/included/uborka-kottedzhej-podderzhivayushchaya-1.webp',
          alt: 'Мытьё и чистка полов'
        },
        title: '',
        description: '',
        list: [
          'Мытьё и чистка полов во всех помещениях внутри дома независимо от площади и количества мебели;',
          'Ликвидация пыли и влажная уборка на всех поверхностях, включая мебель, технику, шкафы, зеркала;',
          'Чистка окон и дверей, стен, плинтусов, наружных фасадов зданий.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-kottedzhej-podderzhivayushchaya-2.webp',
          alt: 'Химчистка ковровых покрытий'
        },
        title: '',
        description: '',
        list: [
          'Химчистка ковровых покрытий, мебели, текстиля, штор, устранение пятен и неприятных запахов;',
          'Уборка придомовой территории, газонов, дорожек, уборка листьев или снега в зависимости от сезона, покос травы.'
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Как можно заказать поддерживающую уборку коттеджа?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы делаем уборку в квартирах разной планировки, площади и количества комнат, в частных домах, коттеджах, в офисах, везде, где требуются наши услуги.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится поддерживающая уборка коттеджа?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка в квартире или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('home_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку коттеджей',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('home_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('home_cleaning'),
    },
    items: customerCleaningReviews('home_cleaning'),
    turboItems: customerCleaningTurboReviews('home_cleaning', 5),
    service_type: 'home_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке коттеджей',
    items: otherServicesService('home_cleaning', '/uslugi/uborka-kottedzhej/podderzhivayushchaya'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на уборку коттеджей',
    description: '',
    items: pricesService('home_cleaning_gen'),
    type: 'premises',
    className: '',
    url: '/uslugi/uborka-kottedzhej/podderzhivayushchaya'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Поддерживающая уборка коттеджей'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <CenterFormComponent role="main">
          {/* header bg */}
          <meta itemProp="image" content={data.settings.bgImage}/>
          <div
            id="top"
            className="quote-bg row"
            style={{backgroundImage: `url(${data.settings.bgImage})`}}
          />

          {/* form */}
          <CenterForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* disclaimer */}
          <div className="disclaimer">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="cell small-12 small-centered xlarge-6">
                  Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                </div>
              </div>
            </div>
          </div>

          {/* head description */}
          <div className="head__description">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div itemProp="description" className="cell small-12 small-centered xlarge-8">
                  {
                    data.settings.form.description.text.map((item: string) => (
                      <p>{item}</p>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>

          {/* Why */}
          <Why data={data.why} />

          {/* Get Started */}
          <div className="grid-container">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'left' }} />
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Affordable Luxury */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Поддерживающая уборка коттеджей и домов производится сегодня силами клинеров нашей компании, предлагающей доступные цены, высокое качество работ и обширный перечень услуг. Закажите уборку у нас и получите возможность поддерживать чистоту регулярно и с выгодой для себя.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </CenterFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* readcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const homePodderzhivayushchaya:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 12 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboCenterForm({settings: data.settings, reviews: data.reviews})}
          ${turboWhy(data.why)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboPrices(data.prices)}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          <section class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Поддерживающая уборка коттеджей и домов производится сегодня силами клинеров нашей компании, предлагающей доступные цены, высокое качество работ и обширный перечень услуг. Закажите уборку у нас и получите возможность поддерживать чистоту регулярно и с выгодой для себя.</p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
