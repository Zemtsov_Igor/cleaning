import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { CenterFormComponent } from '../../../styles/centerFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Why, {turboWhy} from '../../../components/Content/Why/Why';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';

import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';

import CenterForm, {turboCenterForm} from "../../../components/Content/Forms/CenterForm/CenterForm";
import OtherServices, {turboOtherServices} from "../../../components/Content/OtherServices/OtherServices";
import {otherServicesService} from "../../../constants/otherServices.constants";
import {pricesService} from "../../../constants/pricesServices.constants";
import Prices from "../../../components/Content/Prices/Prices";

const data:any = {
  settings: {
    title: 'Ежедневная уборка офисов в Москве | my-cleaning',
    description: 'Ежедневная уборка офисов от компании «my-cleaning». Закажите клининг Вашего офиса  по доступной цене! Работаем без выходных. Наш телефон в Москве +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-ofisov-ezhednevnaya-bg.webp',
    name: 'Ежедневная уборка офисов',
    rating: customerCleaningRating('office_cleaning') / customerCleaningReviewsCount('office_cleaning'),
    url: '/uslugi/uborka-ofisov/ezhednevnaya',
    sku: 13,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: null,
      description: {
        title: null,
        text: [
          'Ежедневная уборка офисов специалистами нашей клининговой компанией  экономит время и силы ваших сотрудников, повышает престиж и репутацию компании в глазах партнеров и клиентов. Мы работаем по вашему графику и готовы идти навстречу, если вы захотите перенести или отменить клининг офиса в один из дней.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: true
    }
  },
  why: {
    title: 'Почему стоит заказать ежедневную уборку офисов?',
    description: '',
    items: [
      {
        text: 'Репутация и высокая скорость работы. В первые минуты клиенты и партнеры обращают внимание на «лицо» компании — ухоженный холл или приемную.  Заботу о состоянии офиса гости оценят по достоинству, ведь куда приятнее вести переговоры в чистом помещении.'
      },
      {
        text: 'Экономия на зарплате. С заказом клининга офиса отпадает необходимость нанимать дополнительного сотрудника. Соответственно, если наш специалист заболеет или не приедет — мы заменяем его. В любом случае работа будет выполнена качественно и в срок.'
      },
      {
        text: 'Экономия на налогах. Дополнительный сотрудник увеличивает нагрузку на бюджет компании. Мы  говорим  про страховку, отчисления в пенсионный фонд и итоговую сумму налогов.'
      },
      {
        text: 'Качество и безопасность. В процессе выполнения уборки наши клинеры используют современные и безвредные средства. Они эффективны, легко справляются с пылью и другими загрязнениями, не вызывают аллергических реакций.'
      },
      {
        text: 'Постоянная цена. При заказе  клининга офиса у нас мы обговариваем стоимость и фиксируем ее в договоре. Более того, если вас не устроит финальный результат, то повторную уборку мы осуществляем за свой  счет.'
      }
    ],
    className: 'with_btn pad-top'
  },
  included: {
    title: 'Что входит в услугу ежедневной уборки офисов?',
    description: '',
    items: [
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-ezhednevnaya-1.webp',
          alt: 'Удаление пыли и грязи со всех поверхностей, в том числе зеркальных'
        },
        title: '',
        description: '',
        list: [
          'Удаление пыли и грязи со всех поверхностей, в том числе зеркальных;',
          'Очистка офисной техники;',
          'Дезинфекция помещения.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-ezhednevnaya-2.webp',
          alt: 'Вынос мусора'
        },
        title: '',
        description: '',
        list: [
          'Вынос мусора;',
          'Мытье окон и подоконников;',
          'Уборка в обеденной зоне (буфете, столовой и тд).'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-ezhednevnaya-3.webp',
          alt: 'Мытье раковин и туалетов'
        },
        title: '',
        description: '',
        list: [
          'Мытье раковин и туалетов;',
          'Удаление пятен и пыли с розеток.'
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Вопрос-ответ:',
    items: [
      {
        title: 'По графику клининг офиса стоит через 3 дня, но в этот день проверка. Могу ли я перенести уборку?',
        text: 'Да, конечно. Вы можете заказать уборку на другой день или вовсе ее отменить. Чтобы избежать штрафных санкций за отмену клининга, рекомендуем оповестить нас не позднее, чем за сутки.',
        list: []
      },
      {
        title: 'Я отметил несколько сотрудников, работа которых мне особенно понравилась. Могу ли я дополнительно их вознаградить?',
        text: 'Да, конечно. Наша клининговая компания не берет проценты с чаевых и ваше признание в денежном эквиваленте наш сотрудник получит полностью. Мы заметили интересную деталь: сотрудники, которым давали чаевые в прошлые их визиты, подстраивают свой график работы под ваш план работы.',
        list: []
      }
    ],
    className: ''
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('office_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку офисов',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('office_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('office_cleaning')
    },
    items: customerCleaningReviews('office_cleaning'),
    turboItems: customerCleaningTurboReviews('office_cleaning', 5),
    service_type: 'office_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке офисов',
    items: otherServicesService('office_cleaning', '/uslugi/uborka-ofisov/ezhednevnaya'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на уборку офисов и коммерческих помещений в Москве и Московской области',
    description: '',
    items: pricesService('office_cleaning'),
    type: 'office',
    className: '',
    url: '/uslugi/uborka-ofisov/ezhednevnaya'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Ежедневная уборка офисов'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <CenterFormComponent role="main">
          {/* header bg */}
          <meta itemProp="image" content={data.settings.bgImage}/>
          <div
            id="top"
            className="quote-bg row"
            style={{backgroundImage: `url(${data.settings.bgImage})`}}
          />

          {/* form */}
          <CenterForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* disclaimer */}
          <div className="disclaimer">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="cell small-12 small-centered xlarge-6">
                  Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                </div>
              </div>
            </div>
          </div>

          {/* head description */}
          <div className="head__description">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div itemProp="description" className="cell small-12 small-centered xlarge-8">
                  {
                    data.settings.form.description.text.map((item: string) => (
                      <p>{item}</p>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>

          {/* Why */}
          <Why data={data.why} />

          {/* Get Started */}
          <div className="grid-container">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'left' }} />
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Affordable Luxury */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Ежедневная уборка офисов — услуга, которая экономит ваши деньги и время. При необходимости вы можете заказать услугу генеральной  уборки. Если у вас в компании недавно был строительные работы, а мусор и пыль остались — рекомендуем заказать у нас услугу клининга после ремонта.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </CenterFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const ofisovEzhednevnaya:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 12 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboCenterForm({settings: data.settings, reviews: data.reviews})}
          ${turboWhy(data.why)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Ежедневная уборка офисов — услуга, которая экономит ваши деньги и время. При необходимости вы можете заказать услугу генеральной  уборки. Если у вас в компании недавно был строительные работы, а мусор и пыль остались — рекомендуем заказать у нас услугу клининга после ремонта.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
