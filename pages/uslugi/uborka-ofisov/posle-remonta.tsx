import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { CenterFormComponent } from '../../../styles/centerFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';

import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';

import {otherServicesService} from "../../../constants/otherServices.constants";
import CenterForm, {turboCenterForm} from "../../../components/Content/Forms/CenterForm/CenterForm";
import OtherServices, {turboOtherServices} from "../../../components/Content/OtherServices/OtherServices";
import {pricesService} from "../../../constants/pricesServices.constants";
import Prices from "../../../components/Content/Prices/Prices";

const data:any = {
  settings: {
    title: 'Уборка офисов после ремонта | Клининговая компания my-cleaning',
    description: 'Уборка офисов после ремонта от компании «my-cleaning». Закажите профессиональную уборку Вашего офиса по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-ofisov-posle-remonta-bg.webp',
    name: 'Уборка офиса после ремонта',
    rating: customerCleaningRating('office_cleaning') / customerCleaningReviewsCount('office_cleaning'),
    url: '/uslugi/uborka-ofisov/posle-remonta',
    sku: 16,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: null,
      description: {
        title: null,
        text: [
          'Размещение в новом офисном помещении после ремонта или окончания строительных работ занимает достаточно большое количество времени, учитывая необходимость предварительно провести уборку помещений и ликвидировать строительный мусор. Перечень услуг современной профессиональной уборки содержит возможность быстро и качественно реализовать уборку любого офисного пространства независимо от его габаритов и особенностей.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: true
    }
  },
  included: {
    title: 'Что входит в уборку офиса после ремонта?',
    description: 'Уборка офиса после ремонта в Москве от специалистов клининговой компании подразумевает:',
    items: [
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-posle-remonta-1.webp',
          alt: 'Вывоз и уборку строительного мусора'
        },
        title: '',
        description: '',
        list: [
          'Вывоз и уборку строительного мусора в крупных и мелких фракциях;',
          'Обработку всех горизонтальных и вертикальных поверхностей от налёта строительной пыли;',
          'Чистку мебели, техники, стен и дверей от пятен краски, лака, цемента и других составов, от следов скотча и плёнки, других повреждений.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-posle-remonta-2.webp',
          alt: 'Мытьё окон'
        },
        title: '',
        description: '',
        list: [
          'Мытьё окон, подоконников, плинтусов, стен и внутренних перегородок;',
          'Чистку и дезинфекцию туалетов, ванных комнат, кафеля, сантехники;',
          'Обработку потолка, осветительных приборов, стёкол, зеркал.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-posle-remonta-3.webp',
          alt: 'Чистка и обработка от запаха ковровых покрытий и ковролина'
        },
        title: '',
        description: '',
        list: [
          'Чистка и обработка от запаха ковровых покрытий и ковролина;',
          'Итоговая уборка и мытьё полов, обработка обеззараживающими составами.'
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Как можно заказать уборку?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы делаем уборку в квартирах разной планировки, площади и количества комнат, в частных домах, в офисах, везде, где требуются наши услуги.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится уборка?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка в квартире или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('office_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку офисов',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('office_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('office_cleaning')
    },
    items: customerCleaningReviews('office_cleaning'),
    turboItems: customerCleaningTurboReviews('office_cleaning', 5),
    service_type: 'office_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке офисов',
    items: otherServicesService('office_cleaning', '/uslugi/uborka-ofisov/posle-remonta'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на уборку офисов и коммерческих помещений в Москве и Московской области',
    description: '',
    items: pricesService('office_cleaning'),
    type: 'office',
    className: '',
    url: '/uslugi/uborka-ofisov/posle-remonta'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Уборка офиса после ремонта'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <CenterFormComponent role="main">
          {/* header bg */}
          <meta itemProp="image" content={data.settings.bgImage}/>
          <div
            id="top"
            className="quote-bg row"
            style={{backgroundImage: `url(${data.settings.bgImage})`}}
          />

          {/* form */}
          <CenterForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* disclaimer */}
          <div className="disclaimer">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="cell small-12 small-centered xlarge-6">
                  Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                </div>
              </div>
            </div>
          </div>

          {/* head description */}
          <div className="head__description">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div itemProp="description" className="cell small-12 small-centered xlarge-8">
                  {
                    data.settings.form.description.text.map((item: string) => (
                      <p>{item}</p>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Affordable Luxury */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Квалифицированные специалисты нашей клининговой компании предлагают клиентам низкие цены, комплексное формирование стоимости уборки в зависимости от индивидуальных параметров, объёма работ и сложности загрязнений, оставшихся после окончания ремонта.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </CenterFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const ofisovPosleRemonta:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 12 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboCenterForm({settings: data.settings, reviews: data.reviews})}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Квалифицированные специалисты нашей клининговой компании предлагают клиентам низкие цены, комплексное формирование стоимости уборки в зависимости от индивидуальных параметров, объёма работ и сложности загрязнений, оставшихся после окончания ремонта.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
