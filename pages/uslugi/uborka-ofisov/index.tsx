import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { CenterFormComponent } from '../../../styles/centerFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import HowWeWorking, {turboHowWeWorking} from '../../../components/Content/HowWeWorking/HowWeWorking';
import Why, {turboWhy} from '../../../components/Content/Why/Why';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import OtherServices, {turboOtherServices} from '../../../components/Content/OtherServices/OtherServices';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';

import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';
import { otherServicesService } from '../../../constants/otherServices.constants';
import { pricesService } from '../../../constants/pricesServices.constants';

import CenterForm, {turboCenterForm} from "../../../components/Content/Forms/CenterForm/CenterForm";

const data:any = {
  settings: {
    title: 'Уборка офисов в Москве и Московской области от компании My-cleaning',
    description: 'Клининг офисов в Москве от компании «my-cleaning». Закажите профессиональную уборку Вашего офиса по доступной цене! Работаем без выходных. Наш телефон в Москве +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-ofisov-bg.webp',
    name: 'Уборка офисов',
    rating: customerCleaningRating('office_cleaning') / customerCleaningReviewsCount('office_cleaning'),
    url: '/uslugi/uborka-ofisov',
    sku: 12,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: null,
      description: {
        title: null,
        text: [
          'Профессиональный клининг офисов — отличная возможность повысить продуктивность сотрудников и поднять авторитет в глазах партнеров и клиентов. В компании My Cleaning работают опытные специалисты, которые обеспечат чистоту и порядок для вас и ваших сотрудников.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Заказать уборку',
      place: true
    }
  },
  howWeWorking: {
    title: 'Как мы работаем?',
    items: [
      {
        icon: {
          src: '/static/images/setup_plan_icon.svg',
          alt: 'Индивидуальный график профессиональной уборки Вашего офиса'
        },
        title: 'Индивидуальный график профессиональной уборки Вашего офиса',
        text: 'Выбирайте удобный для вас день для клининга офиса. Также, если вы решите переместить или пропустить клининг, мы пойдем навстречу.'
      },
      {
        icon: {
          src: '/static/images/manage_online_icon.svg',
          alt: 'Онлайн-управление'
        },
        title: 'Онлайн-управление',
        text: 'Если вам неудобно присутствовать при уборке офиса лично, мы предлагаем следить за процессом клининга с телефона, планшета или ноутбука — онлайн.'
      },
      {
        icon: {
          src: '/static/images/pro_female_icon.svg',
          alt: 'Заказывайте услуги уборки офиса необходимые Вам'
        },
        title: 'Заказывайте услуги уборки офиса необходимые Вам',
        text: 'Вы можете выбрать ежедневную, поддерживающую или генеральную оборку офиса на нашем сайте, а также Вы можете заказать уборку офиса после ремонта.'
      }
    ],
    className: ''
  },
  why: {
    title: 'Причины заказать уборку офисов в клининговой компании my-cleaning?',
    description: '',
    items: [
      {
        text: 'Выполнение услуг по уборке офисных помещений только на профессиональном оборудовании;'
      },
      {
        text: 'Профессиональные чистящие средства;'
      },
      {
        text: 'Профессиональная команда клинеров;'
      },
      {
        text: 'Соблюдение мер безопасности для защиты от Covid-19;'
      },
      {
        text: 'Выгодные цены и специальные предложения для постоянных клиентов;'
      },
      {
        text: 'Осуществление уборки офиса любой сложности, в том числе после ремонта;'
      }
    ],
    className: 'with_btn'
  },
  included: {
    title: 'Что входит в услугу по клинингу офисов?',
    description: '',
    items: [
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-1.webp',
          alt: 'Протирка зеркал и поверхностей из стекла'
        },
        title: '',
        description: '',
        list: [
          'Протирка зеркал и поверхностей из стекла;',
          'Удаление пыли и микробов с люстр и других источников света;',
          'Сбор и вынос мусора.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-ofisov-2.webp',
          alt: 'Влажная уборка полов, плинтусов и дверных ручек'
        },
        title: '',
        description: '',
        list: [
          'Влажная уборка полов, плинтусов и дверных ручек;',
          'Чистка ковров.'
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Вопрос-ответ:',
    items: [
      {
        title: 'По графику клининг офиса стоит через 3 дня, но в этот день проверка. Могу ли я перенести уборку?',
        text: 'Да, конечно. Вы можете заказать уборку на другой день или вовсе ее отменить. Чтобы избежать штрафных санкций за отмену клининга, рекомендуем оповестить нас не позднее, чем за сутки.',
        list: []
      },
      {
        title: 'Я отметил несколько сотрудников, работа которых мне особенно понравилась. Могу ли я дополнительно их вознаградить?',
        text: 'Да, конечно. Наша клининговая компания не берет проценты с чаевых и ваше признание в денежном эквиваленте наш сотрудник получит полностью. Мы заметили интересную деталь: сотрудники, которым давали чаевые в прошлые их визиты, подстраивают свой график работы под ваш план работы.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      }
    ],
    className: ''
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('office_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку офисов',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('office_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('office_cleaning')
    },
    items: customerCleaningReviews('office_cleaning'),
    turboItems: customerCleaningTurboReviews('office_cleaning', 5),
    service_type: 'office_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке офисов',
    items: otherServicesService('office_cleaning', '/uslugi/uborka-ofisov'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на уборку офисов и коммерческих помещений в Москве и Московской области',
    description: '',
    items: pricesService('office_cleaning'),
    type: 'office',
    className: '',
    url: '/uslugi/uborka-ofisov'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Уборка офисов'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <CenterFormComponent role="main">
          {/* header bg */}
          <meta itemProp="image" content={data.settings.bgImage}/>
          <div
            id="top"
            className="quote-bg row"
            style={{backgroundImage: `url(${data.settings.bgImage})`}}
          />

          {/* form */}
          <CenterForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* disclaimer */}
          <div className="disclaimer">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="cell small-12 small-centered xlarge-6">
                  Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                </div>
              </div>
            </div>
          </div>

          {/* head description */}
          <div className="head__description">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div itemProp="description" className="cell small-12 small-centered xlarge-8">
                  {
                    data.settings.form.description.text.map((item: string) => (
                      <p>{item}</p>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>

          {/* works */}
          <HowWeWorking data={data.howWeWorking} />

          {/* Why */}
          <Why data={data.why} />

          {/* Get Started */}
          <div className="grid-container">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'left' }} />
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

        </CenterFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const ofisov:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 12 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboCenterForm({settings: data.settings, reviews: data.reviews})}
          ${turboHowWeWorking(data.howWeWorking)}
          ${turboWhy(data.why)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboPrices(data.prices)}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
