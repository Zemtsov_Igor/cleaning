import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../styles/grid'
import { GlobalStyle } from '../../styles/global'
import { RightFormComponent } from '../../styles/rightFormComponent.Styles';

import MainLayout from '../../components/Layouts/MainLayout/MainLayout';
import Header from '../../components/Header/Header';
import Footer, {turboFooter} from '../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../components/Breadcrumbs/Breadcrumbs';
import Faq, {turboFaq} from '../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../components/Content/GetStarted/GetStarted';
import Prices, {turboPrices} from '../../components/Content/Prices/Prices';

import { pricesService } from '../../constants/pricesServices.constants';
import RightForm, {turboRightForm} from "../../components/Content/Forms/RightForm/RightForm";
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount, customerCleaningTurboReviews
} from "../../constants/customerReviews.constants";
import Reviews from "../../components/Content/Reviews/Reviews";

const data:any = {
  settings: {
    title: 'Дезинфекция квартир в компании my-cleaning',
    description: 'Дезинфекция квартир, домов, офисов от компании «my-cleaning». Закажите профессиональную дезинфекцию по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/dezinfekciya-bg.webp',
    name: 'Дезинфекция квартир, домов и помещений',
    rating: customerCleaningRating('apartment_cleaning') / customerCleaningReviewsCount('apartment_cleaning'),
    url: '/uslugi/dezinfekciya',
    sku: 26,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Дезинфекция квартир, домов и помещений',
      description: {
        title: null,
        text: [
          'Для чего нужна дезинфекция квартиры или дома и почему ее нельзя заменить на обычную влажную уборку?'
        ],
        subText: []
      },
      capabilities: [
        'защита от вирусов. Сегодня очень важно снизить риск заболеваемости от Covid-19  и других инфекций;',
        'пополнение в семье. Перед приездом новорожденного специалисты рекомендуют проводить тщательную дезинфекцию квартиры или дома;',
        'наличие домашних животных. Если дома живут кошка, собака или другие питомцы с обильной линькой, возможно неожиданное проявление аллергической реакции. Снизить риск поможет дезинфекция квартиры, которую рекомендуется проводить хотя бы раз в квартал.'
      ],
      btnText: 'Заказать дезинфекцию',
      place: false
    }
  },
  faq: {
    title: 'Вопрос-ответ',
    items: [
      {
        title: 'Грозит ли мне штраф, если я отменю дезинфекцию квартиры за 2 дня?',
        text: 'Нет. Вы можете отменить или перенести дезинсекцию и дератизацию без штрафа не позднее, чем за сутки.',
        list: []
      },
      {
        title: 'Я бы хотел дать чаевые вашему специалисту, есть ли у меня такая возможность?',
        text: 'Мы уверены, что специалисту будет приятна признательность с вашей стороны. Интересное наблюдение: если вы уже давали чаевые профессионалу, то он подстроит свой график работы под ваш план уборки. Ваша признательность в денежном эквиваленте не повлияет на качество оказанных услуг.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по Covid-19?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      }
    ],
    className: ''
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на дезинфекцию квартир',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('apartment_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('apartment_cleaning'),
    },
    items: customerCleaningReviews('apartment_cleaning'),
    turboItems: customerCleaningTurboReviews('apartment_cleaning', 5),
    service_type: 'apartment_cleaning',
    className: 'with_btn'
  },
  apartmentPrices: {
    title: 'Цены на уборку с дезинфекцией квартиры',
    description: '',
    items: pricesService('apartment_disinfection'),
    type: 'apartment_disinfection',
    className: '',
    url: '/uslugi/dezinfekciya'
  },
  homePrices: {
    title: 'Цены на уборку с дезинфекцией коттеджей, домой, офисов',
    description: '',
    items: pricesService('home_disinfection'),
    type: 'home_disinfection',
    className: '',
    url: '/uslugi/dezinfekciya'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Дезинфекция'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Prices */}
          <Prices data={data.apartmentPrices} />

          {/* Prices */}
          <Prices data={data.homePrices} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <h2>Проведение дезинфекции в Москве</h2>
                    <p>Самостоятельно провести дезинфекцию в квартире или доме невозможно. Этому мешает отсутствие знаний, специального моющего оборудования и профессиональных дезинфицирующих средств. Чтобы добиться отличного результата и привести в идеальный порядок помещение, стоит заказать эту услуги по клинингу и дезинфекции помещений у профессионалов в клининговой компании My-cleaning . Мы выполняем услуги дезинфекции помещений, как жилых, так и коммерческих по доступным ценам.</p>
                    <p>В процессе работы применяются только безопасная химия, специализированное оборудование, современные подходы в уборке. Применяются не только антибактериальные, но и антисептические средства. Наши сотрудники имеют опыт в проведении работ разной сложности.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>

          {/* Reviews */}
          <Reviews data={data.reviews} />

        </RightFormComponent>

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const dezinfekciya:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 22 Jul 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings})}
          ${turboPrices(data.apartmentPrices)}
          ${turboPrices(data.homePrices)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <h2>Проведение дезинфекции в Москве</h2>
                    <p>Самостоятельно провести дезинфекцию в квартире или доме невозможно. Этому мешает отсутствие знаний, специального моющего оборудования и профессиональных дезинфицирующих средств. Чтобы добиться отличного результата и привести в идеальный порядок помещение, стоит заказать эту услуги по клинингу и дезинфекции помещений у профессионалов в клининговой компании My-cleaning . Мы выполняем услуги дезинфекции помещений, как жилых, так и коммерческих по доступным ценам.</p>
                    <p>В процессе работы применяются только безопасная химия, специализированное оборудование, современные подходы в уборке. Применяются не только антибактериальные, но и антисептические средства. Наши сотрудники имеют опыт в проведении работ разной сложности.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
        </main>
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
