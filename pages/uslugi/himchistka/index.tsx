import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import OtherServices, {turboOtherServices} from '../../../components/Content/OtherServices/OtherServices';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";

import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';
import { otherServicesService } from '../../../constants/otherServices.constants';
import { pricesService } from '../../../constants/pricesServices.constants';

const data:any = {
  settings: {
    title: 'Заказать химчистку на дому в компании My-cleaning',
    description: 'Химчистка на дому от компании «my-cleaning». Закажите профессиональную химчистку по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/himchistka-bg.webp',
    name: 'Химчистка на дому в Москве и Московской области',
    rating: customerCleaningRating('dry_cleaning') / customerCleaningReviewsCount('dry_cleaning'),
    url: '/uslugi/himchistka',
    sku: 17,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Химчистка',
      description: {
        title: null,
        text: [
          'Химчистка на дому в Москве на сегодняшний день пользуется растущим спросом. Самостоятельная чистка ковров, мягкой мебели, штор и многих других сложных покрытий требует от владельца большого количества свободного времени, применения специальных моющих средств и выполнения других условий, нецелесообразных при напряжённой работе и плотном графике.',
          'Заказать профессиональную химчистку у лучших специалистов – оптимальное решение, которое гарантированно поможет поддерживать все поверхности в должном внешнем и физическом состоянии.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: false
    }
  },
  included: {
    title: 'Что входит в химчистку?',
    description: 'Опытные специалисты в Москве предлагают услуги химчистки на дому для следующих поверхностей:',
    items: [
      {
        icon: {
          src: '/static/images/included/himchistka-1.webp',
          alt: 'Гарнитуров и наборов мягкой мебели'
        },
        title: '',
        description: '',
        list: [
          'Диванов и любой другой мягкой мебели;',
          'Любых отдельно взятых объектов при их загрязнении;',
          'Матрасов.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/himchistka-2.webp',
          alt: 'Напольных покрытий, ковров и ковролина'
        },
        title: '',
        description: '',
        list: [
          'Напольных покрытий, ковров и ковролина;',
          'Подушек, одеял, пледов, покрывал;',
          'Штор и занавесок.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/himchistka-3.webp',
          alt: 'Чистка мебели и ковров'
        },
        title: 'Дополнительные услуги',
        description: 'Чистка мебели и ковров при этом производится только с применением новейших безопасных для поверхности составов, сертифицированных и проверенных. Химчистка ковров реализуется в несколько этапов с обязательной проверкой критерия, не влияет ли состав на внешний вид, цвет и структуру напольного покрытия.',
        list: []
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Вопрос-ответ',
    items: [
      {
        title: 'Можно ли у вас заказать услугу по чистке жалюзи или штор?',
        text: 'Да, мы оказываем услуги по химчистке штор, занавесок, жалюзи, рулонных и римских штор. В зависимости от вашего желания подвергаем их сухой или влажной очистке, отпариванию, обработке специализированными составами. Практикуется и чистка на весу без снятия штор и жалюзи.',
        list: []
      },
      {
        title: 'Не опасна ли химчистка в доме у аллергиков?',
        text: 'Мы применяем моющие средства профессионального сегмента. Они не дают посторонних запахов, экологичны и абсолютно безвредны. Намного опаснее для вашего здоровья оставлять мягкие предметы интерьера запыленными, это точно вызовет аллергию!',
        list: []
      },
      {
        title: 'Вы оказываете усоуги химчистки на дому только в Москве?',
        text: 'Наши специалисты выезжают на объекте в Москве и Московоской области.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('dry_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на химчистку',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('dry_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('dry_cleaning'),
    },
    items: customerCleaningReviews('dry_cleaning'),
    turboItems: customerCleaningTurboReviews('dry_cleaning', 5),
    service_type: 'dry_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по химчистке',
    items: otherServicesService('dry_cleaning', '/uslugi/himchistka'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на химчистку',
    description: '',
    items: pricesService('dry_cleaning'),
    type: 'dry_cleanin',
    className: '',
    url: '/uslugi/himchistka'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Химчистка'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Качественную выездную химчистку можно заказать у наших специалистов по доступным ценам в любое удобное время независимо от площади уборки и количества предметов, нуждающихся в чистке.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>
        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const himchistka:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Sat, 7 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboPrices(data.prices)}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Качественную выездную химчистку можно заказать у наших специалистов по доступным ценам в любое удобное время независимо от площади уборки и количества предметов, нуждающихся в чистке.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
