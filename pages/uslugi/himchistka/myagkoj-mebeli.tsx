import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";
import OtherServices, {turboOtherServices} from "../../../components/Content/OtherServices/OtherServices";

import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';
import {otherServicesService} from "../../../constants/otherServices.constants";

const data:any = {
  settings: {
    title: 'Химчистка мягкой мебели в Москве | my-cleaning',
    description: 'Химчистка мягкой мебели в Москве от компании «my-cleaning». Закажите химчистку на дом по доступной цене! Работаем без выходных. Наш телефон в Москве +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/himchistka-myagkoj-mebeli-bg.webp',
    name: 'Химчистка мягкой мебели',
    rating: customerCleaningRating('dry_cleaning') / customerCleaningReviewsCount('dry_cleaning'),
    url: '/uslugi/himchistka/myagkoj-mebeli',
    sku: 19,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Химчистка мягкой мебели',
      description: {
        title: null,
        text: [
          'Изначальный вид дивана, кресла или кровати радует глаз: чистый и яркий, без загрязнений. Со временем на мебели появляются пятна от кофе, шерсть домашних животных и неприятные запахи. Позвольте профессионалам компании My Cleaning.  очистить вашу мебель.'
        ],
        subText: []
      },
      capabilities: [],
      btnText: 'Узнать цену',
      place: false
    }
  },
  included: {
    title: 'Что входит в химчистку мягкой мебели?',
    description: 'Услуга химчистки мягкой мебели имеет ряд достоинств перед самостоятельным выведением загрязнений:',
    items: [
      {
        icon: {
          src: '/static/images/included/himchistka-myagkoj-mebeli-1.webp',
          alt: 'Никаких сложностей с выбором химии'
        },
        title: 'Никаких сложностей с выбором химии',
        description: 'При современном обилии средств уборки сложно выбрать подходящее именно для вашей тканевой обивки. Мебель легко испортить: оставить на ней невыводимое пятно от химии или же изменить цвет ткани или кожи.',
        list: []
      },
      {
        icon: {
          src: '/static/images/included/himchistka-myagkoj-mebeli-2.webp',
          alt: 'Комфорт'
        },
        title: 'Комфорт',
        description: 'При заказе профессиональной химчистки мягкой мебели наши сотрудники приедут к вам домой или в офис в удобное для вас время и выполнят свою задачу с соблюдением всех мер предосторожности.',
        list: []
      },
      {
        icon: {
          src: '/static/images/included/himchistka-myagkoj-mebeli-3.webp',
          alt: 'Множество услуг по выгодной цене'
        },
        title: 'Множество услуг по выгодной цене',
        description: 'В итоговую стоимость включены цена моющих средств, транспортные расходы, химчистка мягкой мебели (мы работаем с такими материалами, как синтетика, натуральные ткани, шелк, велюр, бархат, кожа, замша и тд) и профессиональное оборудование.',
        list: []
      },
      {
        icon: {
          src: '/static/images/included/himchistka-myagkoj-mebeli-4.webp',
          alt: 'Постоянная стоимость'
        },
        title: 'Постоянная стоимость',
        description: 'Мы гарантируем каждому клиенту прозрачное ценообразование. Стоимость наших услуг можно рассчитать на сайте. При заказе мы фиксируем итоговую цену в договоре и не меняем ее в ходе работ.',
        list: []
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Как можно заказать уборку?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы делаем уборку в квартирах разной планировки, площади и количества комнат, в частных домах, в офисах, везде, где требуются наши услуги.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится уборка?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка в квартире или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('dry_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на химчистку',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('dry_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('dry_cleaning'),
    },
    items: customerCleaningReviews('dry_cleaning'),
    turboItems: customerCleaningTurboReviews('dry_cleaning', 5),
    service_type: 'dry_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по химчистке',
    items: otherServicesService('dry_cleaning', '/uslugi/himchistka/myagkoj-mebeli'),
    className: 'even-section'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Химчистка мягкой мебели'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Если вы хотите заказать услугу химчистки  мягкой мебели на дому, а звонить каждый раз — неудобно, мы рекомендуем обсудить с нашими специалистами удобный для вас график уборки. Клинеры будут приезжать по вашему индивидуальному плану.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>
        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const himchistkaMyagkojMebeli:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Sat, 7 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Если вы хотите заказать услугу химчистки  мягкой мебели на дому, а звонить каждый раз — неудобно, мы рекомендуем обсудить с нашими специалистами удобный для вас график уборки. Клинеры будут приезжать по вашему индивидуальному плану.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
