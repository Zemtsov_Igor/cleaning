import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import HowWeWorking, {turboHowWeWorking} from '../../../components/Content/HowWeWorking/HowWeWorking';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import OtherServices, {turboOtherServices} from '../../../components/Content/OtherServices/OtherServices';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";

import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews,
  customerCleaningRating
} from '../../../constants/customerReviews.constants';
import { otherServicesService } from '../../../constants/otherServices.constants';
import { pricesService } from '../../../constants/pricesServices.constants';

const data:any = {
  settings: {
    title: 'Заказать уборку квартиры в клининговой компании My Cleaning',
    description: 'Профессиональная уборка квартир от компании «my-cleaning». Закажите клининг квартиры по доступной цене! Работаем без выходных. Наш телефон в Москве +7 (495) 885-72-79',
    keywords: 'клининг квартиры клининговая уборка услуги по уборке заказать уборку квартиры профессиональная уборка квартиры профессиональная уборка квартиры',
    bgImage: '/static/images/bg/uborka-kvartir-bg.webp',
    name: 'Уборка квартир в Москве и Подмосковье',
    rating: customerCleaningRating('apartment_cleaning') / customerCleaningReviewsCount('apartment_cleaning'),
    url: '/uslugi/uborka-kvartir',
    sku: 1,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Заказать расчет уборки',
      description: {
        title: 'Причины заказать профессиональную уборку квартиры?',
        text: [
          'Чистая квартира — залог уюта и хорошей репутации. My Cleaning предлагает услуги клининга комнат и делает влажную уборку быстро и качественно.'
        ],
        subText: []
      },
      capabilities: [
        'Пополнение в семье. Перед приездом новорожденного ребенка рекомендуется провести качественную уборку квартиры, навести идеальную чистоту и обезопасить младенца от опасных инфекций.',
        'Новый домашний питомец. Перед приездом котят, щенят и других животных ветеринары рекомендуют провести влажную уборку.',
        'Освоение нового хобби. Генеральная уборка квартиры своими силами занимает много времени, которое можно потратить на семью, свои интересы или просмотр фильмов.',
        'Защита от вирусов. Сегодня важно обезопасить себя от опасных инфекций и профессиональный  клининг квартиры поможет снизить риск заболеваемости коронавирусом и другими болезнями.'
      ],
      btnText: 'Заказать уборку',
      place: true
    }
  },
  howWeWorking: {
    title: 'Как мы работаем?',
    items: [
      {
        icon: {
          src: '/static/images/setup_plan_icon.svg',
          alt: 'Индивидуальный план по уборке квартиры'
        },
        title: 'Индивидуальный план по уборке квартиры',
        text: 'Будние дни или выходные, утро, день или вечер — вы составляете график заказа клининга, ориентируясь в первую очередь на себя. Также вы можете пропускать или переносить день уборки. Во избежание штрафов рекомендуем сообщать не позднее, чем за сутки.'
      },
      {
        icon: {
          src: '/static/images/manage_online_icon.svg',
          alt: 'Проверка процесса влажной уборки квартиры с мобильного девайса'
        },
        title: 'Проверка процесса влажной уборки квартиры с мобильного девайса',
        text: 'Если вам неудобно присутствовать при клининговой уборке лично, рекомендуем воспользоваться сервисом  онлайн-проверки. Используйте для этого телефон, планшет или ноутбук — что вам будет удобно.'
      },
      {
        icon: {
          src: '/static/images/pro_female_icon.svg',
          alt: 'Время на себя и близких'
        },
        title: 'Время на себя и близких',
        text: 'Самостоятельная уборка квартиры требует много сил и времени, а клининг освободить его. Заказ профессиональных клининговых услуг в нашей компании — отличная возможность вспомнить старое хобби, освоить новое или провести время со своими детьми.'
      }
    ],
    className: 'even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы по уборке квартир',
    items: [
      {
        title: 'Как можно заказать услуги клининга в Вашей компании?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы делаем уборку в жилых помещениях разной планировки, площади и количества комнат (однокомнатных, двухкомнатных, трехкомнатных квартирах и студиях), в частных домах, в офисах и везде, где требуются наши услуги по профессиональной уборке помещений.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты оказывают услуги во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится стандартная уборка?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка квартиры или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('apartment_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку квартир',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('apartment_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('apartment_cleaning'),
    },
    items: customerCleaningReviews('apartment_cleaning'),
    turboItems: customerCleaningTurboReviews('apartment_cleaning', 5),
    service_type: 'apartment_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке квартир',
    items: otherServicesService('apartment_cleaning', '/uslugi/uborka-kvartir'),
    className: 'even-section'
  },
  prices: {
    title: 'Стоимость уборки квартир',
    description: '',
    items: pricesService('apartment_cleaning'),
    type: 'unit',
    className: '',
    url: '/uslugi/uborka-kvartir'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Уборка квартир'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage} />
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* works */}
          <HowWeWorking data={data.howWeWorking} />

          {/* Prices */}
          <Prices data={data.prices} />

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>
        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const uborkaKvartir:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Thu, 22 Jul 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboHowWeWorking(data.howWeWorking)}
          ${turboPrices(data.prices)}
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
