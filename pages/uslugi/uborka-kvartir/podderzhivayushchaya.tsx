import React, { Component } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import OtherServices, {turboOtherServices} from '../../../components/Content/OtherServices/OtherServices';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";

import { apartmentTypes } from '../../../constants/types.constants';
import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';
import { otherServicesService } from '../../../constants/otherServices.constants';
import { pricesService } from '../../../constants/pricesServices.constants';


const data:any = {
  settings: {
    title: 'Поддерживающая уборка квартиры от компании My-cleaning',
    description: 'Поддерживающая уборка квартиры от компании «my-cleaning». Закажите профессиональную уборку Вашей квартиры по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-kvartir-podderzhivayushchaya-bg.webp',
    name: 'Поддерживающая уборка квартиры в Москве и подмосковье',
    rating: customerCleaningRating('apartment_cleaning') / customerCleaningReviewsCount('apartment_cleaning'),
    url: '/uslugi/uborka-kvartir/podderzhivayushchaya',
    sku: 3,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Заказать расчет уборки',
      description: {
        title: null,
        text: [
          'Уборка и поддержание чистоты необходимы в каждой квартире и в каждом жилом пространстве. Для того, чтобы провести влажную уборку квартир, комплексную уборку в Москве, химчистку мебели или одежды, сегодня можно воспользоваться клининговыми услугами лучших специалистов компании My-cleaning . Перечень того, что входит в поддерживающую уборку, можно выбрать самостоятельно в зависимости от степени загрязнения или уточнить у мастеров при заказе.',
          'Профессиональные клининговые услуги и поддерживающая уборка квартиры подразумевают различные виды работ:'
        ],
        subText: []
      },
      capabilities: [
        'Комплексная уборка кухни с очисткой рабочих поверхностей и кухонной техники, холодильника, вытяжки, стен, шкафов для хранения продуктов и инвентаря, окон и занавесок;',
        'Дезинфекция и уборка туалетов, ванных комнат, чистка сантехники, плитки, кафеля, стен, душевых уголков;',
        'Уборка жилых помещений, мытьё полов и очистка от пыли, обработка ковров, сбор и распределение вещей для хранения;',
        'Вынос мусора, глажка и стирка белья, уход за комнатными растениями;',
        'Чистка и обработка специальными составами подоконников, стёкол, зеркал, плинтусов, шкафов, дверных ручек.'
      ],
      btnText: 'Заказать уборку',
      place: true
    }
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Как можно заказать поддерживающую уборку квартиры?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы делаем уборку в квартирах разной планировки, площади и количества комнат, в частных домах, в офисах, везде, где требуются наши услуги.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится поддерживающая уборка?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка в квартире или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('apartment_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку квартир',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('apartment_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('apartment_cleaning'),
    },
    items: customerCleaningReviews('apartment_cleaning'),
    turboItems: customerCleaningTurboReviews('apartment_cleaning', 5),
    service_type: 'apartment_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке квартир',
    items: otherServicesService('apartment_cleaning', '/uslugi/uborka-kvartir/podderzhivayushchaya'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на поддерживающую уборку квартиры',
    description: '',
    items: pricesService('apartment_peoples_cleaning_podd'),
    type: 'apartmentPeoples',
    className: '',
    url: '/uslugi/uborka-kvartir/podderzhivayushchaya'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Поддерживающая уборка квартир'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* State */}
          <Amp.AmpState specName="amp-state" id="apartmentTypes">{apartmentTypes}</Amp.AmpState>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Prices */}
          <Prices data={data.prices} />

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <p>Закажите уборку квартиры в нашей компании для того, чтобы получить доступные цены на все работы и гарантированно качественную уборку при минимальных затратах.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>
        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const uborkaKvartirPodderzhivayushchaya:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Sat, 7 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboPrices(data.prices)}
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <p>Закажите уборку квартиры в нашей компании для того, чтобы получить доступные цены на все работы и гарантированно качественную уборку при минимальных затратах.</p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
