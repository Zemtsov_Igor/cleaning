import React, { Component } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import OtherServices, {turboOtherServices} from '../../../components/Content/OtherServices/OtherServices';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";

import { apartmentTypes } from '../../../constants/types.constants';
import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';
import { otherServicesService } from '../../../constants/otherServices.constants';
import { pricesService } from '../../../constants/pricesServices.constants';

const data:any = {
  settings: {
    title: 'Ежедневнаяя уборка квартиры от компании My-cleaning',
    description: 'Ежедневная уборка квартир от компании «my-cleaning». Закажите клининг Вашей квартиры  по доступной цене! Работаем без выходных. Наш телефон в Москве +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-kvartir-ezhednevnaya-bg.webp',
    name: 'Ежедневная уборка квартир в Москве и Московской области',
    rating: customerCleaningRating('apartment_cleaning') / customerCleaningReviewsCount('apartment_cleaning'),
    url: '/uslugi/uborka-kvartir/ezhednevnaya',
    sku: 5,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Ежедневная уборка квартир',
      description: {
        title: 'Причины заказать ежедневную уборку квартиры в клининговой компании my-cleaning?',
        text: [
          'Находиться в чистом помещении приятно, особенно когда вы не участвуете в наведении чистоты. Мы предлагаем услугу профессиональной ежедневной уборки квартир по доступной цене в Москве и Московской области. В вашем доме будут трудиться клинеры с внушительным стажем.'
        ],
        subText: []
      },
      capabilities: [
        'Выполнение услуг поежедневной уборке квартиры только на профессиональном оборудовании.',
        'Профессиональные чистящие средства безопасные для Ваших детей и домашних питомцев.',
        'Профессиональная команда клинеров.',
        'Соблюдение мер безопасности для защиты от Covid-19.',
        'Доступные цены и специальные предложения для постоянных клиентов.'
      ],
      btnText: 'Узнать цену',
      place: true
    }
  },
  included: {
    title: 'Что включает в себя ежедневная уборка квартиры?',
    description: '',
    items: [
      {
        icon: {
          src: '/static/images/included/uborka-kvartir-ezhednevnaya-1.webp',
          alt: 'Ежедневная уборка квартиры'
        },
        title: '',
        description: '',
        list: [
          'Чистку ковров;',
          'Мытье полов;',
          'Удаление ржавчины и налета со стен;',
          'Очищение кухонной техники от пыли и других загрязнений;',
          'Мытье зеркальных поверхностей и подоконников;',
          'Удаление ржавчины и налета;',
          'Мытье ванны, туалета, раковины и душевой кабины(а также всех смесителей).'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-kvartir-ezhednevnaya-2.webp',
          alt: 'Что входит'
        },
        title: '',
        description: 'В стоимость ежедневной уборки квартиры уже входят цена моющих средств, оплата труда специалистов клининговой компании и расходы на транспорт. Мы также рекомендуем вам заказать дополнительные услуги, которые сэкономят ваше время:',
        list: [
          'Нанесение защитных покрытий. Они защищают полы от коррозии и придают износостойкост;',
          'Чистка ковров. Эта процедура придает напольному покрытию яркость и устраняет неприятные запахи;',
          'Мытье окон с внутренней и внешней стороны (в случае необходимости мы привлечем альпинистов);',
          'Чистка чехлов, мягкой мебели и других покрытий. Эта процедура вернет поверхности изначальный цвет.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-kvartir-ezhednevnaya-3.webp',
          alt: 'График уборки'
        },
        title: '',
        description: 'При заказе ежедневной уборки квартиры рекомендуем составить график, а наши специалисты будут его придерживаться. Благодаря этому вам не понадобится регулярно звонить и заново заказывать клининговые услуги.',
        list: []
      }
    ],
    className: 'even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Как можно заказать ежедневную  уборку квартиры?',
        text: 'Оставить заявку по телефону или на сайте, это займёт всего несколько минут.',
        list: []
      },
      {
        title: 'Вы делаете уборку в разных помещениях или только в квартирах?',
        text: 'Мы делаем уборку в квартирах разной планировки, площади и количества комнат, в частных домах, в офисах, везде, где требуются наши услуги.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Сколько времени длится ежедневная уборка?',
        text: 'Всё зависит от площади помещения, степени загрязнения, количества мебели, наличия сложных поверхностей. В среднем комплексная уборка в квартире или небольшом по площади доме занимает до 4-х часов, генеральная уборка – до 10 часов.',
        list: []
      },
      {
        title: 'Сколько стоит ежедневная уборка квартиры?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('apartment_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку квартир',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('apartment_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('apartment_cleaning'),
    },
    items: customerCleaningReviews('apartment_cleaning'),
    turboItems: customerCleaningTurboReviews('apartment_cleaning', 5),
    service_type: 'apartment_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке квартир',
    items: otherServicesService('apartment_cleaning', '/uslugi/uborka-kvartir/ezhednevnaya'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на ежедневную уборку квартир',
    description: '',
    items: pricesService('apartment_cleaning_ezh'),
    type: 'apartment_unit_price',
    className: '',
    url: '/uslugi/uborka-kvartir/ezhednevnaya'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Ежедневная уборка квартир'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* State */}
          <Amp.AmpState specName="amp-state" id="apartmentTypes">{apartmentTypes}</Amp.AmpState>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Prices */}
          <Prices data={data.prices} />

          {/* Included */}
          <Included data={data.included} />

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Get Started */}
          <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>
        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* readcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const uborkaKvartirEzhednevnaya:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Sat, 7 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboPrices(data.prices)}
          ${turboIncludeds(data.included)}
          ${turboFaq(data.faq)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
