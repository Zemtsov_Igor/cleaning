import React, { Component } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';

import { GridStyle } from '../../../styles/grid'
import { GlobalStyle } from '../../../styles/global'
import { RightFormComponent } from '../../../styles/rightFormComponent.Styles';

import MainLayout from '../../../components/Layouts/MainLayout/MainLayout';
import Header from '../../../components/Header/Header';
import Footer, {turboFooter} from '../../../components/Footer/Footer';
import Breadcrumbs, {turboBreadcrumbs} from '../../../components/Breadcrumbs/Breadcrumbs';
import Included, {turboIncludeds} from '../../../components/Content/Included/Included';
import Faq, {turboFaq} from '../../../components/Content/Faq/Faq';
import GetStarted, {turboGetStarted} from '../../../components/Content/GetStarted/GetStarted';
import Professionals, {turboProfessionals} from '../../../components/Content/Professionals/Professionals';
import Reviews, {turboReviews} from '../../../components/Content/Reviews/Reviews';
import OtherServices, {turboOtherServices} from '../../../components/Content/OtherServices/OtherServices';
import Prices, {turboPrices} from '../../../components/Content/Prices/Prices';
import RightForm, {turboRightForm} from "../../../components/Content/Forms/RightForm/RightForm";

import { apartmentTypes } from '../../../constants/types.constants';
import { professionalsReviewsService } from '../../../constants/professionalsReviews.constants';
import {
  customerCleaningRating,
  customerCleaningReviews,
  customerCleaningReviewsCount,
  customerCleaningTurboReviews
} from '../../../constants/customerReviews.constants';
import { otherServicesService } from '../../../constants/otherServices.constants';
import { pricesService } from '../../../constants/pricesServices.constants';

const data:any = {
  settings: {
    title: 'Генеральная уборка квартир в Москве и Московской области от компании My-cleaning',
    description: 'Генеральная уборка квартиры от компании «my-cleaning». Закажите профессиональную уборку Вашей квартиры по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79',
    keywords: null,
    bgImage: '/static/images/bg/uborka-kvartir-generalnaya-bg.webp',
    name: 'Генеральная уборка квартиры',
    rating: customerCleaningRating('apartment_cleaning') / customerCleaningReviewsCount('apartment_cleaning'),
    url: '/uslugi/uborka-kvartir/generalnaya',
    sku: 2,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Заказать расчет уборки',
      description: {
        title: 'Причины заказать генеральную уборку квартиры в клининговой компании my-cleaning?',
        text: [
          'Генеральная уборка квартиры – неизбежные траты на чистящие средства, губки, тряпки, всевозможные инструменты, необходимость тратить время и собственные силы, не имея полной уверенности в эффективности процесса. Но Вы можете заказать генеральную уборку квартиры в Москве и Подмосковье у профессиональных клинеров  –  это оптимальное решение реализовать идеальную чистоту с минимальными затратами и временными потерями.'
        ],
        subText: []
      },
      capabilities: [
        'Выполнение услуг по генеральной уборке квартиры только на профессиональном оборудовании.',
        'Профессиональные чистящие средства безопасные для Ваших детей и домашних питомцев.',
        'Профессиональная команда клинеров.',
        'Соблюдение мер безопасности для защиты от Covid-19.',
        'Выгодные цены и специальные предложения для постоянных клиентов.'
      ],
      btnText: 'Заказать уборку',
      place: true
    }
  },
  included: {
    title: 'Что входит в генеральную уборку квартир?',
    description: 'Перечень услуг по генеральному клинингу в Москве и Подмосковье в жилых помещениях:',
    items: [
      {
        icon: {
          src: '/static/images/included/uborka-kvartir-generalnaya-1.webp',
          alt: 'Мытьё полов'
        },
        title: '',
        description: '',
        list: [
          'Мытьё полов;',
          'Очистка всех поверхностей от пыли;',
          'Мытьё зеркал, окон, наружных и межкомнатных дверей.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-kvartir-generalnaya-2.webp',
          alt: 'Очистка сложной техники и хрупких поверхностей'
        },
        title: '',
        description: '',
        list: [
          'Очистка сложной техники и хрупких поверхностей;',
          'Мытьё посуды;',
          'Химчистка мебели, матрасов и ковров;',
          'Комплексная уборка в кухне с чисткой техники и шкафов.'
        ]
      },
      {
        icon: {
          src: '/static/images/included/uborka-kvartir-generalnaya-3.webp',
          alt: 'Чистка холодильника, плиты, тумбочек, мест хранения'
        },
        title: '',
        description: '',
        list: [
          'Чистка холодильника, плиты, тумбочек, мест хранения;',
          'Уборка в ванной комнате и туалете с чисткой сантехники, кафеля и стен;',
          'Вынос мусора;',
        ]
      }
    ],
    className: 'with_btn even-section'
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Сколько времени займет уборка?',
        text: 'Генеральная уборка длится от 4 до 8 часов.',
        list: []
      },
      {
        title: 'Сколько клинеров приедет на уборку?',
        text: 'Мы делаем уборку в квартирах разной планировки, площади и количества комнат, в частных домах, в офисах, везде, где требуются наши услуги.<br>Состав бригады зависит от площади Вашей квартиры. Уборку квартиры до 80 квадратных метров выполняет бригада клинеров из 2х челочек. Если площадь от 80 до 150 метров, то приедет бригада из 3х человек. Для площадей больше расчет осуществляется  – 1 клинер на 60 квадратных метров.',
        list: []
      },
      {
        title: 'Вы работаете только в Москве?',
        text: 'Наши специалисты работают во всех районах Москвы и Подмосковья в зависимости от местоположения заказчика.',
        list: []
      },
      {
        title: 'Как узнать стоимость работ?',
        text: 'Полный перечень услуг с ценами есть на сайте нашей компании, более подробно стоимость можно уточнить у наших специалистов, обозначив площадь помещения и количество комнат.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Что вы используете для работы?',
        text: 'Мы используем только качественные и безопасные моющие средства и специальные чистящие составы для каждого вида поверхности индивидуально, профильные инструменты и дополнительную современную технику.',
        list: []
      },
      {
        title: 'Вы следите за безопасностью работ в период сложной эпидемиологической обстановки по коронавирусу?',
        text: 'Все наши сотрудники проходят обязательный ежедневный контроль состояния и здоровья, уровня температуры для обеспечения максимальной безопасности клиентов.',
        list: []
      },
    ],
    className: 'with_btn'
  },
  professionals:{
    title: 'Познакомьтесь с некоторыми из наших лучших профессионалов по уборке квартир',
    description: '',
    items: professionalsReviewsService('apartment_cleaning'),
    className: 'even-section'
  },
  reviews:{
    title: 'Отзывы наших клиентов',
    description: 'на уборку квартир',
    pagination: {
      itemsPerPage: 5,
      currentPage: 1,
      totalCount: customerCleaningReviewsCount('apartment_cleaning'),
      maxPaginationLength: 5,
      reviewsLimit: customerCleaningReviewsCount('apartment_cleaning'),
    },
    items: customerCleaningReviews('apartment_cleaning'),
    turboItems: customerCleaningTurboReviews('apartment_cleaning', 5),
    service_type: 'apartment_cleaning',
    className: 'with_btn'
  },
  otherServices: {
    title: 'Другие услуги по уборке квартир',
    items: otherServicesService('apartment_cleaning', '/uslugi/uborka-kvartir/generalnaya'),
    className: 'even-section'
  },
  prices: {
    title: 'Цены на генеральную уборку квартиры',
    items: pricesService('apartment_peoples_cleaning'),
    type: 'apartmentPeoples',
    className: '',
    url: '/uslugi/uborka-kvartir/generalnaya'
  },
  breadcrumbsPath: [
    {
      url: '/uslugi',
      title: 'Все услуги'
    },
    {
      title: 'Генеральная уборка квартир'
    }
  ]
}

export default class HomeCleaning extends Component {
  render() {
    return (
      <MainLayout itemtype={'http://schema.org/Product'}>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru${data.settings.url}`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <RightFormComponent role="main">
          {/* header bg */}
          <div id="top" className="quote-bg">
            <div className="grid-x service-image">
              <meta itemProp="image" content={data.settings.bgImage}/>
              <div
                className="cell service-image"
                style={{backgroundImage: `url(${data.settings.bgImage})`}}
              />
            </div>

          </div>

          {/* State */}
          <Amp.AmpState specName="amp-state" id="apartmentTypes">{apartmentTypes}</Amp.AmpState>

          {/* head */}
          <RightForm data={{settings: data.settings, reviews: data.reviews}} />

          {/* Included */}
          <Included data={data.included} />

          {/* Get Started */}
          <div className="grid-container even-section-in-section">
            <div className="grid-x align-center">
              <div className="cell small-12 large-10">
                <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' }} />
              </div>
            </div>
          </div>

          {/* Prices */}
          <Prices data={data.prices} />

          {/* FAQ */}
          <Faq data={data.faq} />

          {/* Professionals */}
          <Professionals data={data.professionals} />

          {/* Reviews */}
          <Reviews data={data.reviews} />

          {/* Best Service */}
          <div className="page_text">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="grid-x small-10 align-center">
                  <div className="cell page_text__content">
                    <h2>Заказать генеральную уборку квартиры</h2>
                    <p>Сегодня означает получить идеальную чистоту в собственной квартире по выгодным ценам, не беспокоясь о необходимости реализовать уборку самостоятельно. Заказать услуги независимо от объёма работ можно сегодня с помощью наших специалистов, которые используют только безопасные моющие средства, строго соблюдают сроки работ, выполняют все пункты договора в соответствии с пожеланиями заказчика, обеспечивают чистоту в жилом помещении быстро и качественно.</p>
                  </div>
                </div>
              </div>

              <GetStarted data={{ btnAction: '#top', btnTitle: 'Начать', className:'in-section' }} />

            </div>
          </div>
        </RightFormComponent>

        {/* Other Services */}
        <OtherServices data={data.otherServices} />

        {/* Breadcrumbs */}
        <Breadcrumbs path={data.breadcrumbsPath} />

        <Footer/>
      </MainLayout>
    );
  }
};

export const uborkaKvartirGeneralnaya:string = `
  <item turbo="true">
    <turbo:extendedHtml>true</turbo:extendedHtml>
    <link>https://my-cleaning.ru${data.settings.url}</link>
    <turbo:source>https://my-cleaning.ru${data.settings.url}</turbo:source>
    <title>${data.settings.title}</title>
    <turbo:topic>${data.settings.description}</turbo:topic>
    <pubDate>Tue, 3 Aug 2021 14:34:50 +0300</pubDate>
    <author>${data.settings.brand}</author>
    <turbo:content>
      <![CDATA[
        <header>
          <h1 class="main_title">${data.settings.name}</h1>
        </header>
        <main class="page-frame">
          <section class="quote-bg">
            <div class="grid-x service-image">
              <div class="cell service-image">
                <img src="https://my-cleaning.ru${data.settings.bgImage}" alt="${data.settings.title}" class="img" />
              </div>
            </div>
          </section>
          ${turboRightForm({settings: data.settings, reviews: data.reviews})}
          ${turboIncludeds(data.included)}
          <div class="grid-container even-section-in-section">
            <div class="grid-x align-center">
              <div class="cell small-12 large-10">
                ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section', align:'center' })}
              </div>
            </div>
          </div>
          ${turboPrices(data.prices)}
          ${turboFaq(data.faq)}
          ${turboProfessionals(data.professionals)}
          ${turboReviews(data.reviews, data.settings.url)}
          <div class="page_text">
            <div class="grid-container">
              <div class="grid-x align-center">
                <div class="grid-x small-10 align-center">
                  <div class="cell page_text__content">
                    <h2>Заказать генеральную уборку квартиры</h2>
                    <p>
                      Сегодня означает получить идеальную чистоту в собственной квартире по выгодным ценам, не беспокоясь о необходимости реализовать уборку самостоятельно. Заказать услуги независимо от объёма работ можно сегодня с помощью наших
                      специалистов, которые используют только безопасные моющие средства, строго соблюдают сроки работ, выполняют все пункты договора в соответствии с пожеланиями заказчика, обеспечивают чистоту в жилом помещении быстро и качественно.
                    </p>
                  </div>
                </div>
              </div>
              ${turboGetStarted({ btnAction: '#top', btnTitle: 'Начать', className:'in-section' })}
            </div>
          </div>
        </main>
        ${turboOtherServices(data.otherServices)}
        ${turboBreadcrumbs(data.breadcrumbsPath)}
        ${turboFooter}
      ]]>
    </turbo:content>
  </item>
`;
