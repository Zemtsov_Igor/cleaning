import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { PageComponent } from '../styles/pageComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>О нас</title>
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <PageComponent>
          <div className="quote-bg">
            <div className="grid-container">
              <h1>О нас</h1>
            </div>
          </div>

          <div className="blank-background">
            <div className="grid-container">
              <div className="grid-x align-center">

                <div className="cell small-12 medium-10">
                  <div className="about-content">
                    <h2>О сервисе</h2>
                    <p>Handy - это ведущая платформа для соединения лиц, ищущих бытовые услуги, с высококачественными, предварительно отобранными независимыми специалистами по обслуживанию. Handy каждую неделю мгновенно подбирает тысячи клиентов с лучшими профессионалами в городах по всему миру - от уборки дома до услуг разнорабочего. Благодаря удобному 60-секундному процессу бронирования, безопасной оплате и поддержке Handy Happiness Guarantee, Handy - самый простой и удобный способ заказать услуги на дому.</p>
                  </div>
                </div>

                <div className="cell small-12 medium-10">
                  <div className="about-content">
                    <h2>Наша история</h2>
                    <p>
                      Handy, ранее известная как Handybook, была основана в 2012 году как практическое решение давней проблемы: поиск высококлассных и эффективных профессионалов для обычных домашних услуг. Ойсин Ханрахан была 19-летней студенткой колледжа в Ирландии.
                       когда он решил стать застройщиком в Восточной Европе. Обыскав разные города во время перерывов на выходных в Тринити-колледже в Дублине, Ханрахан начал покупать и ремонтировать квартиры в Будапеште. При ремонте
                       В нескольких квартирах по всему городу Ойсину было трудно найти квалифицированных мастеров, которым он мог доверять, чтобы выполнить свою работу. Легкого пути не было. Несколько лет спустя, посещая Гарвардскую школу бизнеса, Ойсин и его одноклассник Уманг
                       Дуа понял, что то же самое верно и в США. Они разработали Handy, чтобы заполнить этот пробел, с целью создания самого простого и удобного способа для занятых людей во всем мире заказывать услуги по дому.
                    </p>
                  </div>
                </div>

                <div className="cell small-12 medium-10">
                  <div className="bio-content">
                    <h3>Спасибо многим людям, которые помогли Handy начать работу.</h3>
                    <p>
                      Игнасио Леонхардт и Вайна Скотт были первыми членами команды основателей Handy. Проработав 9 месяцев в Handy, Игнасио вернулся в Гватемалу и теперь управляет семейным бизнесом. Вайна работала с Хэнди для
                       7 месяцев, создание первой версии сайта, а теперь работает над другим стартапом в Калифорнии. Боб Дэвис и Джеремия Дейли из Highland Capital Partners вложили первый капитал в Handy, за ними последовали Джоэл Катлер и Нитеш.
                       Banta от General Catalyst Partners, Дэвида Тиша и Адама Ротенберга из Box Group и Art Papas. Эндрю Люрви был первым членом операционной группы Handy. Эндрю присоединился к Хэнди, когда нас было четыре человека в маленькой комнате. Он провел два
                       лет работы с Handy, в то время как мы выросли до 20000 человек в 28 городах.
                    </p>
                  </div>
                </div>

                <div className="cell small-12 medium-10">
                  <div className="about-content">
                    <h3>Спасибо многим людям, которые впоследствии присоединились к команде и инвестировали в Handy. Благодаря вашей помощи мы уверенно движемся к тому, чтобы изменить то, как мир покупает услуги.</h3>
                  </div>
                </div>

              </div>
            </div>
          </div>

        </PageComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
