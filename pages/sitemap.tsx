import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { PageComponent } from '../styles/pageComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>Карта сайта</title>
          <meta name="description" content="Карта сайта - от компании «my-cleaning». Закажите профессиональную уборку по доступной цене! Работаем без выходных. Наш телефон +7 (495) 885-72-79" />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <PageComponent>
          <div className="gradient-background">
            <div className="grid-container">
              <div className="grid-x align-center">
                <div className="cell small-12 large-10">
                  <div className="grid-x align-center content-container">
                    <div className="cell small-11 large-9">
                      <h1>Карта сайта</h1>

                      <div  className="sitemap-content">

                        <ul>
                          <li>
                            <p>
                              <a href="/" target="_blank">Клинговая компания my-cleaning</a>
                            </p>

                            <ul>
                              <li>
                                <p>
                                  <a href="/uslugi" target="_blank">Все услуги</a>
                                </p>

                                <ul>
                                  <li>
                                    <p>
                                      <a href="/uslugi/uborka-kvartir" target="_blank">Уборка квартир в Москве</a>
                                    </p>

                                    <ul>
                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/generalnaya" target="_blank">Генеральная уборка квартир в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/podderzhivayushchaya" target="_blank">Поддерживающая уборка квартир</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/posle-remonta" target="_blank">Уборка квартир после ремонта</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/ezhednevnaya" target="_blank">Ежедневная уборка квартир в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/srochnaya" target="_blank">Срочная уборка квартир в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/odnokomnatnoj" target="_blank">Уборка однокомнатной квартиры</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/dvuhkomnatnoj" target="_blank">Уборка двухкомнатной квартиры</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kvartir/trekhkomnatnoj" target="_blank">Уборка трехкомнатной квартиры</a>
                                        </p>
                                      </li>

                                    </ul>

                                  </li>
                                </ul>

                                <ul>
                                  <li>
                                    <p>
                                      <a href="/uslugi/uborka-kottedzhej" target="_blank">Уборка домов и коттеджей в Москве</a>
                                    </p>

                                    <ul>
                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kottedzhej/generalnaya" target="_blank">Генеральная уборка коттеджей в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kottedzhej/podderzhivayushchaya" target="_blank">Поддерживающая уборка коттеджей</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kottedzhej/posle-remonta" target="_blank">Уборка коттеджей после ремонта</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kottedzhej/srochnaya" target="_blank">Срочная уборка коттеджей в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kottedzhej/chastnyh-domov" target="_blank">Уборка частных домов</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-kottedzhej/taunhausov" target="_blank">Уборка таунхаусов</a>
                                        </p>
                                      </li>

                                    </ul>

                                  </li>
                                </ul>

                                <ul>
                                  <li>
                                    <p>
                                      <a href="/uslugi/uborka-ofisov" target="_blank">Уборка офисов в Москве</a>
                                    </p>

                                    <ul>
                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-ofisov/ezhednevnaya" target="_blank">Ежедневная уборка офисов в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-ofisov/podderzhivayushchaya" target="_blank">Поддерживающая уборка офисов</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-ofisov/generalnaya" target="_blank">Генеральная уборка офисов в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/uborka-ofisov/posle-remonta" target="_blank">Уборка офисов после ремонта</a>
                                        </p>
                                      </li>

                                    </ul>

                                  </li>
                                </ul>

                                <ul>
                                  <li>
                                    <p>
                                      <a href="/uslugi/himchistka" target="_blank">Химчистка на дому в Москве</a>
                                    </p>

                                    <ul>
                                      <li>
                                        <p>
                                          <a href="/uslugi/himchistka/s-vyezdom" target="_blank">Химчистка с выездом в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/himchistka/myagkoj-mebeli" target="_blank">Химчистка мягкой мебели в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/himchistka/shtor" target="_blank">Химчистка штор в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/himchistka/kovrov" target="_blank">Химчистка ковров в Москве</a>
                                        </p>
                                      </li>

                                    </ul>
                                  </li>
                                </ul>

                                <ul>
                                  <li>
                                    <p>
                                      <a href="/uslugi/moyka-okon" target="_blank">Мойка окон в Москве</a>
                                    </p>

                                    <ul>
                                      <li>
                                        <p>
                                          <a href="/uslugi/moyka-okon/vitrin" target="_blank">Мойка витрин в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/moyka-okon/fasadov" target="_blank">Мойка фасадов в Москве</a>
                                        </p>
                                      </li>

                                      <li>
                                        <p>
                                          <a href="/uslugi/moyka-okon/alpinistami" target="_blank">Мойка окон альпинистами в Москве</a>
                                        </p>
                                      </li>
                                    </ul>

                                  </li>
                                </ul>

                                <ul>
                                  <li>
                                    <p>
                                      <a href="/uslugi/dezinfekciya" target="_blank">Дезинфекция квартир, домов, офисов в Москве</a>
                                    </p>
                                  </li>
                                </ul>

                                <ul>
                                  <li>
                                    <p>
                                      <a href="/uslugi/uborka-pomeshchenij" target="_blank">Уборка помещений в Москве</a>
                                    </p>
                                  </li>
                                </ul>

                              </li>

                              <li>
                                <p>
                                  <a href="/kontakty" target="_blank">Контакты в Москве</a>
                                </p>
                              </li>

                              <li>
                                <p>
                                  <a href="/privacy" target="_blank">Политика конфиденциальности</a>
                                </p>
                              </li>

                              <li>
                                <p>
                                  <a href="/terms" target="_blank">Условия использования / Пользовательское соглашение</a>
                                </p>
                              </li>

                            </ul>

                          </li>
                        </ul>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </PageComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
