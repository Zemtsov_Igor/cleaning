import React, { Component } from 'react';
import Head from 'next/head';
import * as Amp from 'react-amphtml';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { GuaranteeComponent } from '../styles/guaranteeComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>Доверие и безопасность</title>
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <GuaranteeComponent>
          <div className="our-guarantee">
            <div className="our-guarantee-hero flex-rows">
              <div className="our-guarantee-hero-box flex-rows centered">
                <h1>Доверие и безопасность - наши обязательства перед вами</h1>
                <div className="subtitle">Мы гордимся платформой, основанной на доверии, и прилагаем все усилия, чтобы обеспечить безопасность домов.</div>
              </div>
            </div>

            <div className="trust-customers flex-rows section centered">
              <div className="pre-title">Наши довольные клиенты</div>
              <div className="title">По всей стране и миллионы завершенных бронирований</div>
              <div className="subtitle">
                Профессионалы выполнили более миллионов бронирований через платформу Handy в США, Великобритании и Канаде. При таком большом количестве безопасность наших клиентов чрезвычайно важна для нас. Средний профессионал, использующий платформу Handy, получает оценку 4,5 из 5 звезд.
              </div>
              <a className="btn btn-secondary privacy-button" href="/">Посмотреть отзывы клиентов</a>
            </div>

            <div className="trust-happiness section centered">
              <div className="trust-happiness-container">
                <Amp.AmpImg
                  specName="default"
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  src='/static/images/security_icon.png'
                  className="trust-icon img"
                  layout="flex-item"
                  alt='security Handy'
                />
                <div className="trust-happiness-text flex-rows centered">
                  <div className="title">Мы вас прикрыли</div>
                  <div className="subtitle">Ваше счастье - наша цель. Если вас не устраивает бронирование, совершенное и оплаченное на платформе Handy Platform, мы постараемся все исправить, и в редких случаях повреждения Handy вас поддержит. Вот почему мы создали <a href="/guarantee">Гарантия счастья под рукой.</a></div>
                </div>
              </div>
            </div>

            <div className="trust-serious section">
              <div className="serious-container">
                <div className="title">Мы серьезно относимся к безопасности</div>
                <div className="subtitle">
                  <span>We’ve set up a </span>
                  <a href="https://cache.hbfiles.com/assets/trust-and-safety/trustandsafetyboard-02d330941c6453c2b11fc07b6f83e9c0b38a15fd61f7b4c3fbd4167083ddba02.pdf" target="_blank">Консультативный совет по вопросам доверия и безопасности</a>
                  <span> состоит из уважаемых лидеров научных кругов, правоохранительных органов и организаций, занимающихся вопросами конфиденциальности. Члены правления несут ответственность за предоставление Handy практических советов о том, как лучше всего защитить как клиентов, так и специалистов.</span>
                </div>
              </div>
            </div>

            <div className="trust-pro grid-x section">
              <div className="medium-auto image-background"></div>
              <div className="medium-auto side-pro-section">
                <div className="title">Наш процесс отбора</div>
                <div className="subtitle">Мы проверяем разнорабочих и специалистов по уборке, которых вы заказываете и оплачиваете непосредственно через платформу Handy.</div>
                <div className="screening-list-item">
                  <div className="checkmark">✓</div>
                  <div className="float-left">
                    <div className="screening-title">Предварительные просмотры</div>
                    <div className="screening-body">
                      Потенциальные специалисты должны предоставить подробную личную информацию и подтвердить свой опыт обслуживания на дому.
                    </div>
                  </div>
                </div>
                <div className="screening-list-item">
                  <div className="checkmark">✓</div>
                  <div className="float-left">
                    <div className="screening-title">Проверка учетных данных</div>
                    <div className="screening-body">
                      Удобные партнеры с Jumio в использовании технологий компьютерного зрения для проверки идентификационных данных потенциальных профессионалов.
                    </div>
                  </div>
                </div>

                <div className="screening-list-item">
                  <div className="checkmark">✓</div>
                  <div className="float-left">
                    <div className="screening-title">Проверка фона</div>
                    <div className="screening-body">
                      Скрининг с проверкой биографических данных выполняется Checkr, аккредитованным сторонним поставщиком проверки биографических данных. Поиск Checkr включает национальные базы данных, базы данных штата и округа за период не менее 7 лет, а в некоторых
                      юрисдикции. Целью этих проверок является выявление нарушений, которые могут лишить потенциальных профессионалов права использовать Handy. Некоторые профессионалы являются сотрудниками, франчайзи, дилерами или независимыми подрядчиками более крупных национальных или
                      корпоративные компании. Для этих специалистов выполняется только этап проверки биографических данных.
                    </div>
                  </div>
                </div>

                <div className="flex-cols">
                  <a className="btn btn-secondary process-button" target="_blank" href="https://cache.hbfiles.com/assets/trust-and-safety/learnmore-92324ba39131e7422ca1ea93ff55f8e02f34d1c5846e5ac6fe03b8d69f798d11.pdf">Узнать больше</a>
                </div>
              </div>
            </div>

            <div className="trust-contact-us flex-rows centered section">
              <div className="title">Мы здесь, чтобы помочь</div>
              <div className="subtitle">
                Handy - это безопасный и надежный способ нанять высококлассных специалистов по обслуживанию на дому. При этом могут произойти несчастные случаи и инциденты. А что касается скрининга, у каждой системы есть свои недостатки. Это отчасти потому, что прошлое поведение может не
                точно прогнозировать, как люди будут вести себя в будущем, но также потому, что ни одна система в США не имеет стопроцентно точных записей прошлого. Если у вас возникла проблема, свяжитесь с нашей круглосуточной службой поддержки клиентов.
              </div>
              <a className="btn btn-secondary contact-us-title" href="/help">Центр помощи</a>
            </div>

            <div className="trust-book flex-rows section">
              <div className="title">Готовы к бронированию?</div>
              <div className="subtitle">
                Наш процесс проверки гарантирует, что мы работаем с лучшими профессионалами. Просто выберите время и будьте уверены, все остальное мы сделаем сами.
              </div>
              <div className="buttons-wrapper">
                <a className="btn btn-secondary button" href="/uslugi/uborka-kvartir">Закажите уборку квартиры</a>
                <a className="btn btn-invert-brand-color button" href="/uslugi">Все услуги</a>
              </div>
            </div>



          </div>
        </GuaranteeComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
