import React, { Component } from 'react';
import Head from 'next/head';

import { GridStyle } from '../styles/grid'
import { GlobalStyle } from '../styles/global'
import { HomeComponent } from '../styles/homeComponent.Styles';

import MainLayout from '../components/Layouts/MainLayout/MainLayout';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import Reliable from '../components/Content/Reliable/Reliable';
import HowWeWorking from '../components/Content/HowWeWorking/HowWeWorking';
import Faq from '../components/Content/Faq/Faq';

import {apartmentTypes, apartmentTypesStr} from '../constants/types.constants';

const data:any = {
  settings: {
    title: 'Клинговая компания my-cleaning | Уборка домов, квартир, офисов в Москве',
    description: 'Уборка квартир в Москве — цены на клининговые услуги My-cleaning. Работаем без выходных. Наш телефон в Москве: +7 (495) 885-72-79.',
    keywords: null,
    bgImage: '/static/images/logo.svg',
    name: 'Дезинфекция квартир, домов и помещений',
    rating: 50,
    url: '',
    sku: 26,
    mpn: 'CLEANING',
    brand: 'My Cleaning',
    form: {
      title: 'Дезинфекция квартир, домов и помещений',
      description: {
        title: null,
        text: [
          'Для чего нужна дезинфекция квартиры или дома и почему ее нельзя заменить на обычную влажную уборку?'
        ],
        subText: []
      },
      capabilities: [
        'защита от вирусов. Сегодня очень важно снизить риск заболеваемости от Covid-19  и других инфекций;',
        'пополнение в семье. Перед приездом новорожденного специалисты рекомендуют проводить тщательную дезинфекцию квартиры или дома;',
        'наличие домашних животных. Если дома живут кошка, собака или другие питомцы с обильной линькой, возможно неожиданное проявление аллергической реакции. Снизить риск поможет дезинфекция квартиры, которую рекомендуется проводить хотя бы раз в квартал.'
      ],
      btnText: 'Заказать дезинфекцию',
      place: false
    }
  },
  reliable: {
    title: 'Преимущества заказа услуг клининга в нашей компании',
    subTitle: 'Почему заказывают уборку в нашей компании? ',
    items: [
      {
        icon: {
          src: '/static/images/experienced_icon.svg',
          alt: 'Опыт'
        },
        title: 'Опыт',
        text: 'С 2012 года наша компания предоставляет людям услуги по профессиональной уборке помещений. За свою историю профессионалы по уборке помогли очистить миллионы домов и квартир в Москве. На нашем сайте вы можете оставить заявку на уборку квартиры, дома и других помещений.'
      },
      {
        icon: {
          src: '/static/images/reliable_icon.svg',
          alt: 'Надежность'
        },
        title: 'Надежность',
        text: 'Когда вы заказываете услуги уборки на нашем сайте, вы можете не сомневаться в том, что услуги будут оказаны в необходимое для Вас время. Мы поможем вернуть вашу спальню, ванную, кухню, гостиную и другие помещения в их первоначальное состояние.'
      },
      {
        icon: {
          src: '/static/images/convenient_icon.svg',
          alt: 'Удобство'
        },
        title: 'Удобство',
        text: 'Работа допоздна мешает вам поддерживать порядок в квартире? Вы слишком заняты тем, что водите детей в школу, на тренировки? Мы знаем, что жизнь может быть сумасшедшей и непредсказуемой, и когда вы устали и перегружены работой, последнее, что кто-то хочет делать, это убирать свой дом. Какой бы ни была причина, по которой вы ищете помощь по уборке дома или квартиры мы поможем  вам.'
      },
      {
        icon: {
          src: '/static/images/flexible_icon.svg',
          alt: 'Гибкость'
        },
        title: 'Гибкость',
        text: 'На нашем сайте легко планировать и переносить бронирования уборок. Просто откройте форму записи и выберите наиболее удобные для вас дату и время. Или, может быть, вашему дому нужна генеральная уборка - вы можете использовать приложение, чтобы добавить к бронированию дополнительные услуги, такие как стирка, уборка в шкафах, в холодильнике и т. д. Сделайте нашу команду вашим помощником номер один по уборке квартиры, дома или офиса.'
      }
    ],
    className: 'section-empty'
  },
  howWeWorking: {
    title: 'Закажите уборку квартиры в Москве',
    items: [
      {
        icon: {
          src: '/static/images/setup_plan_icon.svg',
          alt: 'Индивидуальный план клининговой уборки квартиры'
        },
        title: 'Индивидуальный план клининговой уборки квартиры',
        text: 'Будние дни или выходные, утро, день или вечер — вы составляете график влажной уборки, ориентируясь в первую очередь на себя. Также вы можете пропускать или переносить день удобки. Во избежание штрафов рекомендуем сообщать не позднее, чем за сутки.'
      },
      {
        icon: {
          src: '/static/images/manage_online_icon.svg',
          alt: 'Проверка процесса влажной уборки квартиры с мобильного девайса'
        },
        title: 'Проверка процесса влажной уборки квартиры с мобильного девайса',
        text: 'Если вам неудобно присутствовать при клининговой уборке лично, рекомендуем воспользоваться услугой онлайн-проверки. Используйте для этого телефон, планшет или ноутбук — что вам будет удобно.'
      },
      {
        icon: {
          src: '/static/images/pro_female_icon.svg',
          alt: 'Время на себя и близких'
        },
        title: 'Время на себя и близких',
        text: 'Самостоятельная уборка квартиры требует много сил и времени, а клининг освободить его. Заказ профессиональной влажной уборки в нашей клининговой компании — отличная возможность вспомнить старое хобби, освоить новое или провести время со своими детьми.'
      }
    ],
    className: ''
  },
  faq: {
    title: 'Часто задаваемые вопросы',
    items: [
      {
        title: 'Какой клинер приедет ко мне?',
        text: 'Мы располагаем обширной сетью опытных уборщиков с самым высоким рейтингом. В зависимости от времени и даты вашего бронирования услуг по уборке мы выберем лучшего клинера.',
        list: []
      },
      {
        title: 'Могу ли я пропустить или перенести бронирование?',
        text: 'Вы можете перенести любое бронирование без штрафных санкций, если сделаете это как минимум на 24 часа раньше запланированного времени начала.',
        list: []
      },
      {
        title: 'Сколько стоят услуги по уборке дома',
        text: 'Стоимость услуг по уборке дома зависит от площади - укажите, сколько у вас спален и ванных комнат, в форму выше, и мы сразу же предоставим вам расчет стоимости. Мы уверены, что вы найдете цены чрезвычайно разумными, учитывая высокий уровень услуги клининга в нашей компании.',
        list: []
      },
      {
        title: 'Включает ли уборка стирка?',
        text: 'Если у вас дома есть стиральная машина и / или сушилка, ваш клинер сможет выстирать и сложить белье за вас! Обязательно добавьте указание об этой услуге в дополнения во время оформления заказа, чтобы клинер мог рассчитать правильно время для уборки и стирки (это добавит дополнительный час к вашему бронированию).',
        list: []
      },
      {
        title: 'Сколько времени должна длиться уборка дома?',
        text: 'Время по уборке полностью зависит от площади вашего дома! Минимальная продолжительность бронирования составляет 3 часа, но вы всегда можете запросить более длительное время, если это необходимо. В зависимости от количества спален и ванных комнат, при оформлении заказа определяется оптимальное время на уборку в зависимости от площади вашего дома или квартиры.',
        list: []
      },
    ],
    className: ''
  },
}

export default class Services extends Component {
  render() {
    return (
      <MainLayout>
        <Head>
          <title>{data.settings.title}</title>
          {data.settings.description && <meta name="description" content={data.settings.description} />}
          {data.settings.keywords && <meta name="keywords" content={data.settings.keywords} />}
          {data.settings.bgImage && <meta name="image" content={data.settings.bgImage} />}
          <link rel="amphtml" href={`https://amp.my-cleaning.ru`} />
        </Head>
        <GridStyle/>
        <GlobalStyle/>
        <Header/>

        <HomeComponent>
          <div className="home-page">
            <section className="grid-container">
              <div itemScope itemType="http://schema.org/Attorney" className="grid-x">
                <meta itemProp="name" content="My Cleaning" />
                <meta itemProp="url" content="https://my-cleaning.ru/" />
                <meta itemProp="logo" content="https://my-cleaning.ru/static/images/logo.svg" />
                <meta itemProp="image" content="https://my-cleaning.ru/static/images/logo.svg" />
                <meta itemProp="telephone" content="+7 (495) 885-72-79" />
                <meta itemProp="openingHours" content="Mo,Tu,We,Th,Fr,Su,Sa 09:00-20:00" />
                <meta itemProp="priceRange" content="50 - 1000" />
                <meta itemProp="hasMap" content="https://www.google.com/maps/place/My-cleaning/@55.712216,37.496811,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x3c30472ed2ad311!8m2!3d55.7122901!4d37.499062?hl=ru" />
                <div itemScope itemProp="address" itemType="http://schema.org/PostalAddress" className="cell">
                  <meta itemProp="streetAddress" content=" Минская д.1 Г, корп. 2, офис 29" />
                  <meta itemProp="addressLocality" content="Moscow" />
                  <meta itemProp="addressCountry" content="Russia" />
                  <meta itemProp="postalCode" content="121108" />
                  <h1>Закажите профессиональную уборку в Москве</h1>
                </div>
              </div>
            </section>

            <div className="header-buttons">
              <div className="grid-container show-for-small full">
                <div className="grid-x">
                  <div className="cell nav-button hover-cleaner-green">
                    <a className="card-link" href="/uslugi/uborka-kvartir"/>
                    <div className="spray-icon" />
                    <div className="button-title">
                      Уборка квартир
                    </div>
                    <i className="button-arrow" />
                  </div>
                  <div className="cell nav-button hover-handyman-teal">
                    <a className="card-link" href="/uslugi/uborka-kottedzhej" />
                    <div className="wrench-icon" />
                    <div className="button-title">
                      Уборка коттеджей
                    </div>
                    <i className="button-arrow" />
                  </div>
                  <div className="cell nav-button hover-handyman-teals">
                    <a className="card-link" href="/uslugi/uborka-ofisov" />
                    <div className="wrenchs-icon"></div>
                    <div className="button-title">
                      Уборка офисов
                    </div>
                    <i className="far fa-angle-right button-arrow" />
                  </div>
                  <div className="cell nav-button hover-perfect-blue">
                    <a className="card-link" href="/uslugi/himchistka" />
                    <div className="shop-icon" />
                    <div className="button-title">
                      Химчистка
                    </div>
                    <i className="button-arrow" />
                  </div>
                </div>
              </div>
              <div className="grid-container hide-for-small">
                <div className="grid-x grid-margin-x">
                  <div className="cell medium-3 nav-button hover-cleaner-green">
                    <a className="card-link" href="/uslugi/uborka-kvartir" />
                    <div className="spray-icon" />
                    <div className="button-title">
                      Уборка квартир
                    </div>
                  </div>
                  <div className="cell medium-3 nav-button hover-cleaner-teal">
                    <a className="card-link" href="/uslugi/uborka-kottedzhej" />
                    <div className="wrench-icon" />
                    <div className="button-title">
                      Уборка коттеджей
                    </div>
                  </div>
                 <div className="cell medium-3 nav-button hover-cleaner-teals">
                    <a className="card-link" href="/uslugi/uborka-ofisov" />
                    <div className="wrenchs-icon" />
                    <div className="button-title">
                      Уборка офисов
                    </div>
                  </div>
                  <div className="cell medium-3 nav-button hover-cleaner-blue">
                    <a className="card-link" href="/uslugi/himchistka" />
                    <div className="shop-icon" />
                    <div className="button-title">
                      Химчистка
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <section className="header-form">
              <div className="grid-container">
                <div className="grid-x">
                  <div className="cell small-12">
                    <div className="cell header-form-title">
                      <h2>Уборка квартиры</h2>
                      <p>Закажите безопасную уборку квартиры в Москве</p>
                    </div>

                    <div className="cell grid-x">
                      <div className="form-container small-12">
                        <form
                          className="requies-form"
                          acceptCharset="UTF-8"
                          id="headerForm"
                        >
                          <div className="cell">

                            <div className="small-12 columns when-page-errors">
                              При отправки вашей заявки произошла ошибка!
                            </div>


                            <div className="small-12 columns when-page-success">
                              Ваша заявка успешно отправлена!
                            </div>
                          </div>

                          <div className="cell grid-x">
                            <div className="cell small-12 form-element xlarge-3 ">
                              <input
                                className="when-font name"
                                placeholder="Имя"
                                type="text"
                                name="name"
                                required
                              />
                            </div>
                            <div className="cell grid-x small-12 form-element-apt xlarge-3">
                              <span
                                className="cell small-2 decrement"
                                role="button"
                                tabIndex={0}
                              >−</span>
                              <span className="cell auto description" data-type={0} data-types={apartmentTypesStr}>{apartmentTypes[0].name}</span>
                              <span
                                className="cell small-2 increment"
                                role="button"
                                tabIndex={0}
                              >+</span>
                            </div>
                            <div className="cell small-12 form-element xlarge-3">
                              <input
                                placeholder="Телефон"
                                className="when-font phone"
                                type="tel"
                                name="phone"
                                required
                              />
                            </div>
                            <div className="cell small-12 form-element xlarge-3">
                              <button className="booking-continue-button btn-continue initial-height hide-overflow btn btn-primary" type="submit">Рассчитать стоимость</button>
                            </div>
                          </div>

                        </form>

                        <div className="cell term_description">
                          <p>Нажимая «Рассчитать стоимость», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a></p>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            {/* Reliable */}
            <Reliable data={data.reliable} />

            {/* works */}
            <HowWeWorking data={data.howWeWorking} />

            {/* FAQ */}
            <Faq data={data.faq} />

            {/* footer form */}
            <section className="footer-form">
              <div className="grid-container">
                <div className="grid-x">
                  <div className="cell small-12">
                    <div className="cell header-form-title">
                      <h2>Заказать уборку квартиры в Москве</h2>
                    </div>

                    <div className="cell grid-x">
                      <div className="form-footer-container small-12">
                        <form
                          id="footerForm"
                          className="requies-form"
                        >
                          <div className="cell">

                            <div id="headerFormError" className="small-12 columns when-page-errors">
                              При отправки вашей заявки произошла ошибка!
                            </div>


                            <div id="headerFormSuccess" className="small-12 columns when-page-success">
                              Ваша заявка успешно отправлена!
                            </div>
                          </div>

                          <div className="cell grid-x">
                            <div className="cell small-12 form-element xlarge-3 ">
                              <input
                                className="when-font name"
                                placeholder="Имя"
                                type="text"
                                name="name"
                                required
                              />
                            </div>
                            <div className="cell grid-x small-12 form-element-apt xlarge-3">
                              <span
                                className="cell small-2 decrement"
                                role="button"
                                tabIndex={0}
                              >−</span>
                              <span className="cell auto description" data-type={0} data-types={apartmentTypesStr}>{apartmentTypes[0].name}</span>
                              <span
                                className="cell small-2 increment"
                                role="button"
                                tabIndex={0}
                              >+</span>
                            </div>
                            <div className="cell small-12 form-element xlarge-3">
                              <input
                                placeholder="Телефон"
                                className="when-font phone"
                                type="tel"
                                name="phone"
                                required
                              />
                            </div>
                            <div className="cell small-12 form-element xlarge-3">
                              <button className="booking-continue-button btn-continue initial-height hide-overflow btn btn-primary" type="submit">Рассчитать стоимость</button>
                            </div>
                          </div>

                        </form>

                        <div className="cell term_description">
                          <p>Нажимая «Рассчитать стоимость», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a></p>
                        </div>

                      </div>
                    </div>



                  </div>
                </div>
              </div>
            </section>

          </div>

          <div className="tooltip-form">
            <div className="tooltip-form__container">
              <div className="grid-container">
                <div className="grid-x">
                  <div className="cell small-12">
                    <form
                      id="tooltipForm"
                      className="requies-form"
                    >
                      <div className="cell">

                        <div className="small-12 columns when-page-errors">
                          При отправки вашей заявки произошла ошибка!
                        </div>


                        <div className="small-12 columns when-page-success">
                          Ваша заявка успешно отправлена!
                        </div>
                      </div>

                      <div className="cell grid-x">
                        <div className="cell small-12 form-element xlarge-3 ">
                          <input
                            className="when-font name"
                            placeholder="Имя"
                            type="text"
                            name="name"
                            required
                          />
                        </div>
                        <div className="cell grid-x small-12 form-element-apt xlarge-3">
                          <span
                            className="cell small-2 decrement"
                            role="button"
                            tabIndex={0}
                          >−</span>
                          <span className="cell auto description" data-type={0} data-types={apartmentTypesStr}>{apartmentTypes[0].name}</span>
                          <span
                            className="cell small-2 increment"
                            role="button"
                            tabIndex={0}
                          >+</span>
                        </div>
                        <div className="cell small-12 form-element xlarge-3">
                          <input
                            placeholder="Телефон"
                            className="when-font phone"
                            type="tel"
                            name="phone"
                            required
                          />
                        </div>
                        <div className="cell small-12 form-element xlarge-3">
                          <button className="booking-continue-button btn-continue initial-height hide-overflow btn btn-primary" type="submit">Рассчитать стоимость</button>
                        </div>
                      </div>
                    </form>

                    <div className="cell term_description">
                      <p>Нажимая «Рассчитать стоимость», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a></p>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </HomeComponent>

        <Footer/>
      </MainLayout>
    );
  }
};
