import styled from 'styled-components';

export const ErrorComponent:any = styled('div')`
  height: 39em;
  background-position: center bottom;
  background-image: url(/static/images/bg/gradient.webp);
  
  .background {
    background-image: url(/static/images/bg/404.webp);
    background-position: center bottom;
    background-repeat: no-repeat;
    height: 39em;
  }
  
  .content__container {
    padding-left: 0.9375rem;
    padding-right: 0.9375rem;
    width: 100%;
    float: left;
    margin-left: auto;
    margin-right: auto;
    float: none;
    padding-top: 6em;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    
    &-header {
      font-size: 2.5em;
      font-weight: 500;
      line-height: 120%;
    }
    
    &-subtitle {
      font-size: 1.2em;
      padding: 1em 1.25em 0 1.25em;
    }
  }

  @media only screen and (min-width: 32em) {
    .content__container {
      width: 32em;
    }
  }
`;
