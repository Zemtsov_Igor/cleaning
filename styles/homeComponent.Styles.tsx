import styled from 'styled-components';

export const HomeComponent:any = styled('main')`
  margin-top: 3em;
  margin-bottom: -23em;

  section {
    margin-bottom: 3em;

    &.section-empty {
      margin: 0;
      padding: 0;
    }
  }

  h1 {
    font-weight: bold;
    margin-bottom: 0.5rem;
    text-align: center;
    font-size: 1.75em;
  }

  .header-buttons {
    margin-bottom: 3rem;

    .nav-button {
      display: flex;
      position: relative;
      color: #434343;
      font-size: 1.125em;
      line-height: 1.25em;
      padding: 0;
      border-bottom: 1px solid #EAEAEA;
      border-right: 1px solid #eaeaea;
      transition: 0.5s;

      &:first-child {
        border-top: 1px solid #EAEAEA;
      }

      .spray-icon, .wrench-icon, .wrenchs-icon, .shop-icon {
        width: 48px;
        height: 100%;
        background-repeat: no-repeat;
        background-position: center center;
      }

      .spray-icon {
        background-color: #8BC53E;
        background-image: url(/static/images/spray_icon.png);
        background-size: 21px 34px;
      }

      .wrench-icon {
        background-color: #43D1B8;
        background-image: url(/static/images/spray_icon.png);
        background-size: 21px 34px;
      }

      .wrenchs-icon {
        background-color: #25cad2;
        background-image: url(/static/images/spray_icon.png);
        background-size: 21px 34px;
      }

      .shop-icon {
        background-color: #00CDED;
        background-image: url(/static/images/spray_icon.png);
        background-size: 21px 34px;
      }

      .button-title {
        display: block;
        font-weight: normal;
        font-size: 1rem;
        padding: 0.8rem 1.5rem;
      }

      .button-arrow {
        margin-left: auto;
        margin-right: 1.5rem;
        padding: 0.8rem 0;

        &:before {
          content: '';
          display: block;
          width: .5rem;
          height: 1rem;
          margin-top: -6px;
          top: 50%;
          position: absolute;
          right: 1rem;
          background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTYgNTEyIj48cGF0aCBmaWxsPSIjNjY2IiBkPSJNMjI0LjMgMjczbC0xMzYgMTM2Yy05LjQgOS40LTI0LjYgOS40LTMzLjkgMGwtMjIuNi0yMi42Yy05LjQtOS40LTkuNC0yNC42IDAtMzMuOWw5Ni40LTk2LjQtOTYuNC05Ni40Yy05LjQtOS40LTkuNC0yNC42IDAtMzMuOUw1NC4zIDEwM2M5LjQtOS40IDI0LjYtOS40IDMzLjkgMGwxMzYgMTM2YzkuNSA5LjQgOS41IDI0LjYuMSAzNHoiLz48L3N2Zz4=);
        }
      }
    }

    .card-link {
      width: 100%;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      z-index: 1;
    }

    .hover-cleaner-green {
      &:hover {
        color: #98CD52;
        border: 1px solid #98CD52;
      }
    }

    .hover-cleaner-green {
      &:hover {
        color: #98CD52;
        border: 1px solid #98CD52;
      }
    }

    .hover-cleaner-teal {
      &:hover {
        color: #68EED7;
        border: 1px solid #68EED7;
      }
    }

    .hover-cleaner-teals {
      &:hover {
        color: #25cad2;
        border: 1px solid #25cad2;
      }
    }

    .hover-cleaner-blue {
      &:hover {
        color: #07DEFF;
        border: 1px solid #07DEFF;
      }
    }
  }

  .header-form {
    &-title {
      text-align: center;
      padding-top: 0;
      padding-bottom: 1em;

      h2 {
        font-size: 1.5rem;
        margin-bottom: 1rem;
        line-height: 1.875rem;
      }

      p {
        margin-top: 0;
        padding-left: 0.5rem;
        padding-right: 0.5rem;
      }
    }
  }

  .form-container {
    padding: 2.5em 1em;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,.5);
    background-color: #fff;
    text-align: center;
    overflow: auto;

    .when-page-errors {
      display: none;
      color: #FF5C5C;
      font-size: 0.9em;
      margin: 0 0 1em 0;
      text-align: center;

      &.-visible {
        display: block;
      }

      &:before {
        content: '';
        display: inline-block;
        width: 0.8rem;
        height: 0.7rem;
        margin-right: 0.25em;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1NzYgNTEyIj48cGF0aCBmaWxsPSIjRkY1QzVDIiBkPSJNNTY5LjUxNyA0NDAuMDEzQzU4Ny45NzUgNDcyLjAwNyA1NjQuODA2IDUxMiA1MjcuOTQgNTEySDQ4LjA1NGMtMzYuOTM3IDAtNTkuOTk5LTQwLjA1NS00MS41NzctNzEuOTg3TDI0Ni40MjMgMjMuOTg1YzE4LjQ2Ny0zMi4wMDkgNjQuNzItMzEuOTUxIDgzLjE1NCAwbDIzOS45NCA0MTYuMDI4ek0yODggMzU0Yy0yNS40MDUgMC00NiAyMC41OTUtNDYgNDZzMjAuNTk1IDQ2IDQ2IDQ2IDQ2LTIwLjU5NSA0Ni00Ni0yMC41OTUtNDYtNDYtNDZ6bS00My42NzMtMTY1LjM0Nmw3LjQxOCAxMzZjLjM0NyA2LjM2NCA1LjYwOSAxMS4zNDYgMTEuOTgyIDExLjM0Nmg0OC41NDZjNi4zNzMgMCAxMS42MzUtNC45ODIgMTEuOTgyLTExLjM0Nmw3LjQxOC0xMzZjLjM3NS02Ljg3NC01LjA5OC0xMi42NTQtMTEuOTgyLTEyLjY1NGgtNjMuMzgzYy02Ljg4NCAwLTEyLjM1NiA1Ljc4LTExLjk4MSAxMi42NTR6Ij48L3BhdGg+PC9zdmc+);
      }
    }

    .when-page-success {
      display: none;
      color: #8CC63F;
      font-size: 0.9em;
      margin: 0 0 1em 0;
      text-align: center;

      &.-visible {
        display: block;
      }

      &:before {
        content: '';
        display: inline-block;
        width: 0.7rem;
        height: 0.8rem;
        margin-right: 0.25em;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOENDNjNGIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
      }
    }

    .form-element {
      padding-bottom: 0.5em;
    }

    .form-element-apt {
      margin-bottom: 0;
      padding-bottom: 0.5em;

      .decrement, .description, .increment {
        border: 1px solid #babbbd;
        user-select: none;
        color: #434343;
        padding: 0.625rem;
        outline: none;
      }

      .decrement, .increment {
        color: #A2A2A2;
        cursor: pointer;
        font-weight: 900;

        &:hover {
          background-color: #F0F2F4;
        }
      }

      .decrement {
        border-right-style: none;
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
      }

      .increment {
        border-left-style: none;
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
      }
    }

    input[type="text"], input[type="tel"] {
      font-size: 1em;
      padding-top: .75em;
      padding-bottom: .75em;
      line-height: 1.375em;
    }

    input.user-invalid {
      border: 1px solid #FF5C5C;
      box-shadow: inset 1px 1px #FF5C5C, inset -1px -1px #FF5C5C;
      transition: all 0.1s ease-in-out;
    }

    .btn {
      font-size: 1.125em;
      padding: .6em 0;
      margin-top: 0;
      width: 100%;
    }

    .term_description {
      color: #9cabb5;
      text-align: center;
      font-size: .75rem;
      font-weight: 300;
      line-height: .9375rem;
    }
  }

  .footer-form {
    .header-form-title {
      text-align: center;
      padding: 2.5rem 0.5rem;
      padding-bottom: 1em;

      h2 {
        font-size: 1.5rem;
        margin-bottom: 1rem;
        line-height: 1.875rem;
      }
    }
  }

  .form-footer-container {
    text-align: center;
    overflow: auto;

    .when-page-errors {
      display: none;
      color: #FF5C5C;
      font-size: 0.9em;
      margin: 1em 0 1em 0;
      text-align: center;

      &.-visible {
        display: block;
      }

      .fa-warning {
        margin-right: 0.25em;

        &:before {
          content: '\f071';
        }
      }
    }

    .when-page-success {
      display: none;
      color: #8CC63F;
      font-size: 0.9em;
      margin: 1em 0 1em 0;
      text-align: center;

      &.-visible {
        display: block;
      }

      .fa-check {
        margin-right: 0.25em;

        &:before {
          content: '\f00c';
        }
      }
    }

    .form-element {
     padding-bottom: 0.5em;
    }

    .form-element-apt {
      margin-bottom: 0;
      padding-bottom: 0.5em;

      .decrement, .description, .increment {
        border: 1px solid #babbbd;
        user-select: none;
        color: #434343;
        padding: 0.625rem;
        outline: none;
      }

      .decrement, .increment {
        color: #A2A2A2;
        cursor: pointer;
        font-weight: 900;

        &:hover {
          background-color: #F0F2F4;
        }
      }

      .decrement {
        border-right-style: none;
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
      }

      .increment {
        border-left-style: none;
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
      }
    }

    input[type="text"], input[type="tel"] {
      font-size: 1em;
      padding-top: .75em;
      padding-bottom: .75em;
      line-height: 1.375em;
    }

    input.user-invalid {
      border: 1px solid #FF5C5C;
      box-shadow: inset 1px 1px #FF5C5C, inset -1px -1px #FF5C5C;
      transition: all 0.1s ease-in-out;
    }

    .btn {
      font-size: 1.125em;
      padding: .6em 0;
      margin-top: 0;
      width: 100%;
    }

    .term_description {
      color: #9cabb5;
      text-align: center;
      font-size: .75rem;
      font-weight: 300;
      line-height: .9375rem;
    }
  }

  .head__description {
    margin-bottom: 2rem;

    p {
      font-size: 1.1rem;
      margin-bottom: 1em;
    }
  }

  .tooltip-form {
    z-index: 10;
    position: sticky;
    left: 0;
    right: 0;
    bottom: 0;
    transition: all 0.4s ease-in-out 0s;
    box-shadow: rgba(0, 0, 0, 0.3) 0px 2px 6px 0px;
    margin-bottom: 0;
    padding-bottom: 0;

    transform: translateY(130px);

    &.-visible {
      bottom: 0px;
      transform: translateY(0);
    }

    .tooltip-form__container {
      padding: 1.5em 0;
      background-color: #fff;
      text-align: center;
      overflow: auto;

      .when-page-errors {
        display: none;
        color: #FF5C5C;
        font-size: 0.9em;
        margin: 1em 0 1em 0;
        text-align: center;

        &.-visible {
          display: block;
        }

        .fa-warning {
          margin-right: 0.25em;

          &:before {
            content: '\f071';
          }
        }
      }

      .when-page-success {
        display: none;
        color: #8CC63F;
        font-size: 0.9em;
        margin: 1em 0 1em 0;
        text-align: center;

        &.-visible {
          display: block;
        }

        .fa-check {
          margin-right: 0.25em;

          &:before {
            content: '\f00c';
          }
        }
      }

      .form-element {
        padding-bottom: 0.5em;
      }

      .form-element-apt {
        margin-bottom: 0;
        padding-bottom: 0.5em;

        .decrement, .description, .increment {
          border: 1px solid #babbbd;
          user-select: none;
          color: #434343;
          padding: 0.625rem;
          outline: none;
        }

        .decrement, .increment {
          color: #A2A2A2;
          cursor: pointer;
          font-weight: 900;

          &:hover {
            background-color: #F0F2F4;
          }
        }

        .decrement {
          border-right-style: none;
          border-top-left-radius: 3px;
          border-bottom-left-radius: 3px;
        }

        .increment {
          border-left-style: none;
          border-top-right-radius: 3px;
          border-bottom-right-radius: 3px;
        }
      }

      input[type="text"], input[type="tel"] {
        font-size: 1em;
        padding-top: .75em;
        padding-bottom: .75em;
        line-height: 1.375em;
      }

      input.user-invalid {
        border: 1px solid #FF5C5C;
        box-shadow: inset 1px 1px #FF5C5C, inset -1px -1px #FF5C5C;
        transition: all 0.1s ease-in-out;
      }

      .btn {
        font-size: 1.125em;
        padding: .6em 0;
        margin-top: 0;
        width: 100%;
      }

      .term_description {
        color: #9cabb5;
        text-align: center;
        font-size: .75rem;
        font-weight: 300;
        line-height: .9375rem;
      }
    }
  }

  @media only screen and (max-width: 64.0625em) {
    .tooltip-form {
      visibility: hidden;
    }
  }

  @media only screen and (min-width: 40.0625em) { //640px
    margin-bottom: -22em;

    h1 {
      font-size: 2.5em;
      padding: 0;
      line-height: 1em;
      margin-bottom: 1rem;
    }

    .header-buttons {
      margin-bottom: 4.5rem;

      .nav-button {
        font-size: 1rem;
        font-weight: bold;
        text-align: center;
        border: 1px solid #EAEAEA;
        box-shadow: 0 2px 4px 0 rgba(0,0,0,0.2);

        .spray-icon, .wrench-icon, .wrenchs-icon, .shop-icon {
          width: 72px;
        }

        .button-title {
          padding: 1.625em 0;
          text-align: center;
          margin: auto;
        }
      }
    }

    .header-form {
      &-title {
        padding-top: 0;
        padding-bottom: 1em;

        h2 {
          font-size: 2rem;
          display: inline-block;
        }

        p {
          padding-left: 0;
          padding-right: 0;
        }
      }
    }
  }

  @media only screen and (min-width: 48.0625em) {
    margin-bottom: -20em;
  }

  @media only screen and (min-width: 64.0625em) {
    margin-bottom: -9em;

    .header-buttons {
      border-right: 0;
    }

    .form-container {
      .form-element {
        padding-bottom: 0;
        padding-right: .5em;
        padding-left: .5em;
      }

      .form-element-apt {
        padding-bottom: 1.01em;
        padding-right: .5em;
        padding-left: .5em;
      }
    }

    .form-footer-container {
      .form-element {
        padding-bottom: 0;
        padding-right: .5em;
        padding-left: .5em;
      }

      .form-element-apt {
        padding-bottom: 1.01em;
        padding-right: .5em;
        padding-left: .5em;
      }
    }

    .tooltip-form {

      .tooltip-form__container {
        .form-element {
          padding-bottom: 0;
          padding-right: .5em;
          padding-left: .5em;
        }

        .form-element-apt {
          padding-bottom: 1.01em;
          padding-right: .5em;
          padding-left: .5em;
        }
      }
    }

  }
`;
