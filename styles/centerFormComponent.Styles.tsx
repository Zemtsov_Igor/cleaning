import styled from 'styled-components';

export const CenterFormComponent:any = styled('div')`
  position: relative;
  *zoom: 1;
  z-index: 1;
  width: 100%;

  &:before, &r:after {
    content: " ";
    display: table;
  }

  .quote-bg {
    min-height: 24.25em;
    max-width: 100%;
  }

  .quote-request-form {
    .quote-form-container {
      padding: 2.5em 1em;
      box-shadow: 0 2px 4px 0 rgba(0,0,0,0.5);
      margin-top: -15.5em;
      background-color: #FFF;

      .header {
        margin-bottom: 1em;

        h1 {
          text-align: center;
        }
      }

      .rating {
        margin-bottom: 2em;

        .rating-stars {
          display: inline-block;
          padding-right: .5rem;

          .star {
            position: relative;
            display: inline-block;
            height: 1em;
            width: 1em;
            margin-right: 4px;

            i {
              position: absolute;
              left: 0;
              top: 0;
              height: 100%;
              vertical-align: top;
              overflow: hidden;
              color: #FFB600;
            }
          }

          .fa-star {
            &:before {
              content: "\f005";
            }
          }
        }

        .review_count {
          background: 0;
          border: 0;
          border-bottom: 0.5px solid #434343;
          color: #434343;
          font-size: 1em;
          cursor: pointer;
        }
      }

      .when-page-errors {
        display: none;
        color: #FF5C5C;
        font-size: 0.9em;
        margin: 1em 0 1em 0;
        text-align: center;

        &.-visible {
          display: block;
        }

        .checkout-row {
          div {
            &:before {
              content: '';
              display: inline-block;
              width: 0.8rem;
              height: 0.7rem;
              margin-right: 0.25em;
              background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1NzYgNTEyIj48cGF0aCBmaWxsPSIjRkY1QzVDIiBkPSJNNTY5LjUxNyA0NDAuMDEzQzU4Ny45NzUgNDcyLjAwNyA1NjQuODA2IDUxMiA1MjcuOTQgNTEySDQ4LjA1NGMtMzYuOTM3IDAtNTkuOTk5LTQwLjA1NS00MS41NzctNzEuOTg3TDI0Ni40MjMgMjMuOTg1YzE4LjQ2Ny0zMi4wMDkgNjQuNzItMzEuOTUxIDgzLjE1NCAwbDIzOS45NCA0MTYuMDI4ek0yODggMzU0Yy0yNS40MDUgMC00NiAyMC41OTUtNDYgNDZzMjAuNTk1IDQ2IDQ2IDQ2IDQ2LTIwLjU5NSA0Ni00Ni0yMC41OTUtNDYtNDYtNDZ6bS00My42NzMtMTY1LjM0Nmw3LjQxOCAxMzZjLjM0NyA2LjM2NCA1LjYwOSAxMS4zNDYgMTEuOTgyIDExLjM0Nmg0OC41NDZjNi4zNzMgMCAxMS42MzUtNC45ODIgMTEuOTgyLTExLjM0Nmw3LjQxOC0xMzZjLjM3NS02Ljg3NC01LjA5OC0xMi42NTQtMTEuOTgyLTEyLjY1NGgtNjMuMzgzYy02Ljg4NCAwLTEyLjM1NiA1Ljc4LTExLjk4MSAxMi42NTR6Ij48L3BhdGg+PC9zdmc+);
            }
          }
        }
      }

      .when-page-success {
        display: none;
        color: #8CC63F;
        font-size: 0.9em;
        margin: 1em 0 1em 0;
        text-align: center;

        &.-visible {
          display: block;
        }
        
        .checkout-row {
          div {
            &:before {
              content: '';
              display: inline-block;
              width: 0.7rem;
              height: 0.8rem;
              margin-right: 0.25em;
              background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOENDNjNGIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
            }
          }
        }
      }

      .form-element {
        padding-bottom: 1em;
      }

      input[type="text"], input[type="tel"], input[type="email"], input[type="date"], input[type="time"], select {
        font-size: 1em;
        padding-top: .75em;
        padding-bottom: .75em;
        line-height: 1.375em;
      }

      select {
        padding: .75em 2em .75em .75em;
      }

    input[type="date"], input[type="time"] {
        appearance: none;
        border: 1px solid #babbbd;
        width: 100%;
        padding: .69em .75em .69em .75em;
        line-height: 1.375em;
        font-size: 1em;
        line-height: 1.4em;
        cursor: pointer;
        display: block;
        background-color: transparent;
        text-indent: .01px;
        text-overflow: '';
        color: #434343;
        outline: 0;
      }

      .dropdown-date-selector {
        input {
          cursor: pointer;
        }
      }

      input.user-invalid {
        border: 1px solid #FF5C5C;
        box-shadow: inset 1px 1px #FF5C5C, inset -1px -1px #FF5C5C;
        transition: all 0.1s ease-in-out;
      }

      .btn {
        font-size: 1.125em;
        padding: .6em 0;
        margin-top: 1.25em;
        width: 100%;
      }

      .select-wrapper {
        border-radius: 3px;
        position: relative;
        overflow: hidden;
        color: #434343;
        border: 1px solid #BABBBD;
        margin: 0;

        &:after {
          content: "\f107";
          cursor: pointer;
          font-family: "Font Awesome 5 Pro";
          position: absolute;
          right: 0.75em;
          top: .625em;
          pointer-events: none;
        }
      }

      .adjacent-left {
        margin-left: -1px;
        border-radius: 3px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
      }

      .adjacent-right {
        padding-right: 0.1em;
        border-radius: 3px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
      }
    }
  }

  .disclaimer {
    color: #9CABB5;
    text-align: center;
    font-size: 0.75rem;
    font-weight: 300;
    line-height: 0.9375rem;
    padding: 1rem 1.4rem 1rem 1.4rem;
  }

  .head__description {
    margin-bottom: 3rem;
  }


  .get-started {
    min-width: 100px;
    max-width: 300px;

    .btn {
      margin-bottom: 0;
      padding: 0.85em 0.85em;
      font-size: 1.25rem;
      line-height: 1;
      width: 100%;
      color: white;
      border-radius: 3px;
    }
  }

  .page_text__content {
    margin-bottom: 2rem;

    h2 {
      padding: 0.5em 0;
    }

    h3 {
      padding: 0.5em 0;
      font-size: 1.625em;
      color: #434343;
      line-height: 1.25em;
      font-weight: 400;
    }

    h4 {
      margin-top: 0;
      font-weight: bold;
    }

    p {
      margin-bottom: 2rem;

      &:last-child {
        margin-bottom: 0;
      }
    }
  }

  .CalendarMonth_caption {
    background-color: #0bb8e3;

    > * {
      font-weight: 400;
    }
  }

  .CalendarDay_button {
    outline: none;
  }

  .CalendarDay__selected {
    border: 0;
    background-color: #8cc63f;
  }

  .DayPickerKeyboardShortcuts_show {
    border: 2px solid #8cc63f;
    color: #8cc63f;
    outline: none;
  }

  .DayPickerKeyboardShortcuts_showSpan {
    display: block;
    margin-top: -2px;
    color: #8cc63f;
  }

  @media only screen and (max-width: 40.0625em) { //640px
    .quote-request-form {
      .quote-form-container {
        box-shadow: none;
        padding-bottom: 0;
      }
    }

    .page_text__content {
      h3 {
        font-size: 2rem;
      }
    }
  }

  @media only screen and (max-width: 85.3125em) { //1365px
    .quote-bg {
      min-height: 16em;
    }
  }

  @media only screen and (max-width: 64.0625em) { //1024px

  }

  @media only screen and (max-width: 64.0625em) and (min-width: 40.0625em) {
  }

  @media only screen and (min-width: 64.0625em) { //1024px
    .quote-bg {
      min-height: 24.25em;
      max-width: 100%;
      background-size: cover;
    }

    .quote-request-form {
      .quote-form-container {
        .form-element {
          padding-bottom: 1.5em;
          padding-right: 0.5em;
          padding-left: 0.5em;
        }

        .btn {
          margin-top: 0;
        }
      }
    }

    .page_text__content {
      h3 {
        font-size: 2em;
        line-height: 1.25em;
      }
    }
  }
`;
