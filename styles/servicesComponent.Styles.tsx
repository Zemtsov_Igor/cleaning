import styled from 'styled-components';

export const ServicesComponent:any = styled('div')`
  .hero-section {
    align-items: center;
    background: url(/static/images/services_landing_hero_web.png) no-repeat 100%;
    background-size: cover;
    display: flex;
    height: 387px;
    justify-content: center;
    text-align: center;

    h1 {
      color: #fff;
      font-size: 1.4em;
      font-weight: 700;
    }
  }

  .service-categories-container {
    padding-top: 1.125rem;
    padding-bottom: 2rem;
  }

  .stickybar {
    position: sticky;
    position: -webkit-sticky;
    align-self: flex-start;
    top: 1rem;

    .all-categories {
      margin-bottom: 2.25rem;
    }

    .categories-list {
      font-size: 1.125rem;

      ul {
        list-style-type: none;
        margin: 0;

        li {
          margin-bottom: 1rem;

          .service-category-link {
            color: #434343;
            font-size: 1.125rem;
            background: 0;
            border: 0;
            cursor: pointer;
          }

          .clicked-category {
            opacity: 0;
            visibility: hidden;
            position: absolute;
            left: 0;

            .service-category-link {
              font-weight: bold;
            }
          }
        }
      }

      &__item {
        &.-clicked {
          .service-category-link {
            font-weight: 700;
          }
        }
      }
    }
  }

  .scrolling-wrapper {
    overflow-x: auto;
    overflow-y: hidden;
    white-space: nowrap;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border-top: 1px solid #F4F4F4;
    border-bottom: 1px solid #F4F4F4;
    margin: 0;
    width: 100%;
    background-color: #ffffff;
    z-index: 10;
    top: -1px;

    .scrollable-item {
      display: inline-block;
      position: relative;
      margin: 1.125rem 1rem;

      .service-category-link {
        color: #434343;
        font-size: 1.125rem;
        background: 0;
        border: 0;
        cursor: pointer;
      }

      .clicked-category {
        opacity: 0;
        visibility: hidden;
        position: absolute;
        left: 0;

        .service-category-link {
          font-weight: normal;
          text-decoration: underline;
          color: #00CDED;
        }
      }
    }
  }

  .category-list-block {
    margin-top: 0.5rem;

    .category-anchor {
      padding-bottom: 1rem;
    }

    .category-header {
      font-size: 1.25rem;
      line-height: 1.4;
      font-weight: normal;
      margin-top: 0;
      margin-bottom: 1.5rem;
      text-align: left;
    }

    .services-grid {
      margin-bottom: 1rem;

      .card {
        display: flex;
        flex-direction: column;
        flex-grow: 0;
        position: relative;
        margin-bottom: 1rem;
        padding: 0;
        text-align: center;
        border: 1px solid #e6e6e6;
        border-radius: 0;
        background: #ffffff;
        box-shadow: none;
        overflow: hidden;
        color: #434343;

        .card-link {
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0;
          left: 0;
          z-index: 1;
        }

        .thumbnail {
          display: inline-block;
          max-width: 100%;
          margin-bottom: 0;
          border: 0;
          border-radius: 0;
          box-shadow: unset;
          line-height: 0;
          min-height: 168px;
        }

        .title {
          padding: 1rem;
          font-size: 0.875rem;
          font-weight: 500;
        }

        > {
          &:last-child {
            margin-bottom: 0;
          }
        }
      }

      .card-shadow {
        box-shadow: 0 2px 4px 0 rgba(0,0,0,0.1);
      }

    }

    .services-list {
      margin-bottom: 1.5rem;

      .cell {
        margin-bottom: 1.5rem;
      }

      .service-link {
        color: #434343;
      }
    }
  }

  @media screen and (max-device-width: 812px) { //812px
    .scrolling-wrapper {
      display: none;
    }
  }

  @media only screen and (min-width: 40.0625em) { //640px
    .service-categories-container {
      padding-top: 2.75rem;
      padding-bottom: 8rem;
    }

    .category-list-block {
      margin-top: 4.25rem;

      .services-grid {
        margin-bottom: 0;

        .card {
          margin-bottom: 2rem;

          .title {
            font-size: 1rem;
          }
        }
      }

      .services-list {
        margin-bottom: 3rem;

        .cell {
          margin-bottom: 1rem;
        }
      }
    }
  }
`;
