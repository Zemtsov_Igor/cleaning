import styled from 'styled-components';

export const GuaranteeComponent:any = styled('div')`
  .our-guarantee {
    font-size: 1.2em;
    background-color: #F4F4F4;

    .section {
      min-height: 300px;
      background-color: #F4F4F4;

      &:nth-of-type(odd) {
        background-color: #fff;
      }
    }
    .centered {
      justify-content: center;
      align-items: center;
    }
    .title {
      font-size: 32px;
      padding-bottom: 0.5em;
      text-align: center;
    }
    .subtitle {
      font-size: 1rem;
    }
    .text-block {
      max-width: 45ch;
    }
    .our-guarantee-hero {
      align-items: center;
      background-image: url(/static/images/safety_background.jpg);
      background-size: auto;
      background-position: left;
      background-repeat: no-repeat;
      height: 495px;

      .our-guarantee-hero-box {
        background-color: #fff;
        box-shadow: 0 2px 29px 0 rgba(0,0,0,0.1);
        text-align: center;
        margin: 1em;
        padding: 1em;

        h1 {
          padding-bottom: 0.5em;
          font-size: 34px;
          line-height: 1.12;
          letter-spacing: -0.6px;
          font-weight: normal;
          text-align: center;
        }

        .subtitle {
          font-size: 1.2rem;
          color: #9CABB5;
          padding-top: 0.4em;
        }
      }
    }
    .trust-customers {
      max-width: 768px;
      padding: 2em;
      margin: auto;

      .privacy-button {
        margin-top: 1em;
      }
    }
    .trust-happiness {
      position: relative;

      .trust-happiness-container {
        display: flex;
        padding: 2em;
        margin: auto;
        max-width: 768px;

        .trust-icon {
          height: 151px;
          width: 151px;
          margin-right: 1em;
          margin-left: 5em;
        }

        .trust-happiness-text {
          margin-left: 50px;
          width: 50%;
        }
      }
    }
    .trust-serious {
      padding: 2em;

      .serious-container {
        margin: 2em auto;
        padding: 4em 2em;
        background: white;
        max-width: 768px;
        display: block;
        box-shadow: 0 2px 4px 0 rgba(0,0,0,0.2);
        -webkit-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.2);
        -moz-box-shadow: 0 2px 4px 0 rgba(0,0,0,0.2);

        .subtitle {
          text-align: center;
        }
      }
    }
    .trust-pro {
      .image-background {
        background-image: url(/static/images/pro-pic.jpg);
        background-size: cover;
        background-position: left;
        background-repeat: no-repeat;
        overflow: hidden;
        height: 800px;
        width: 50%;
      }
      .side-pro-section {
        padding-left: 1em;
        padding-top: 4em;

        .title {
          text-align: left;
        }

        .subtitle {
          margin-bottom: 24px;
        }

        .screening-list-item {
          display: flex;
          margin-bottom: 1em;

          .checkmark {
            color: #8bc53e;
            font-weight: bold;
            margin-right: .4em;
          }
          .float-left {
            max-width: 91%;
          }
          .screening-title {
            font-weight: bold;
          }
          .screening-body {
            color: #9CABB5;
          }
        }
      }

      .process-button {
        margin-top: 1em;
        margin-bottom: 1em;
      }
    }

    .trust-contact-us {
      width: 75%;
      padding: 2em;
      margin: auto;

      .subtitle {
        text-align: center;
        margin-bottom: 1em;
      }
    }

    .trust-book {
      margin: auto;
      padding: 2em;

      .subtitle {
        margin-bottom: 1em;
        text-align: center;
      }

      .buttons-wrapper {
        margin: auto;
        display: block;

        .button {
          max-width: 13em;
          margin: 2px;

          &:first-child {
            margin-bottom: 0;
            margin-right: 1em;
          }
        }
      }
    }

    &.-guarantee {
      .section {
        background-color: #fff;

        &:nth-of-type(odd) {
          background-color: #F4F4F4;
        }
      }

      .our-guarantee-hero
        background-image: url(/static/images/bg-flip.jpg);
      }
    }

    .our-guarantee-pro {
      height: 400px;
      background-image: url(/static/images/our-guarantee-pro.png);
      background-position: right;
      background-size: contain;
      background-repeat: no-repeat;

      .our-guarantee-pro-container {
        margin: 3em 3.5em;
        margin-right: 25%;
      }

      .title {
        font-size: 32px;
        font-weight: bold;
        padding-bottom: 0.5em;
        text-align: left;
      }

      .subtitle {
        font-size: 1.1rem;
      }
    }

    .our-guarantee-pro-review {
      padding: 3.5rem 0;

      .pro-review-box-small {
        background-color: #fff;
        width: 1.25em;
        height: 400px;
        border-radius: 5px;
        margin-right: 1.5em;
      }

      .pro-review-box {
        background-color: #fff;
        width: 300px;
        border-radius: 5px;
        padding: 1.5em;
        margin: 0 1.5em;
        align-items: center;

        .img {
          width: 78px;
          height: 78px;
          max-height: 78px;
          margin-bottom: 1em;
          border-radius: 50%;
        }

        .pro-name {
          font-size: 20px;
          font-weight: bold;
          margin-bottom: 0.2em;
        }

        .pro-stars {
          font-size: 24px;
          margin: 1em;

          .star {
            position: relative;
            display: inline-block;
            color: #FFB600;
            height: 1em;
            width: 1em;
            margin: 0 0.05em;

            > {
              * {
                position: absolute;
                top: 0;
                height: 100%;

                &:not(.blocker) {
                  left: 0;
                }
              }
            }

            .star-under {
              vertical-align: top;
            }

            .star-over {
              position: absolute;
              left: 0;
              top: 0;
              overflow: hidden;
            }

            .fa-star {
              &:before {
                content: "\f005";
              }
            }

            .fa-star-o {
              &:before {
                content: "\f006";
              }
            }
          }
        }

        .pro-rating-number {
          font-size: 18px;
          color: #9CABB5;
          padding-left: 0.3em;
        }

        .customer-review-line {
          width: 245px;
          border: solid 1px #eaeaea;
          margin-bottom: 1em;
        }

        .customer-review {
          margin-bottom: 1em;
        }

        .customer-review-statement {
          font-size: 16px;
          font-style: italic;
          line-height: 1.31;
          text-align: center;
          margin-bottom: 1em;
        }
      }

      .description {
        margin: 1.5em;

        .title {
          font-size: 32px;
          font-weight: 700;
          padding-bottom: 0.5em;
          text-align: left;
        }
      }
    }

    .our-guarantee-pros-rectangle {
      background-image: url(/static/images/pros.png);
      background-repeat: no-repeat;
      background-position: 100% 100%;
      background-size: 500px auto;

      .our-guarantee-container-pros {
        margin: 3em 3.5em;
        margin-right: 25%;
      }

      .title {
        font-size: 32px;
        font-weight: bold;
        padding-bottom: 0.5em;
        text-align: left;
      }

      .subtitle {
        font-size: 1.1rem;
      }
    }

    .our-guarantee-contact-us {
      background-image: url(//cache.hbfiles.com/assets/our-guarantee/seal-small-dkgrey.png);
      background-repeat: no-repeat;
      background-position: 90% 0%;
      padding: 0 1em;
      text-align: center;

      .title {
        font-size: 24px;
        font-weight: normal;
      }

      .btn {
        font-size: 1.25em;
        padding-top: 0.5em;
        padding-bottom: 0.5em;
      }
    }

    .our-guarantee-ready-to-book {
      .title {
        font-size: 32px;
        font-weight: bold;
        padding-bottom: 0.5em;
      }

      .btn {
        font-size: 1.1em;
        padding-top: 0.5em;
        padding-bottom: 0.5em;

        &:first-child {
          margin-bottom: 0;
          margin-right: 1em;
        }
      }
    }
  }

  @media only screen and (max-width: 40.0625em) { //640px
    .our-guarantee {
      .trust-happiness {
        .trust-happiness-container {
          flex-direction: column;

          .trust-icon {
            display: none;
          }

          .trust-happiness-text {
            width: 100%;
            margin-left: 0;
          }
        }
      }
      .trust-serious {
        .serious-container {
          margin: 0;
          padding: 0;
          background: none;
          box-shadow: none;
        }
      }
      .trust-pro {
        .image-background {
          display: none;
        }

        .side-pro-section {
          padding: 4em 2em 2em 2em;

          .screening-list-item {
            .checkmark {
              display: none;
            }
          }
        }
        .process-button {
          width: 100%;
        }
      }

      .trust-contact-us {
        width: 100%;
        padding: 4em 2em 2em 2em;
        margin: 0;
      }

      .trust-book {
        .buttons-wrapper {
          display: grid;
        }
      }

      .hide-small {
        display: none;
      }

      .our-guarantee-pro {
        background-image: none;
        height: auto;

        .our-guarantee-pro-container {
          margin: 2em 1.5em;
        }
      }

      .our-guarantee-pro-review {
        &.flex-rows-reverse {
          flex-flow: column-reverse nowrap;
        }

        .pro-review-box {
          margin: 0 0.5em;
          margin-bottom: 1.5em;
        }

        .description {
          margin-top: 0;

          .title {
            font-size: 26px;
          }
        }
      }

      .our-guarantee-pros-rectangle {
        background-image: none;

        .our-guarantee-container-pros {
          margin: 5em 1.5em;
        }
      }

      .our-guarantee-contact-us {
        .btn {
          width: 100%;
          padding-left: 0;
          padding-right: 0;
        }
      }

      .our-guarantee-ready-to-book {
        .btn {
          width: 100%;
          padding-left: 0;
          padding-right: 0;
          margin-bottom: 1em;
        }
      }
    }
  }

  @media only screen and (min-width: 40.0625em) { //640px
    .our-guarantee {
      .our-guarantee-hero {
        justify-content: center;

        .subtitle {
          padding: 0.4em 5em 0;
        }

        .our-guarantee-hero-box {
          width: 630px;
          height: 340px;

          h1 {
            font-size: 40px;
          }
        }
      }
    }

    .our-guarantee-pro-review {
      .pro-review-box {
        .pro-stars {
          .star {
            > {
              * {
                margin-left: 0;
              }
            }
          }
        }
      }
    }
  }
`;