import styled from 'styled-components';

export const PageComponent:any = styled('div')`
  width: 100%;
  position: relative;
  *zoom: 1;
  z-index: 1;

  .gradient-background {
    background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4gPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJncmFkIiBncmFkaWVudFVuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeDE9IjAuNSIgeTE9IjAuMCIgeDI9IjAuNSIgeTI9IjEuMCI+PHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2VkZjVmOCIvPjxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIvPjwvbGluZWFyR3JhZGllbnQ+PC9kZWZzPjxyZWN0IHg9IjAiIHk9IjAiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==);
    background-size: 100%;
    background-image: linear-gradient(#edf5f8, #fff);
    padding-top: 4em;
  }

  .content-container {
    background-color: #fff;
    padding-top: 4em;
    padding-bottom: 4em;
    border-radius: 4px;
    *zoom: 1;

    h1 {
      text-align: center;
      padding-bottom: 1em;
    }

    .subheader {
      text-align: center;
      margin-top: 0;
      line-height: 1.61em;
      padding-bottom: 1em;
    }

    .paragraph-content {
      padding-bottom: 0.5em;

      hr {
        display: block;
        margin-top: 1em;
        margin-bottom: 1em;
        border-top: 1px solid #CCCCCC;
        border-bottom: none;
        border-left: none;
      }
    
      p {
        padding-bottom: 1em;
        font-size: 1em;

        strong {
          font-weight: bold;

          u {
            font-weight: bold;
          }
        }
      }

      ol {
        list-style: none;
        margin-left: 1em;

        li {
          display: flex;
          margin-bottom: 0.5em;

          span {
            margin-right: 5px;
          }
        }

        ul {
          margin-left: 2em;
          font-size: 1em;

          li {
            margin-bottom: 0.5em;
            
            &:before {
              content: '';
              display: block;
              width: 0.6rem;
              height: 0.6rem;
              margin-top: .6em;
              margin-right: 5px;
              background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjNDM0MzQzIiBkPSJNMjU2IDhDMTE5IDggOCAxMTkgOCAyNTZzMTExIDI0OCAyNDggMjQ4IDI0OC0xMTEgMjQ4LTI0OFMzOTMgOCAyNTYgOHoiLz48L3N2Zz4=);
              background-repeat: no-repeat;
            }
          }
        }
      }

      

      .btn-link {
        background: none;
        border: 0;
        color: #0bb8e3;
        font-size: 1em;
        margin-left: 0.25rem;
        cursor: pointer;
      }

      ul {
        font-size: 0.9em;
        margin: 1.2em;

        li {
          list-style-type: disc;
        }
      }
    }

    .sitemap-content {
      padding-bottom: 0.5em;

      ul {
        margin: 1.2em;
        font-size: 1em;
        list-style: none;

        li {
          margin-bottom: 0.5em;
          
          &:before {
            content: '';
            display: inline-block;
            width: 1rem;
            height: 1rem;
            margin-top: .6em;
            margin-right: 5px;
            background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOGJjNTNlIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
            background-repeat: no-repeat;
          }

          p {
            display: inline-block;
          }
        }
      }
    }

    .promo-description-row {
      display: flex;
      width: 100%;
      margin-left: auto;
      margin-right: auto;
      margin-top: 0;
      margin-bottom: 0;
      max-width: 960px;
      *zoom: 1;
      padding-top: 2em;
      padding-bottom: 2em;
      border-bottom: 1px dashed #E1E1E1;

      &:last-of-type {
        border-bottom: none;
      }

      .promo-description-row-right {
        padding-right: 0.9375em;
      }

      .promo-description-row-left {
        padding-left: 0.9375em;

        .header-container {
          margin-top: 1em;
          margin-bottom: 1em;
        }
      }

      .img {
        display: block;
        padding: 3px;
        width: 100%;
        height: 236px;
        margin: 0 auto;
      }

      .promo-description-list-item-title {
        font-size: 1.6em;
        max-width: 14em;
        font-weight: 500;
        line-height: 1.61em;
        margin-bottom: 0;
      }

      .promo-benefit-list {
        li {
          padding-left: 1em;
          position: relative;
          margin-bottom: .25em;
          list-style-type: none;

          &:before {
            content: '·';
            position: absolute;
            font-weight: 700;
            left: 0;
          }
        }
      }
    }
  }

  .quote-bg {
    width: 100%;
    background-repeat: no-repeat;
    background-position: center;
    text-align: center;
    height: 12.5em;
    padding-top: 5em;
    background-color: #f7f7f9;
    background-image: url(/static/images/about-header-mobile.jpg);
    background-size: 100%;

    h1 {
      color: #424242;
      font-size: 1.4em;
      font-weight: bold;
    }
  }

  .blank-background {
    background-color: #fff;
    padding-bottom: 4em;

    &.-contacts {
      margin-top: 5em;
    }

    .about-content {
      padding-top: 1.5em;

      h2 {
        font-size: 1.3em;
        font-weight: 600;
        text-transform: uppercase;
      }

      h3 {
        font-size: 1.2em;
        font-weight: bold;
      }

      p {
        padding-top: 1em;
        font-size: 0.95em;
      }
    }

    .bio-content {
      padding-top: 1.5em;

      h3 {
        font-size: 1.2em;
        font-weight: bold;
      }

      p {
        padding-top: 0.7em;
        font-size: 0.95em;
      }
    }
  }

  .contacts-col-item {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    padding-right: 2em;
    margin-bottom: 2em;

    &__text {
      font-size: 1em;
      color: rgb(166, 166, 166);
      font-weight: 500;
      line-height: 25px;
      margin-right: 2em;
    }

    &__link {
      font-size: 1em;
      color: rgb(34, 34, 34);
      font-weight: 500;
      line-height: 25px;
    }
  }
  
  .map {
    margin-top: 80px;
  }

  @media only screen and (min-width: 40.0625em) { //640px
    .content-container {
      .promo-description-row {
        .promo-description-row-left {
          .header-container {
            margin-top: 0;
          }
        }
      }
    }

    .quote-bg {
      background-image: url(/static/images/about-header.jpg);
      background-size: auto;
    }
  }
`;
