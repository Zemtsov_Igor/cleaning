import styled from 'styled-components';

export const RightFormComponent:any = styled('div')`
  position: relative;
  *zoom: 1;
  z-index: 1;
  width: 100%;

  &:before, &r:after {
    content: " ";
    display: table;
  }

  p {
    font-size: 1.1rem;
    margin-bottom: 1em;
  }

  .quote-bg {
    margin-bottom: 2rem;
    padding-bottom: 1em;

    .service-image {
      height: 280px;
      background-position: 0 100%;
      background-size: cover;
      background-repeat: no-repeat;
    }
  }

  .head-container {
    margin-bottom: 4rem;

    h1 {
      font-weight: bold;
      margin-bottom: .5rem;
      font-size: 2.5rem;
      text-align: left;
    }

    .rating {
      margin-bottom: 1em;

      .stars {
        padding-right: 1rem;
      }

      .review_count {
        background: 0;
        border: 0;
        border-bottom: 0.5px solid #434343;
        color: #434343;
        font-size: 1em;
        cursor: pointer;
      }
    }
  }

  .head__description {
    margin-bottom: 2rem;
  }

  .media-object {
    display: flex;
    margin-bottom: 1rem;
    flex-wrap: nowrap;
  }

  .media-object-section {
    flex: 0 1 auto;

    &:first-child {
      padding-right: 1rem;
    }

    > {
      &:last-child {
        margin-bottom: 0;
      }
    }

    &.media-card__icon {
      &:before {
        content: '';
        display: block;
        width: 1rem;
        height: 1.15rem;
        margin-top: 0.25rem;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOGJjNTNlIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
        background-repeat: no-repeat;
      }
    }

    .media-card__description {
      display: inline;
    }
  }

  .quote-form-container {
    text-align: center;
    overflow: auto;
    margin-top: -17.5rem;

    .quote-form {
      border: 1px solid #D1D1D1;
      border-radius: 5px;
      background-color: white;

      h3 {
        width: 100%;
        margin: 2rem 0 1.5rem 0;
      }

      input[type="text"], input[type="tel"], input[type="email"], input[type="date"], input[type="time"], select {
        display: block;
        box-sizing: border-box;
        width: 100%;
        height: 2.4375rem;
        border: 1px solid #D1D1D1;
        border-radius: 5px;
        margin-bottom: .5rem;
        padding-left: 1rem;
        background-color: #ffffff;
        box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
        font-family: inherit;
        font-size: 1rem;
        font-weight: normal;
        line-height: 1.5;
        color: #434343;
        transition: box-shadow 0.5s, border-color 0.25s ease-in-out;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;

        &:focus {
          outline: none;
          background-color: #ffffff;
          box-shadow: 0 0 5px #cacaca;
          transition: box-shadow 0.5s, border-color 0.25s ease-in-out;
        }
      }

      input[type="date"], input[type="time"] {
        padding: .69em .75em .69em .75em;
        cursor: pointer;
        outline: 0;
      }

      .select-wrapper {
        position: relative;

        &:after {
          content: '';
          cursor: pointer;
          position: absolute;
          right: 0.75em;
          top: .625em;
          width: .65rem;
          height: 1.5rem;
          background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMjAgNTEyIj48cGF0aCBmaWxsPSIjNDM0MzQzIiBkPSJNMTQzIDM1Mi4zTDcgMjE2LjNjLTkuNC05LjQtOS40LTI0LjYgMC0zMy45bDIyLjYtMjIuNmM5LjQtOS40IDI0LjYtOS40IDMzLjkgMGw5Ni40IDk2LjQgOTYuNC05Ni40YzkuNC05LjQgMjQuNi05LjQgMzMuOSAwbDIyLjYgMjIuNmM5LjQgOS40IDkuNCAyNC42IDAgMzMuOWwtMTM2IDEzNmMtOS4yIDkuNC0yNC40IDkuNC0zMy44IDB6Ii8+PC9zdmc+);
          background-repeat: no-repeat;
          pointer-events: none;
          
          
        }
      }

      .dropdown-date-selector {
        input {
          cursor: pointer;
        }
      }

      input.user-invalid {
        border: 1px solid #FF5C5C;
        box-shadow: inset 1px 1px #FF5C5C, inset -1px -1px #FF5C5C;
        transition: all 0.1s ease-in-out;
      }

      h4 {
        margin: 1.5rem 0 .5rem 0;
        text-align: center;
      }

      .quote-form__details {
        margin-bottom: .5rem;

        .decrement, .description, .increment {
          border: 1px solid #D1D1D1;
          user-select: none;
          color: #434343;
          padding: 0.625rem;
          outline: none;
        }

        .decrement, .increment {
          color: #A2A2A2;
          cursor: pointer;
          font-weight: 900;

          &:hover {
            background-color: #F0F2F4;
          }
        }

        .decrement {
          border-right-style: none;
          border-top-left-radius: 5px;
          border-bottom-left-radius: 5px;
        }

        .increment {
          border-left-style: none;
          border-top-right-radius: 5px;
          border-bottom-right-radius: 5px;
        }

        .description {
          width: calc(100% - 80px);
          padding: 3.52%;
        }
      }

      .quote-form__questions {
        margin-bottom: 0.3125rem;
      }

      h5 {
        margin: 1rem 0 .5rem 0;
      }

      .quote-form__button {
        margin-top: 1rem;

        .btn {
          font-size: 1.25rem;
          width: 100%;
          color: white;
          border-radius: 0.1875rem;
          margin: 0 0 1rem 0;
          padding: 0.85em 1em;
          border: 1px solid transparent;
          transition: background-color 0.25s ease-out, color 0.25s ease-out;
          line-height: 1;
        }
      }
    }

    .when-page-errors {
      display: none;
      color: #FF5C5C;
      padding-bottom: 1rem;

      &.-visible {
        display: block;
      }

      &:before {
        content: '';
        display: inline-block;
        width: 0.8rem;
        height: 0.7rem;
        margin-right: 0.25em;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1NzYgNTEyIj48cGF0aCBmaWxsPSIjRkY1QzVDIiBkPSJNNTY5LjUxNyA0NDAuMDEzQzU4Ny45NzUgNDcyLjAwNyA1NjQuODA2IDUxMiA1MjcuOTQgNTEySDQ4LjA1NGMtMzYuOTM3IDAtNTkuOTk5LTQwLjA1NS00MS41NzctNzEuOTg3TDI0Ni40MjMgMjMuOTg1YzE4LjQ2Ny0zMi4wMDkgNjQuNzItMzEuOTUxIDgzLjE1NCAwbDIzOS45NCA0MTYuMDI4ek0yODggMzU0Yy0yNS40MDUgMC00NiAyMC41OTUtNDYgNDZzMjAuNTk1IDQ2IDQ2IDQ2IDQ2LTIwLjU5NSA0Ni00Ni0yMC41OTUtNDYtNDYtNDZ6bS00My42NzMtMTY1LjM0Nmw3LjQxOCAxMzZjLjM0NyA2LjM2NCA1LjYwOSAxMS4zNDYgMTEuOTgyIDExLjM0Nmg0OC41NDZjNi4zNzMgMCAxMS42MzUtNC45ODIgMTEuOTgyLTExLjM0Nmw3LjQxOC0xMzZjLjM3NS02Ljg3NC01LjA5OC0xMi42NTQtMTEuOTgyLTEyLjY1NGgtNjMuMzgzYy02Ljg4NCAwLTEyLjM1NiA1Ljc4LTExLjk4MSAxMi42NTR6Ij48L3BhdGg+PC9zdmc+);
      }
    }

    .when-page-success {
      display: none;
      color: #8CC63F;
      padding-bottom: 1rem;

      &.-visible {
        display: block;
      }

      &:before {
        content: '';
        display: inline-block;
        width: 0.7rem;
        height: 0.8rem;
        margin-right: 0.25em;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOENDNjNGIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
      }
    }
  }

  .quote-form-description {
    color: #9CABB5;
    text-align: center;
    font-size: 0.75rem;
    font-weight: 300;
    line-height: 0.9375rem;
    padding: 1rem 1.4rem 1rem 1.4rem;
  }

  .CalendarMonth_caption {
    background-color: #0bb8e3;

    > * {
      font-weight: 400;
    }
  }

  .CalendarDay_button {
    outline: none;
  }

  .CalendarDay__selected {
    border: 0;
    background-color: #8cc63f;
  }

  .DayPickerKeyboardShortcuts_show {
    border: 2px solid #8cc63f;
    color: #8cc63f;
    outline: none;
  }

  .DayPickerKeyboardShortcuts_showSpan {
    display: block;
    margin-top: -2px;
    color: #8cc63f;
  }

  .get-started {
    min-width: 100px;
    max-width: 300px;

    .btn {
      margin-bottom: 0;
      padding: 0.85em 0.85em;
      font-size: 1.25rem;
      line-height: 1;
      width: 100%;
      color: white;
      border-radius: 3px;
    }
  }

  .page_text__content {
    margin-bottom: 2rem;

    ul {
      list-style: none;
      margin-bottom: 1rem;

      li {
        &:before {
          margin-right: 10px;
          content: '';
          display: inline-block;
          width: 1rem;
          height: 1rem;
          background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOGJjNTNlIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
          -webkit-transition: .5s;
          transition: .5s;
        }
      }
    }
  }

  @media only screen and (max-width: 85.3125em) { //1365px
    .quote-bg {
      min-height: 16em;
    }
  }

  @media only screen and (max-width: 48.0625em) {
    .head-container {
      h1 {
        text-align: center;
        font-size: 1.75rem;
      }

      .head {
        flex-flow: column-reverse;
        justify-content: center;
      }
    }

    .quote-form-container {
      margin-top: -18.4rem;
      margin-bottom: 4rem;
    }

    p {
      font-size: 16px;
    }
  }

  @media only screen and (min-width: 64.0625em) { //1024px
    & > {
      .row {
        max-width: 1440px;
      }
    }

    .backgrounds-bedroom-webp {
      background-image: url(/static/images/bedroom.webp);
    }

    .backgrounds-bathroom-webp {
      background-image: url(/static/images/bathroom.webp);
    }

    .backgrounds-kitchen-webp {
      background-image: url(/static/images/kitchen.webp);
    }

    .backgrounds-extras-webp {
      background-image: url(/static/images/extras.webp);
    }

  }
`;
