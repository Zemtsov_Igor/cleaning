import styled from 'styled-components';

export const OtherPagesComponent:any = styled('div')`
  position: relative;
  *zoom: 1;
  z-index: 1;
  width: 100%;

  &:before, &r:after {
    content: " ";
    display: table;
  }

  p {
    font-size: 1.1rem;
    margin-bottom: 1em;
  }

  .quote-bg {
    margin-bottom: 2rem;

    .service-image {
      height: 280px;
      background-position: 0 100%;
      background-size: cover;
      background-repeat: no-repeat;
    }
  }

  .head-container {
    margin-bottom: 4rem;

    h1 {
      font-weight: bold;
      margin-bottom: .5rem;
      font-size: 2.5rem;
      text-align: left;
    }

    .rating {
      margin-bottom: 3rem;

      .stars {
        padding-right: 1rem;
      }

      .rating-stars {
        .star {
          position: relative;
          display: inline-block;
          height: 1em;
          width: 1em;
          margin-right: 4px;

          i {
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            vertical-align: top;
            overflow: hidden;
            color: #FFB600;
          }
        }

        .fa-star {
          &:before {
            content: "\f005";
          }
        }
      }
      
      .review_count {
        background: 0;
        border: 0;
        border-bottom: 0.5px solid #434343;
        color: #434343;
        font-size: 1em;
        cursor: pointer;
      }
    }
  }

  .how-it-works__media-cards-container {
    .img {
      width: 88px;
      height: 88px;
      display: inline-block;
      vertical-align: middle;
      max-width: 100%;

      img {
        width: 88px;
        height: 88px;
      }
    }

    h3 {
      margin-bottom: 0.3125rem;
    }

    p {
      font-size: 1rem;
    }
  }

  .media-object {
    display: flex;
    margin-bottom: 1rem;
    flex-wrap: nowrap;
  }

  .media-object-section {
    flex: 0 1 auto;

    &:first-child {
      padding-right: 1rem;
    }

    > {
      &:last-child {
        margin-bottom: 0;
      }
    }

    &.media-card__icon {
      &:before {
        content: '';
        display: block;
        width: 1rem;
        height: 1.15rem;
        margin-top: 0.25rem;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOGJjNTNlIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
        background-repeat: no-repeat;
      }
    }

    .media-card__description {
      display: inline;
    }
  }

  .media-card {
    margin-bottom: 3rem;

    .media-card__logo {
      padding-right: 1.5rem;
    }
  }

  .quote-form-container {
    text-align: center;
    overflow: auto;
    margin-top: -17.5rem;

    .quote-form {
      border: 1px solid #D1D1D1;
      border-radius: 5px;
      background-color: white;

      h3 {
        width: 100%;
        margin: 2rem 0 1.5rem 0;
      }

      input[type="text"], input[type="tel"], input[type="email"], input[type="time"], select {
        display: block;
        box-sizing: border-box;
        width: 100%;
        height: 2.4375rem;
        border: 1px solid #D1D1D1;
        border-radius: 5px;
        margin-bottom: .5rem;
        padding-left: 1rem;
        background-color: #ffffff;
        box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
        font-family: inherit;
        font-size: 1rem;
        font-weight: normal;
        line-height: 1.5;
        color: #434343;
        transition: box-shadow 0.5s, border-color 0.25s ease-in-out;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;

        &:focus {
          outline: none;
          background-color: #ffffff;
          box-shadow: 0 0 5px #cacaca;
          transition: box-shadow 0.5s, border-color 0.25s ease-in-out;
        }
      }

      input[type="time"] {
        padding: .69em .75em .69em .75em;
        cursor: pointer;
        outline: 0;
      }

      .select-wrapper {
        position: relative;

        &:after {
          content: "\f107";
          cursor: pointer;
          font-family: "Font Awesome 5 Pro";
          position: absolute;
          right: 0.75em;
          top: .625em;
          pointer-events: none;
        }
      }

      .dropdown-date-selector {
        input {
          cursor: pointer;
        }
      }

      input.user-invalid {
        border: 1px solid #FF5C5C;
        box-shadow: inset 1px 1px #FF5C5C, inset -1px -1px #FF5C5C;
        transition: all 0.1s ease-in-out;
      }

      h4 {
        margin: 1.5rem 0 .5rem 0;
        text-align: center;
      }

      .quote-form__details {
        margin-bottom: .5rem;

        .decrement, .description, .increment {
          border: 1px solid #D1D1D1;
          user-select: none;
          color: #434343;
          padding: 0.625rem;
          outline: none;
        }

        .decrement, .increment {
          color: #A2A2A2;
          cursor: pointer;
          font-weight: 900;

          &:hover {
            background-color: #F0F2F4;
          }
        }

        .decrement {
          border-right-style: none;
          border-top-left-radius: 5px;
          border-bottom-left-radius: 5px;
        }

        .increment {
          border-left-style: none;
          border-top-right-radius: 5px;
          border-bottom-right-radius: 5px;
        }

        .description {
          width: calc(100% - 80px);
          padding: 3.52%;
        }
      }

      .quote-form__questions {
        margin-bottom: 0.3125rem;
      }

      h5 {
        margin: 1rem 0 .5rem 0;
      }

      .quote-form__button {
        margin-top: 1rem;

        .btn {
          font-size: 1.25rem;
          width: 100%;
          color: white;
          border-radius: 0.1875rem;
          margin: 0 0 1rem 0;
          padding: 0.85em 1em;
          border: 1px solid transparent;
          transition: background-color 0.25s ease-out, color 0.25s ease-out;
          line-height: 1;
        }
      }
    }

    .error {
      display: none;
      color: #FF5C5C;
      padding-bottom: 1rem;

      &.-visible {
        display: block;
      }

      .fa-warning {
        margin-right: 0.25em;

        &:before {
          content: '\f071';
        }
      }
    }

    .success {
      display: none;
      color: #8CC63F;
      padding-bottom: 1rem;

      &.-visible {
        display: block;
      }

      .fa-check {
        margin-right: 0.25em;

        &:before {
          content: '\f00c';
        }
      }
    }
  }

  .quote-form-description {
    color: #9CABB5;
    text-align: center;
    font-size: 0.75rem;
    font-weight: 300;
    line-height: 0.9375rem;
    padding: 1rem 1.4rem 1rem 1.4rem;
  }

  .CalendarMonth_caption {
    background-color: #0bb8e3;

    > * {
      font-weight: 400;
    }
  }

  .CalendarDay_button {
    outline: none;
  }

  .CalendarDay__selected {
    border: 0;
    background-color: #8cc63f;
  }

  .DayPickerKeyboardShortcuts_show {
    border: 2px solid #8cc63f;
    color: #8cc63f;
    outline: none;
  }

  .DayPickerKeyboardShortcuts_showSpan {
    display: block;
    margin-top: -2px;
    color: #8cc63f;
  }

  .get-started {
    min-width: 100px;
    max-width: 300px;

    .btn {
      margin-bottom: 0;
      padding: 0.85em 0.85em;
      font-size: 1.25rem;
      line-height: 1;
      width: 100%;
      color: white;
      border-radius: 3px;
    }
  }

  .page_text__content {
    margin-bottom: 2rem;

    h2 {
      margin-bottom: 1rem;
    }
  }

  .other_services {
    margin-bottom: 5rem;

    .cards {
      padding: 3rem;
      color: #434343;
      background-color: #F9F9F9;

      .cards__title {
        margin-bottom: 1rem;
      }

      ul {
        list-style-type: none;
        margin-left: 0;

        p {
          font-size: 1em;
          margin-bottom: 0;
        }

        a {
          color: #434343;
        }
      }
    }

    .cards__link {
      margin-top: 1rem;
    }
  }

  @media only screen and (max-width: 85.3125em) { //1365px
    .quote-bg {
      min-height: 16em;
    }
  }

  @media only screen and (max-width: 48.0625em) {
    .head-container {
      h1 {
        text-align: center;
        font-size: 1.75rem;
      }

      .head {
        flex-flow: column-reverse;
        justify-content: center;
      }
    }

    .rating {
      .cell {
        justify-content: center;
      }
    }

    .media-card {
      flex-wrap: wrap;
      text-align: center;

      .media-card__logo {
        margin-bottom: 1.5rem;
        padding-right: 0;
      }
    }

    .media-object-section {
      padding: 0;
      flex-basis: 100%;
      max-width: 100%;
    }

    .quote-form-container {
      margin-top: -18.4rem;
      margin-bottom: 4rem;
    }

    p {
      font-size: 16px;
    }
  }

  @media only screen and (min-width: 64.0625em) { //1024px
    & > {
      .row {
        max-width: 1440px;
      }
    }

    .backgrounds-bedroom-webp {
      background-image: url(/static/images/bedroom.webp);
    }

    .backgrounds-bathroom-webp {
      background-image: url(/static/images/bathroom.webp);
    }

    .backgrounds-kitchen-webp {
      background-image: url(/static/images/kitchen.webp);
    }

    .backgrounds-extras-webp {
      background-image: url(/static/images/extras.webp);
    }

  }
`;
