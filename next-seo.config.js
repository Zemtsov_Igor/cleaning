export default {
  openGraph: {
    type: 'website',
    locale: 'ru_RU',
    site_name: 'Клининг',
    url: '/',
  },
  title: 'Клининг Заголовок',
  description: 'Клининг Описание',
  // twitter: {
  //   handle: '@handle',
  //   site: '@site',
  //   cardType: 'summary_large_image'
  // }
};
