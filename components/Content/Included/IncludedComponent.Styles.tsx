import styled from 'styled-components';

export const IncludedComponent:any = styled('section')`
  margin-bottom: 5rem;
  padding: 5rem 0;

  &.with_btn {
    margin-bottom: 0;
    padding-bottom: 0;
  }

  .service-description__title {
    margin-bottom: 2rem;

    h3 {
      font-size: 1.625em;
      color: #434343;
      line-height: 1.25em;
      font-weight: 400;
    }

    h3, h4 {
      text-align: center;
    }
  }

  .section {
    margin-bottom: 2rem;
    justify-content: center;

    .img {
      width: 604px;
      height: 320px;
    }

    .section__description {
      border: 1px solid #EAEAEA;
      box-shadow: 0 0 8px 0 rgba(0,0,0,0.05);
      padding: 2rem;
      background-color: white;
      margin-top: -6.25rem;
      padding: 1.5rem;
      z-index: 10;

      h3 {
        text-align: left;
      }

      &.only-title {
        h3 {
          margin-bottom: 0;
        }
      }

      p {
        margin-bottom: 0;
        font-size: inherit;
        line-height: 1.6;
        text-rendering: optimizeLegibility;
      }

      ul {
        font-size: 1em;
        margin: 0.2em;
        padding-left: 1em;

        li {
          list-style-type: disc;
        }
      }
    }
  }

  @media only screen and (max-width: 40.0625em) {
    .section {
      &-description__title {
        h3 {
          font-size: 2rem;
        }
      }

      .img {
        height: 200px;
      }

      .section__description {
        h3 {
          text-align: center;
        }
      }
    }
  }

  @media only screen and (min-width: 48.0625em) { //768px
    .section {
      .section__description {
        margin-top: -6.25rem;
      }
    }
  }

  @media only screen and (min-width: 64.0625em) { //1024px
    .service-description__title {
      margin-bottom: 3rem;

      h3 {
        font-size: 2em;
        line-height: 1.25em;
      }
    }

    .section {
      align-items: center;
      justify-content: initial;

      .section__description {
        overflow: hidden;
        margin-top: 0;
        margin-left: -20%;
        margin-right: 10%;
        padding: 1.875rem;
      }

      &:nth-of-type(odd) {
        flex-flow: row-reverse;

        .section__description {
          margin-right: -20%;
          margin-left: 10%;
        }
      }
    }
  }
`;