import { IncludedComponent } from './IncludedComponent.Styles';

export default function Included({ data }:any) {

  return (
    <IncludedComponent className={`service-description ${data.className}`}>
      <div className="grid-x align-center">
        <div className="cell small-10 text-center service-description__title">
          {
            data.title && <h3>{data.title}</h3>
          }
          {
            data.description && <h4>{data.description}</h4>
          }
        </div>
        {
          data.items.map((item:any) => (
            <div
              key={Buffer.from(Math.random().toString()).toString('base64')}
              className="cell grid-x section"
            >
              <div className="cell small-shrink medium-shrink large-shrink section__image">
                <img
                  src={item.icon.src}
                  className="img"
                  alt={item.icon.alt}
                />
              </div>
              <div className={`cell small-11 large-auto section__description ${(!item.description && !item.list.length) && 'only-title'}`}>
                {
                  (!!item.title) && (
                    <h3>{item.title}</h3>
                  )
                }
                {
                  (!!item.description) && (
                    <p dangerouslySetInnerHTML={{ __html: item.description }} />
                  )
                }
                {
                  (!!item.list.length) && (
                    <ul>
                      {
                        item.list.map((listItem:any) => (
                          <li dangerouslySetInnerHTML={{ __html: listItem }} />
                        ))
                      }
                    </ul>
                  )
                }
              </div>
            </div>
          ))
        }
      </div>
    </IncludedComponent>
  );
};

const getTurboIncludedListItems = (items:any) => {
  let turboIncludedListItems:string = '';
  items.forEach((item:any) => {
    turboIncludedListItems = turboIncludedListItems + `
      <li>${item}</li>
    `
  });

  return turboIncludedListItems;
};

const getTurboIncludedItems = (items:any) => {
  let turboIncludedItems:string = '';
  items.forEach((item:any) => {
    turboIncludedItems = turboIncludedItems + `
      <div class="cell grid-x section">
        <div class="cell small-shrink medium-shrink large-shrink section__image">
          <img src="https://my-cleaning.ru${item.icon.src}" alt="${item.icon.alt}" class="img" />
        </div>
        <div class="cell small-11 large-auto section__description ${(!item.description && !item.list.length) ? 'only-title' : ''}">
          ${
            (!!item.title) ? `
              <h3>${item.title}</h3>
            ` : ''
          }
          ${
            (!!item.description) ? `
              <p>${item.description}</p>
            ` : ''
          }
          ${
            (!!item.list.length) ? `
              <ul>
                ${getTurboIncludedListItems(item.list)}
              </ul>
            ` : ''
          }
        </div>
      </div>
    `
  });

  return turboIncludedItems;
};

export const turboIncludeds:any = (data:any) => {
  return `
    <section class="section service-description ${data.className ? data.className : ''}">
      <div class="grid-x align-center">
        <div class="cell small-10 text-center service-description__title">
          <h3>${data.title}</h3>
          <h4>${data.description}</h4>
        </div>
        ${getTurboIncludedItems(data.items)}
      </div>
    </section>
  `;
}
