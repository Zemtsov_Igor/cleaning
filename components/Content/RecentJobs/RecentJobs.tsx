import React, { ReactElement } from 'react';
import * as Amp from 'react-amphtml';

import { RecentJobsComponent } from './RecentJobsComponent.Styles';

export default function RecentJobs({ data }:any) {

  return (
    <RecentJobsComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center">
          <div className="grid-x align-center small-12 medium-10">
            <div className="cell sample-projects__header row">
              <h2>{data.title}</h2>
            </div>
            <div className="cell sample-projects__container">
              <Amp.Button specName="default" on="tap:recentJob.prev()" className="card-prev"><i className="fal fa-chevron-left"></i></Amp.Button>

                <Amp.AmpBaseCarousel
                  specName="default"
                  className="card-track"
                  id="recentJob"
                  layout="responsive"
                  width="2"
                  height="0"
                  visible-count="(min-width: 80em) 3, (min-width: 48.0625em) 2, (min-width: 40.0625em) 1, 1"
                  loop="true"
                >
                  {
                    data.items.map(
                      (item:any): ReactElement => (
                        <div className="card-slide" key={Buffer.from(Math.random().toString()).toString('base64')}>
                          <div className="card-wrapper">
                            <div className="card">
                              <div className="icon">
                                <Amp.AmpImg
                                  specName="default"
                                  key={Buffer.from(Math.random().toString()).toString('base64')}
                                  src="/static/images/cleaner_filled_icon.png"
                                  className="img"
                                  layout="flex-item"
                                  alt="Handy plan"
                                />
                              </div>
                              <div className="card__title">{item.title}</div>
                              <div className="card__fields row">
                                <div className="item column">
                                  <div className="key">Дата начала</div>
                                  <div className="value">{item.dateStart}</div>
                                </div>
                                <div className="item column">
                                  <div className="key">Базовые часы</div>
                                  <div className="value">{item.baseHours}</div>
                                </div>
                              </div>
                              <div className="card__fields row">
                                <div className="item column">
                                  <div className="key">Создано в</div>
                                  <div className="value">{item.createdAt}</div>
                                </div>
                                <div className="item column">
                                  <div className="key">Тип</div>
                                  <div className="value">{item.bedrooms}</div>
                                </div>
                              </div>
                              <div className="card__description">
                                <div className="item">
                                  <div className="key">Описание</div>
                                  <div className="value">{item.description}</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ),
                    )
                  }
                  <button slot="next-arrow" className="carousel-btn" aria-label="Next"></button>
                  <button slot="prev-arrow" className="carousel-btn" aria-label="Previous" ></button>
                </Amp.AmpBaseCarousel>
              <Amp.Button specName="default" on="tap:recentJob.next()" className="card-next"><i className="fal fa-chevron-right"></i></Amp.Button>
            </div>
          </div>
        </div>
      </div>
    </RecentJobsComponent>
  );
};