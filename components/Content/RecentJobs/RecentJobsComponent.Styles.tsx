import styled from 'styled-components';

export const RecentJobsComponent:any = styled('section')`
  .sample-projects__header {
    padding: 0 0;

    h2 {
      float: none;
    }
  }

  .sample-projects__container {
    margin-top: 45px;
    margin-bottom: 65px;
    display: block;
    position: relative;
    margin: auto;
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-touch-callout: none;
    -khtml-user-select: none;
    touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;

    .carousel-btn {
      display: none;
    }

    .card-prev, .card-next {
      display: flex;
      align-items: center;
      justify-content: center;
      position: absolute;
      top: 50%;
      width: 40px;
      height: 40px;
      cursor: pointer;
      font-size: 28px;
      color: #9CABB5;
      border: 0;
      background: 0;
      outline: none;
    }

    .card-prev {
      left: -55px;

      .fa-chevron-left {
        &:before {
          content: "\f053";
        }
      }
    }
    .card-next {
      right: -38px;

      .fa-chevron-right {
        &:before {
          content: "\f054";
        }
      }
    }

    .card-slide {
      display: block;
      float: left;
      height: 100%;
      min-height: 1px;
      padding-top: 40px;
    }

    .card-wrapper {
      outline: none;
      width: 100%;
      display: inline-block;
    }

    .card-track {
      > div {
        position: relative;
      }
    }

    .card {
      border: 1px solid #EEEEEE;
      border-radius: 3.5px;
      background: #FFFFFF;
      color: #434343;
      margin: 0 20px 0 0;
      padding: 41px 24px 16px;
      position: relative;

      .icon {
        box-sizing: border-box;
        height: 50px;
        width: 50px;
        position: absolute;
        top: -25px;
        left: 42%;
      }

      .card__title {
        font-size: 16px;
        text-align: center;
        margin-bottom: 8px;
      }

      .card__fields {
        font-size: 12px;
        display: flex;
        flex-wrap: wrap;
        margin-right: -24px;

        .item {
          padding: 0;
          margin-bottom: 8px;
          width: 111px;
          margin-right: 20px;

          .key {
            margin-bottom: 4px;
            color: #9CABB5;
          }
        }
      }

      .card__description {
        margin-top: 16px;
        font-size: 12px;

        .item {
          margin-bottom: 0;
          width: auto;
          max-height: 140px;
          overflow: hidden;

          .key {
            margin-bottom: 4px;
            color: #9CABB5;
          }
        }
      }
    }
  }

  @media only screen and (max-width: 40.0625em) { //640px
    .sample-projects__container {
      .card {
        margin-left: 20px;
      }
      .card-prev, .card-next {
        display: none;
      }
    }
  }
`;