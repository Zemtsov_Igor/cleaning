import styled from 'styled-components';

export const WantToWorkComponent:any = styled('section')`
  margin-bottom: 5rem;
  background-color: #EEF6F9;

  .want-to-work__content {
    padding-top: 1rem;
    align-content: center;

    h2 {
      margin-bottom: 1rem;
    }

    .btn {
      min-width: 300px;
    }
  }

  .want-to-work__image {
    padding-top: 1rem;
    align-items: flex-end;
    justify-content: flex-end;
    text-align: end;

    .pro {
      max-width: 50%;

      .img {
        width: 200px;
        height: 380px;
      }
    }
  }
`;