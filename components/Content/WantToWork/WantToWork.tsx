import * as Amp from 'react-amphtml';

import GetStarted from '../../../components/Content/GetStarted/GetStarted';

import { WantToWorkComponent } from './WantToWorkComponent.Styles';

export default function WantToWork({ data }:any) {

  return (
    <WantToWorkComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center want-to-work">
          <div className="cell grid-x small-12">
            <div className="cell grid-x medium-6 want-to-work__content">
              <div className="cell grid-x content-title">
                <h2>{data.title}</h2>
              </div>

              <div className="cell grid-x content-description">
                <p dangerouslySetInnerHTML={{ __html: data.description }} />
              </div>

              <GetStarted data={{ btnAction: 'tap:top.scrollTo(duration=500)', btnTitle: data.btn, className:'', align:'left' }} />

            </div>
            <div className="cell grid-x medium-6 align-middle hide-for-small-only want-to-work__image">
              <div className="pro">
                <Amp.AmpImg
                  specName="default"
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  src="/static/images/handy_man.png"
                  className="img"
                  layout="flex-item"
                  alt="Горничный в Москве"
                />
              </div>
              <div className="pro">
                <Amp.AmpImg
                  specName="default"
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  src="/static/images/handy_woman.png"
                  className="img"
                  layout="flex-item"
                  alt="Горничная в Москве"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </WantToWorkComponent>
  );
};