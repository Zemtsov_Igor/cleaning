import { ReliableComponent } from './ReliableComponent.Styles';

export default function Reliable({ data }:any) {

  return (
    <ReliableComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="align-center trusted-wrap">
          <div className="grid-x align-center trusted-container">
            <div className="cell text-center small-12 large-10 trusted-name">
              <h2>{data.title}</h2>
              <h3 className="section-sub-header">{data.subTitle}</h3>
            </div>
            <div className="cell grid-x grid-margin-x grid-padding-y small-12 trusted-capabilities">
              {
                data.items.map((item:any) => (
                  <div
                    key={Buffer.from(Math.random().toString()).toString('base64')}
                    className="cell capability small-12 medium-6"
                  >
                    <div className="capability__title">
                      <span className="icon">
                        <img
                          src={item.icon.src}
                          className="img"
                          alt={item.icon.alt}
                        />
                      </span>
                      <span className="text">{item.title}</span>
                    </div>

                    <div className="capability__description">
                      <p dangerouslySetInnerHTML={{ __html: item.text }} />
                    </div>

                  </div>
                ))
              }
            </div>
          </div>
        </div>
      </div>
    </ReliableComponent>
  );
};
