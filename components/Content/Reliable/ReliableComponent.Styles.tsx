import styled from 'styled-components';

export const ReliableComponent:any = styled('section')`
  margin-bottom: 5rem;

  &.with_btn {
    margin-bottom: 0;
    padding-bottom: 0;
  }

  .trusted-name {
    margin-bottom: 2rem;

    h2, h3 {
      text-align: center;
    }
  }

  .trusted-capabilities {
    height: 100%;

    .capability {
      margin-bottom: 2em;

      .capability__title {
        display: flex;
        font-size: 1.25rem;
        margin-bottom: 0.5rem;

        .icon {
          display: inline-block;
          margin-right: 1rem;

          .img {
            width: 40px;
            height: 40px;

            img {
              display: inline-block;
              vertical-align: middle;
              max-width: 100%;
              bottom: auto;
            }
          }
        }
      }
    }
  }

  &.with_btn {
    .trusted-capabilities {
      padding-bottom: 0;
    }
  }

  &.even-section {
    padding: 2em 0;
    margin-bottom: 3rem;

    .grid-container {
      .trusted-container {
        width: 91.66667%;
        margin: 0 auto;
        background-color: #FFFFFF;
        border: 1.4px solid #EEEEEE;
        box-shadow: 0 2px 4px 0 #EAEAEA;

        > {
          .small-12 {
            width: 91.66667%;
          }
        }
      }
    }

    .trusted-name {
      padding: 3em 0;
    }
  }

  @media only screen and (min-width: 48.0625em) { //768px
    .capability__description {
      margin-left: 3.625rem;
    }
  }

  @media only screen and (min-width: 64.0625em) { //1024px
    
  }
`;