import React from "react";

import { RightFormComponent } from './RightFormComponent.Styles';
import RatingStars from "../../RatingStars/RatingStars";
import {papulate} from "../../../../utils/papulate";
import {apartmentTypes, apartmentTypesStr} from "../../../../constants/types.constants";

export default function RightForm({ data }:any) {
  const date = new Date().toISOString().slice(0, 10);
  return (
    <RightFormComponent className="head-container">
      <div className="grid-container">
        <div className="grid-x align-center head">
          <div className="grid-x head">
            <div className="cell small-12 large-7">
              <div className="cell head__title">
                <h1 itemProp="name">{data.settings.name}</h1>
              </div>
              <meta itemProp="sku" content={data.settings.sku} />
              <meta itemProp="mpn" content={data.settings.mpn} />
              <div itemProp="brand" itemScope itemType="https://schema.org/Brand">
                <meta itemProp="name" content={data.settings.brand} />
              </div>

              {
                data.reviews && (
                  <div className="rating">
                    <div className="cell grid-x">
                      <div className="cell small-6 medium-shrink stars">
                        <RatingStars data={{rating: data.settings.rating, itemProp: 'aggregateRating', itemType: 'https://schema.org/AggregateRating', ratingType: 'reviewCount'}} />
                      </div>
                      <div className="cell small-6 medium-auto reviews">
                        <span
                          role="button"
                          tabIndex={0}
                          className="review_count anchor-link"
                          data-anchor="#reviews"
                        >
                          {data.reviews.pagination.totalCount} { papulate(data.reviews.pagination.totalCount, ['отзыв', 'отзыва', 'отзывов']) }
                        </span>
                      </div>
                    </div>
                  </div>
                )
              }

              <div itemProp="description" className="cell head__description">
                {
                  data.settings.form.description.text.map((item: string) => (
                    <p>{item}</p>
                  ))
                }

                { data.settings.form.description.title ? <h2>{data.settings.form.description.title}</h2> : null}

                {
                  data.settings.form.description.subText.map((item: string) => (
                    <p>{item}</p>
                  ))
                }
              </div>
              {
                data.settings.form.capabilities.length ? (
                  <div className="cell head__capabilities">
                    {
                      data.settings.form.capabilities.map((item: string) => (
                        <div className="media-object media-card">
                          <div className="media-object-section media-card__icon" />
                          <div className="media-object-section">
                            <p
                              itemProp="description"
                              className="media-card__description"
                            >{item}</p>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                ) : null
              }
            </div>
            <div className="cell small-12 large-offset-1 large-4">
              <div className="cell grid-x">
                <div className="quote-form-container">
                  <div>
                    <form
                      className="quote-form requies-form"
                      acceptCharset="UTF-8"
                      id="headerForm"
                    >
                      <div className="grid-container">
                        <div className="grid-x grid-padding-x">
                          <h3>{data.settings.form.title}</h3>
                          <div className="cell">

                            <div className="small-12 columns error when-page-errors">
                              При отправки вашей заявки произошла ошибка!
                            </div>

                            <div className="small-12 columns success when-page-success">
                              Ваша заявка успешно отправлена!
                            </div>

                            <div className="cell small-12">
                              <input
                                className="name"
                                placeholder="Имя"
                                type="text"
                                name="name"
                                required
                              />
                            </div>

                            <div className="cell small-12">
                              <input
                                placeholder="Телефон"
                                className="phone"
                                type="tel"
                                name="phone"
                                required
                              />
                            </div>

                            <div className="cell small-12">
                              <input
                                className="email"
                                placeholder="E-mail"
                                type="email"
                                name="email"
                              />
                            </div>

                            {
                              data.settings.form.place ? (
                                <>
                                  <h4>Расскажи нам о своем месте</h4>
                                  <div>
                                    <div className="cell grid-x small-12 form-element-apt quote-form__details">
                                      <span
                                        className="cell small-2 decrement"
                                        role="button"
                                        tabIndex={0}
                                      >−</span>
                                      <span className="cell auto description" data-type={0} data-types={apartmentTypesStr}>{apartmentTypes[0].name}</span>
                                      <span
                                        className="cell small-2 increment"
                                        role="button"
                                        tabIndex={0}
                                      >+</span>
                                    </div>
                                  </div>
                                  <div className="quote-form__questions" />
                                  <div>
                                    <h5>Мы рекомендуем&nbsp;
                                      <span className="description-hours">{apartmentTypes[0].hours}</span>
                                    </h5>
                                    <div className="cell small-12 select-wrapper">
                                      <select name="hours">
                                        {
                                          apartmentTypes.map((type:any) => (
                                            <option key={Buffer.from(Math.random().toString()).toString('base64')} value={type.hours}>{type.hours}</option>
                                          ))
                                        }
                                      </select>
                                    </div>
                                  </div>
                                </>
                              ) : null
                            }

                            <h5>Когда вы хотите, чтобы мы приехали?</h5>
                            <div className="cell small-12">
                              <div className="dropdown-date-selector">
                                <input
                                  type="date"
                                  name="date_start"
                                  defaultValue={date}
                                />
                              </div>
                            </div>
                            <div className="cell small-12">
                              <input
                                type="time"
                                name="time_start"
                                defaultValue="07:00"
                              />
                            </div>
                          </div>
                          <div className="cell small-12 quote-form__button">
                            <button
                              className="btn btn-primary"
                              type="submit"
                            >
                              {data.settings.form.btnText}
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <p className="grid-container quote-form-description">
                      Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </RightFormComponent>
  );
};

const getRightFormDescriptionItems = (items:any) => {
  let turboRightFormDescriptionItems:string = '';
  items.forEach((item:any) => {
    turboRightFormDescriptionItems = turboRightFormDescriptionItems + `
      <p>${item}</p>
    `
  });

  return turboRightFormDescriptionItems;
};

const getRightFormCapabilitiesItems = (items:any) => {
  let turboRightFormCapabilitiesItems:string = '';
  items.forEach((item:any) => {
    turboRightFormCapabilitiesItems = turboRightFormCapabilitiesItems + `
      <div class="media-object media-card">
        <div class="media-object-section media-card__icon"></div>
        <div class="media-object-section">
          <p class="media-card__description">${item}</p>
        </div>
      </div>
    `
  });

  return turboRightFormCapabilitiesItems;
};

export const turboRightForm:any = (data:any) => {
  return `
    <section class="head-container">
      <div class="grid-container">
        <div class="grid-x align-center head">
          <div class="grid-x head">
            <div class="cell small-12 large-7">
              <div class="cell head__title">${data.settings.name}</div>
              <div class="rating">
                <div class="cell grid-x">
                  <div class="cell small-6 medium-shrink stars">
                    <div class="rating-stars">
                      <div itemscope itemtype="http://schema.org/Rating">
                        <meta itemprop="ratingValue" content="${((data.settings.rating / 100) * 5).toFixed(1)}">
                        <meta itemprop="bestRating" content="5">
                      </div>
                    </div>
                  </div>
                  ${
                    data.reviews ? `
                      <div class="cell small-6 medium-auto reviews">
                        <a href="#reviews"
                           class="review_count">${data.reviews.pagination.totalCount} ${papulate(data.reviews.pagination.totalCount, ['отзыв', 'отзыва', 'отзывов'])}</a>
                      </div>
                    ` : ''
                  }
                </div>
              </div>

              <div class="cell head__description">
                ${getRightFormDescriptionItems(data.settings.form.description.text)}
                ${data.settings.form.description.title ? `<h2>${data.settings.form.description.title}</h2>` : ''}
              </div>
              ${data.settings.form.capabilities.length ? `
                <div class="cell head__capabilities">
                  ${getRightFormCapabilitiesItems(data.settings.form.capabilities)}
                </div>
              ` : ''}
            </div>
            <div class="cell small-12 large-offset-1 large-4">
              <div class="cell grid-x">
                <div class="quote-form-container">
                  <div>
                    <div class="quote-form-wrapper">
                      <h3 id="top">${data.settings.form.title}</h3>
                      <form data-type="dynamic-form" end_point="https://my-cleaning.ru/api/yandex_form">
                        <div type="result-block">
                          <span type="text" class="small-12 columns when-page-errors" field="description"></span>
                        </div>
                        <div type="input-block">
                          <span type="input" name="name" label="Имя" input-type="text" placeholder="Имя" required="true"></span>
                          <span type="input" name="phone" label="Телефон" input-type="text" placeholder="+7 (999) 999-99-99" required="true"></span>
                          <span type="select" name="type" label="Тип" value="studija">
                            <span type="option" value="studija" text="Студия"></span>
                            <span type="option" value="1komnata" text="1 комната"></span>
                            <span type="option" value="2komnaty" text="2 комнаты"></span>
                            <span type="option" value="3komnaty" text="3 комнаты"></span>
                            <span type="option" value="4komnaty" text="4 комнаты"></span>
                            <span type="option" value="5komnat" text="5 комнат"></span>
                          </span>
                          <button type="submit" text="${data.settings.form.btnText}"></button>
                        </div>
                      </form>
                    </div>
                    <p class="grid-container quote-form-description">
                      Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  `;
}
