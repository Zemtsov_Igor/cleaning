import React from "react";

import { CenterFormComponent } from './CenterFormComponent.Styles';
import RatingStars from "../../RatingStars/RatingStars";
import {papulate} from "../../../../utils/papulate";

export default function CenterForm({ data }:any) {
  const date = new Date().toISOString().slice(0, 10);
  return (
    <CenterFormComponent className="head-container">
      <div className="quote-request-form">
        <div className="grid-container">
          <div className="grid-x align-center">
            <div className="cell small-12 large-8">
              <div className="cell grid-x">
                <div className="quote-form-container small-12">
                  <form
                    className="simple_form new_quote_request requies-form"
                    acceptCharset="UTF-8"
                    id="headerForm"
                  >
                    <div>
                      <div className="small-12 header">
                        <h1 itemProp="name">{data.settings.name}</h1>
                      </div>

                      <meta itemProp="sku" content={data.settings.sku} />
                      <meta itemProp="mpn" content={data.settings.mpn} />
                      <div itemProp="brand" itemScope itemType="https://schema.org/Brand">
                        <meta itemProp="name" content={data.settings.brand} />
                      </div>
                      {
                        data.reviews && (
                          <div className="rating">
                            <div className="cell grid-x align-center">
                              <div className="cell small-12 medium-shrink stars text-center">
                                <RatingStars data={{rating: data.settings.rating, itemProp: 'aggregateRating', itemType: 'https://schema.org/AggregateRating', ratingType: 'reviewCount'}}/>
                                <span
                                  role="button"
                                  tabIndex={0}
                                  className="review_count anchor-link"
                                  data-anchor="#reviews"
                                >
                                  {data.reviews.pagination.totalCount} { papulate(data.reviews.pagination.totalCount, ['отзыв', 'отзыва', 'отзывов']) }
                                </span>
                              </div>
                            </div>
                          </div>
                        )
                      }

                      <div className="when-section when-page-errors">
                        <div className="checkout-row">
                          <div className="small-12 columns full"> При отправки вашей заявки произошла ошибка!</div>
                        </div>
                      </div>

                      <div className="when-section when-page-success">
                        <div className="checkout-row">
                          <div className="small-12 columns full"> Ваша заявка успешно отправлена!</div>
                        </div>
                      </div>


                    </div>

                    <div className="grid-x">
                      <div className="cell small-12 form-element xlarge-4 ">
                        <input
                          className="when-font name"
                          placeholder="Имя"
                          type="text"
                          name="name"
                          required
                        />
                      </div>
                      <div className="cell small-12 form-element xlarge-4">
                        <input
                          placeholder="Телефон"
                          className="when-font phone"
                          type="tel"
                          name="phone"
                          required
                        />
                      </div>
                      <div className="cell small-12 form-element xlarge-4">
                        <input
                          className="when-font email"
                          placeholder="E-mail"
                          type="email"
                          name="email"
                        />
                      </div>
                    </div>
                    <div className="grid-x">
                      <div className="cell small-12 form-element xlarge-4">
                        <input
                          placeholder="Площадь, кв.м"
                          className="when-font square"
                          type="text"
                          name="square"
                          required
                        />
                      </div>
                      <div className="cell small-12 form-element xlarge-4">
                        <div className="grid-x">
                          <div className="cell small-6">
                            <div className="dropdown-date-selector">
                              <input
                                type="date"
                                name="date_start"
                                defaultValue={date}
                              />
                            </div>
                          </div>
                          <div className="cell small-6">
                            <div className="when-font date-time-input">
                              <input
                                className="adjacent-left"
                                type="time"
                                name="time_start"
                                defaultValue="07:00"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="cell small-12 form-element xlarge-4">
                        <button className="booking-continue-button btn-continue initial-height hide-overflow btn btn-primary" type="submit">{data.settings.form.btnText}</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </CenterFormComponent>
  );
};

const getCenterFormHeadDescription = (items:any) => {
  let turboCenterFormHeadDescriptionItems:string = '';

  items.forEach((item:any) => {
    turboCenterFormHeadDescriptionItems = turboCenterFormHeadDescriptionItems + `
      <p>${item}</p>
    `
  });

  return `<div class="head__description">
    <div class="grid-container">
      <div class="grid-x align-center">
        <div class="cell small-12 small-centered xlarge-8">
          ${turboCenterFormHeadDescriptionItems}
        </div>
      </div>
    </div>
  </div>`;
};

export const turboCenterForm:any = (data:any) => {
  return `
    <section class="head-container head-container-center">
      <div class="quote-request-form">
        <div class="grid-container">
          <div class="grid-x align-center">
            <div class="cell small-12 large-8">
              <div class="cell grid-x">
                <div class="quote-form-container small-12">
                  <div class="small-12 header">
                    <h3 id="top" id="title">${data.settings.name}</h3>
                  </div>
                  
                  
                  <div class="rating">
                    <div class="cell grid-x align-center">
                      <div class="cell small-12 medium-shrink stars text-center">
                        <div class="rating-stars">
                          <div itemscope itemtype="http://schema.org/Rating">
                            <meta itemprop="ratingValue" content="${((data.settings.rating / 100) * 5).toFixed(1)}">
                            <meta itemprop="bestRating" content="5">
                          </div>
                        </div>
                          ${
                            data.reviews ? `
                              <a href="#reviews" class="review_count">${data.reviews.pagination.totalCount} ${ papulate(data.reviews.pagination.totalCount, ['отзыв', 'отзыва', 'отзывов']) }</a>
                            ` : '' 
                          }
                      </div>
                    </div>
                  </div>
                  
                  <form data-type="dynamic-form" end_point="https://my-cleaning.ru/api/yandex_form">
                    <div type="result-block">
                      <span type="text" class="small-12 columns when-page-errors" field="description"></span>
                    </div>
                    <div type="input-block">
                      <span type="input" name="name" label="Имя" input-type="text" placeholder="Имя" required="true"></span>
                      <span type="input" name="phone" label="Телефон" input-type="text" placeholder="+7 (999) 999-99-99" required="true"></span>
                      <span type="input" name="email" label="Email" input-type="text" placeholder="email@email.ru" required="false"></span>
                      <button type="submit" text="${data.settings.form.btnText}"></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="disclaimer">
        <div class="grid-container">
          <div class="grid-x align-center">
            <div class="cell small-12 small-centered xlarge-6">
              Нажимая «Узнать цену», я даю <a href="/privacy" target="_blank">согласие на обработку персональных данных</a> и принимаю <a href="/terms" target="_blank">условия использования сайта</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    ${
      data.settings.form.description.text.length ? getCenterFormHeadDescription(data.settings.form.description.text) : ''
    }
  `;
}
