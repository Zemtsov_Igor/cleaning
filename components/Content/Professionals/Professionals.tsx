import { ProfessionalsComponent } from './ProfessionalsComponent.Styles';

import { papulate } from '../../../utils/papulate';
import RatingStars from "../RatingStars/RatingStars";
import React from "react";

export default function Professionals({ data }:any) {
  return (
    <ProfessionalsComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center top-professionals">
          <div className="cell small-12 medium-8 top-professionals__title">
            <h2>{data.title}</h2>
          </div>
          <div className="cell small-12 medium-8 top-professionals__description">
            <p dangerouslySetInnerHTML={{ __html: data.description }} />
          </div>
          <div className="cell grid-x grid-margin-x grid-margin-y small-12 medium-12 top-professionals__cards-container">
            {
              data.items.map((item:any) => (
                <div
                  className="cell small-12 medium-12 large-4 cards"
                  itemProp="review"
                  itemScope
                  itemType="https://schema.org/Review"
                >
                  <div className="media-object align-middle align-center cards__header">
                    <div className="media-object-section photo">
                      <img
                        src={item.avatar.src}
                        className="img"
                        alt={item.avatar.alt}
                      />
                    </div>
                    <div className="media-object-section text-left">
                      <p className="name">{item.name}</p>
                      <div className="rating">
                        <RatingStars data={{rating: item.rating, itemProp: 'reviewRating', itemType: 'https://schema.org/Rating', ratingType: 'bestRating'}} />
                      </div>
                      {
                        (item.premium) && (
                          <div className="status">
                            <i className="status__icon" />
                            <span className="status__text">Premium</span>
                          </div>
                        )
                      }

                      <div
                        className="jobs-counter"
                        itemProp="author"
                        itemScope
                        itemType="https://schema.org/Person"
                      >
                        <i className="jobs-counter__icon" />
                        <span className="jobs-counter__text">{`${item.jobsCompleted} ${papulate(item.jobsCompleted, ['работа', 'работы', 'работ'])} ${papulate(item.jobsCompleted, ['выполнена', 'выполнены', 'выполнено'])}`}</span>
                        <meta itemProp="name" content={item.name} />
                      </div>
                    </div>
                  </div>
                  {/*<div className="cell cards__body">{item.comment}</div>*/}
                </div>
              ))
            }
          </div>
        </div>
      </div>

    </ProfessionalsComponent>
  );
};

const getTurboProfessionalsItems = (items:any) => {
  let turboProfessionalsItems:string = '';
  items.forEach((item:any) => {
    turboProfessionalsItems = turboProfessionalsItems + `
      <div class="cell small-12 medium-12 large-4 cards">
        <div class="media-object align-middle align-center cards__header">
          <div class="media-object-section photo">
            <img src="${item.avatar.src}" alt="${item.avatar.alt}" class="img" />
          </div>
          <div class="media-object-section text-left">
            <p class="name">${item.name}</p>
            <div class="rating">
              <div class="rating-stars">
                <div itemscope itemtype="http://schema.org/Rating">
                  <meta itemprop="ratingValue" content="${(((item.rating / 100) * 10) >= 10) ? '5' : ('4.' + Math.round((item.rating / 100) * 10))}">
                  <meta itemprop="bestRating" content="5">
                </div>
              </div>
            </div>
            ${
              (item.premium) ? `
                <div class="status">
                  <span class="status__text">Premium</span>
                </div>
              ` : ''
            }
            <div class="jobs-counter">
              <span class="jobs-counter__text">${item.jobsCompleted} ${papulate(item.jobsCompleted, ['работа', 'работы', 'работ'])} ${papulate(item.jobsCompleted, ['выполнена', 'выполнены', 'выполнено'])}</span>
            </div>
          </div>
        </div>
      </div>
    `
  });

  return turboProfessionalsItems;
};

export const turboProfessionals:any = (data:any) => {
  return `
    <section class="section top-professionals__section ${data.className ? data.className : ''}">
      <div class="grid-container">
        <div class="grid-x align-center top-professionals">
          <div class="cell small-12 medium-8 top-professionals__title">
            <h2>${data.title}</h2>
          </div>
          <div class="cell grid-x grid-margin-x grid-margin-y small-12 medium-12 top-professionals__cards-container">
            ${getTurboProfessionalsItems(data.items)}
          </div>
        </div>
      </div>
    </section>
  `;
}
