import styled from 'styled-components';

export const ProfessionalsComponent:any = styled('section')`
  margin-bottom: 5rem;
  padding: 5rem 0;

  .top-professionals {
    text-align: center;

    .top-professionals__title {
      h2 {
        margin-bottom: .5rem;
      }
    }

    .top-professionals__description {
      margin-bottom: 3rem;
    }

    .top-professionals__cards-container {
      .cards {
        background-color: white;
        padding: 1.25rem 1.25rem;
        text-align: center;
        border: 2px solid #EEEEEE;
        box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.05);
        border-radius: 3px;
        font-size: small;

        .cards__header {
          display: flex;
          margin-bottom: 1rem;
          flex-wrap: nowrap;

          .media-object-section {
            flex: 0 1 auto;

            &:first-child {
              padding-right: 1rem;
            }

            > {
              &:last-child {
                margin-bottom: 0;
              }
            }

            .img {
              width: 88px;
              height: 88px;
              border-radius: 50%;
              max-width: 88px;
            }
          }

          .name {
            font-size: initial;
            font-weight: bold;
          }

          .star {
            & .icon:before {
              width: 0.9rem;
              height: 0.8rem;
            }
            
            & .icon-o:before {
              width: 0.9rem;
              height: 0.8rem;
            }
          }

          .status {
            .status__icon {
              content: '';
              display: inline-block;
              width: 14px;
              height: 14px;
              background: url(/static/images/premium_icon.svg) no-repeat center;
              background-size: 14px 14px;
              margin-right: 4px;
            }

            .status__text {
              vertical-align: text-bottom;
            }
          }

          .jobs-counter {
            .jobs-counter__icon {
              content: '';
              display: inline-block;
              width: 14px;
              height: 14px;
              background: url(/static/images/job_star_icon.svg) no-repeat center;
              background-size: 14px 14px;
              margin-right: 4px;
            }

            .jobs-counter__text {
              vertical-align: text-bottom;
            }
          }
        }

        .cards__body {
          border-top: 2px solid #EEEEEE;
          padding-top: 1rem;
          color: #727272;
        }
      }
    }
    
  }
`;
