import styled from 'styled-components';

export const GuaranteeComponent:any = styled('section')`
  margin-bottom: 5rem;

  .img {
    width: 168px;
    height: 168px;

    img {
      width: 100%;
      height: auto;
    }
  }

  @media only screen and (max-width: 48.0625em) {
    .media-card {
      flex-wrap: wrap;
      text-align: center;

      .media-card__logo {
        padding: 0;
        padding-bottom: 1rem;
        flex-basis: 100%;
        max-width: 100%;
      }
    }
  }
`;