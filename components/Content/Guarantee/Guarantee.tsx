import * as Amp from 'react-amphtml';

import { GuaranteeComponent } from './GuaranteeComponent.Styles';

export default function Guarantee({ data }:any) {

  return (
    <GuaranteeComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center happiness-guaranteed">
          <div className="cell grid-x small-12 medium-8 align-center happiness-guaranteed__media-cards-container">
            <div className="media-object align-middle stack-for-small media-card">

              <div className="media-object-section media-card__logo">
                <Amp.AmpImg
                  specName="default"
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  src="/static/images/stamp.svg"
                  className="img"
                  layout="flex-item"
                  alt="Гарантии в москве"
                />
              </div>

              <div className="media-object-section main-section">
                <h2 className="media-card__title">{data.title}</h2>
                <p className="media-card__description" dangerouslySetInnerHTML={{ __html: data.description }} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </GuaranteeComponent>
  );
};