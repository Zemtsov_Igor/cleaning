import { GetStartedComponent } from './GetStartedComponent.Styles';
import React from "react";

export default function GetStarted({ data }:any) {

  return (
    <GetStartedComponent className={`${data.className} ${data.align}`}>
      <div className="grid-container">
        <div className="grid-x align-center">
          <div className="cell small-12 get-started">
            <span
              role="button"
              tabIndex={0}
              className="btn btn-primary back-to-top anchor-link"
              data-anchor={data.btnAction}
            >
              {data.btnTitle}
            </span>
          </div>
        </div>
      </div>
    </GetStartedComponent>
  );
};

export const turboGetStarted:any = (data:any) => {
  return `
    <div class="btn-section ${data.className ? data.className : ''} ${data.align ? data.align : ''}">
      <div class="grid-container">
        <div class="grid-x align-center">
          <div class="cell small-12 get-started">
            <a href="${data.btnAction}" data-turbo="true" class="btn btn-primary back-to-top">${data.btnTitle}</a>
          </div>
        </div>
      </div>
    </div>
  `;
}
