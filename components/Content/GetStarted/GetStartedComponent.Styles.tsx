import styled from 'styled-components';

export const GetStartedComponent:any = styled('div')`
  &.in-section {
    padding-bottom: 1em;
    margin-bottom: 5rem;
  }

  &.left {
    .grid-container {
      padding-left: 0;
      padding-right: 0;
    }

    .align-center {
      justify-content: flex-start;
    }
  }
  
  .btn {
    display: inline-block;
  }
`;
