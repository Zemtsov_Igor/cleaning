import { FaqComponent } from './FaqComponent.Styles';

export default function Faq({ data }:any) {

  return (
    <FaqComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center">
          <div className="grid-x align-center small-12 medium-10 faq">
            <div className="cell text-center faq__title">
              <h3>{data.title}</h3>
            </div>

            <ul className="faq-question">
              {
                data.items.map((item:any) => (
                  <li
                    key={Buffer.from(Math.random().toString()).toString('base64')}
                    className="faq-question-col -hidden"
                  >
                    <h4 className="faq__question">
                      <span className="cell grid-x align-middle faq__question_wrap">
                        <span className="cell small-11 question">{item.title}</span>
                        <i className="cell icon" />
                      </span>
                    </h4>
                    <div className="cell small-12 faq__answer">
                      {
                        (item.text) && (
                          <p dangerouslySetInnerHTML={{ __html: item.text }} />
                        )
                      }
                      {
                        (!!item.list.length) && (
                          <ul>
                            {
                              item.list.map((listItem:any) => (
                                <li dangerouslySetInnerHTML={{ __html: listItem }} />
                              ))
                            }
                          </ul>
                        )
                      }
                    </div>
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
      </div>
    </FaqComponent>
  );
};

const getFaqItems = (items:any) => {
  let turboFaqItems:string = '';
  items.forEach((item:any) => {
    turboFaqItems = turboFaqItems + `
      <div
        data-block="item"
        data-title="${item.title}"
      >
        <p>${item.text}</p>
      </div>
    `
  });

  return turboFaqItems;
};

export const turboFaq:any = (data:any) => {
  return `
    <section class="section ${data.className ? data.className : ''}">
      <div class="grid-container">
        <div class="grid-x align-center">
          <div class="grid-x align-center small-12 medium-10 faq">
            <div class="cell text-center faq__title">
              <h3>${data.title}</h3>
            </div>
            <div data-block="accordion">
              ${getFaqItems(data.items)}
            </div>
          </div>
        </div>
      </div>
    </section>
  `;
}
