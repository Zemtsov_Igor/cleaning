import styled from 'styled-components';

export const FaqComponent:any = styled('section')`
  margin-bottom: 5rem;

  &.with_btn {
    margin-bottom: 0;
    padding-bottom: 0;
  }

  .faq__title {
    h3 {
      margin-bottom: 2rem;
      text-align: center;
      font-size: 1.625em;
      color: #434343;
      line-height: 1.25em;
      font-weight: 400;
    }
  }

  .faq-question {
    width: 100%;
  }

  .faq__question {
    background: 0;
    border: 0;
    cursor: pointer;
    font-size: 1.25rem;
    margin-bottom: 1rem;
    border-bottom: 1px solid #D1D1D1;
    justify-content: space-between;
    flex-wrap: nowrap;
    outline: 0;
    padding-right: 0;
    margin-top: 0;

    .faq__question_wrap {
      padding-right: 0;
      justify-content: space-between;
      flex-wrap: nowrap;
    }

    .question {
      padding-bottom: 1rem;
      text-align: left;
      margin-bottom: 0;
      font-size: inherit;
      line-height: 1.6;
      margin-top: 0;
      text-rendering: optimizeLegibility;
    }

    .icon {
      position: relative;
      width: 1rem;
      height: 1rem;
      cursor: pointer;

      &:before, :after {
        content: "";
        position: absolute;
        background-color: #D1D1D1;
      }

      &:before {
        visibility: hidden;
        top: 0;
        left: 50%;
        width: 0.125rem;
        height: 100%;
        margin-left: -1px;
        transition: all 0.5s ease-in-out;
      }

      &:after {
        top: 50%;
        left: 0;
        width: 100%;
        height: 0.125rem;
        margin-top: -1px;
      }
    }
  }

  .faq__answer {
    height: auto;
    overflow: hidden;
    font-size: initial;
    padding-left: 1rem;
    margin-bottom: 1rem;
    transition: all 0.5s ease-in-out;

    p {
      margin-bottom: 1rem;
      font-size: inherit;
      line-height: 1.6;
      text-rendering: optimizeLegibility;
    }
  }

  .faq-question-col {
    padding: 0;
    margin: 0;
    
    &::marker {
      font-size: 0;
    }

    &.-hidden {
      .icon {
        &:before {
          visibility: visible;
        }
      }

      .faq__answer {
        height: 0;
        margin-bottom: 0;
      }
    }
  }

  

  @media only screen and (max-width: 40.0625em) {
    .faq__title {
      h3 {
        font-size: 2rem;
      }
    }
  }

  @media only screen and (min-width: 64.0625em) { //1024px
    .faq__title {
      h3 {
        font-size: 2em;
        line-height: 1.25em;
      }
    }
  }
`;
