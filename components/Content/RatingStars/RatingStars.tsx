import { RatingStarsComponent } from './RatingStarsComponent.Styles';
import React from "react";

export default function RatingStars({ data }:any) {
  const stars:any[] = [];

  let counter = 0;
  let rating = data.rating;
  while (counter < 5) {
    const item:number = rating < 20 ? (rating * 5) : 100;
    stars.push(item > 0 ? item : 0);
    counter = counter + 1;
    rating = rating - 20;
  }

  return (
    <RatingStarsComponent
      className="rating-stars"
      itemProp={`${data.itemProp}`}
      itemScope
      itemType={`${data.itemType}`}
    >
      {
        stars.map((star:number) => (
          <div className="star">
            <i className="icon-o">
              <i className="icon" style={{width: `${star}%`}} />
            </i>
          </div>
        ))
      }
      <meta itemProp="ratingValue" content={`${((data.rating / 100) * 5).toFixed(1)}`} />
      <meta itemProp={`${data.ratingType}`} content="5" />
    </RatingStarsComponent>
  );
};
