import styled from 'styled-components';

export const StatisticsComponent:any = styled('section')`
  h2 {
    margin-bottom: 1rem;
  }

  .region-statistics__cards-container {
    margin: 1rem 0 3rem 0;

    .cards {
      padding: 1rem 2rem;
      text-align: center;
      border: 2px solid #EEEEEE;
      box-shadow: 4px 3px 6px 0 rgba(0,0,0,0.05);
      border-radius: 3px;
    }

    .cards__counter {
      font-size: 2rem;
      font-weight: bold;
      margin-bottom: 0;
    }

    .cards__description {
      font-size: 1.25rem;
    }
  }
`;