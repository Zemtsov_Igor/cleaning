import { StatisticsComponent } from './StatisticsComponent.Styles';

export default function Statistics({ data }:any) {

  return (
    <StatisticsComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center">
          <div className="cell small-12 text-center region-statistics__title">
            <h2>{data.title}</h2>
          </div>
          <div className="cell grid-x grid-x grid-margin-x grid-margin-y small-12 region-statistics__cards-container">

            {
              data.items.map((item:any) => (
                <div className="cell grid-margin-x small-12 large-4 cards">
                  <div className="cell cards__counter">{item.count}</div>
                  <div className="cell cards__description">{item.title}</div>
                </div>
              ))
            }

          </div>
        </div>
      </div>
    </StatisticsComponent>
  );
};