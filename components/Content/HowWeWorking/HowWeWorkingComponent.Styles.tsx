import styled from 'styled-components';

export const HowWeWorkingComponent:any = styled('section')`
  margin-bottom: 5rem;
  
  &:not(.even-section) {
    margin-bottom: 3em;
  }

  .how-it-works {
    padding: 3rem 0;

    h2 {
      text-align: center;
      margin-bottom: 3rem;
    }

    .img {
      width: 88px;
      height: 88px;
      display: inline-block;
      vertical-align: middle;
      max-width: 100%;

      img {
        width: 88px;
        height: 88px;
      }
    }

    h3 {
      margin: 1rem 0.5rem 0.5rem;
    }

    p {
      font-size: 1rem;
    }

    ul {
      list-style: none;

      li {
        &:before {
          margin-right: 10px;
          content: '';
          display: inline-block;
          width: 1rem;
          height: 1rem;
          background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOGJjNTNlIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
          -webkit-transition: .5s;
          transition: .5s;
        }
      }
    }
  }

  .media-card {
    margin-bottom: 3rem;
  }


  @media only screen and (min-width: 64.0625em) { //1024px
    .how-it-works {

      h2 {
        position: relative;

        &:before {
          border-top: 1px solid #BABBBD;
          content: "";
          margin: 0 auto;
          position: absolute;
          top: 50%;
          left: 0;
          right: 0;
          bottom: 0;
          z-index: -1;
        }

        span {
          background: #FFFFFF;
          padding: 0 50px;
        }
      }
    }

    &.even-section {
      .how-it-works {
        h2 {
          &:before {
            border-color: #fff;
          }

          span {
            background: #f9f9f9;
          }
        }
      }
    }

    .media-card {
      margin-bottom: 0;
    }
  }
`;
