import { HowWeWorkingComponent } from './HowWeWorkingComponent.Styles';

export default function HowWeWorking({ data }:any) {

  return (
    <HowWeWorkingComponent className={`row ${data.className}`}>
      <div className="grid-container">
        <div className="grid-x how-it-works">
          <div className="cell small-12 medium-12 text-center">
            <h2>
              <span>{data.title}</span>
            </h2>
          </div>
          <div className="cell grid-x how-it-works__media-cards-container">
            {
              data.items.map((item:any) => (
                <div
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  className="cell grid-x small-12 large-4 align-center text-center media-card"
                >
                  <div className="cell">
                    <img
                      src={item.icon.src}
                      className="img"
                      alt={item.icon.alt}
                    />
                  </div>
                  <div className="cell small-10">
                    <h3>{item.title}</h3>
                    <p itemProp="description" dangerouslySetInnerHTML={{ __html: item.text }} />
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    </HowWeWorkingComponent>
  );
};

const getHowWeWorkingItems = (items:any) => {
  let turboHowWeWorkingItems:string = '';
  items.forEach((item:any) => {
    turboHowWeWorkingItems = turboHowWeWorkingItems + `
      <div class="cell grid-x small-12 large-4 align-center text-center media-card">
        <div class="cell">
          <img src="https://my-cleaning.ru${item.icon.src}" alt="${item.icon.alt}" class="img" />
        </div>
        <div class="cell small-10">
          <h3>${item.title}</h3>
          <p itemprop="description">${item.text}</p>
        </div>
      </div>
    `
  });

  return turboHowWeWorkingItems;
};

export const turboHowWeWorking:any = (data:any) => {
  return `
    <section class="section row ${data.className ? data.className : ''}">
      <div class="grid-container">
        <div class="grid-x how-it-works">
          <div class="cell small-12 medium-12 text-center">
            <h2><span>${data.title}</span></h2>
          </div>
          <div class="cell grid-x how-it-works__media-cards-container">
            ${getHowWeWorkingItems(data.items)}
          </div>
        </div>
      </div>
    </section>
  `;
}
