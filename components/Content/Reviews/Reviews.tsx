import { ReviewsComponent } from './ReviewsComponent.Styles';

import { papulate } from '../../../utils/papulate';
import RatingStars from '../RatingStars/RatingStars';
import React from 'react';
import { useRouter } from 'next/router'

export default function Reviews({ data }:any) {
  const router = useRouter()
  const query = router.asPath.replace(router.pathname, '');
  const queryObj = query.length ? JSON.parse('{"' + decodeURI(query.substring(1)).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}') : {};

  const itemsPerPage:number = data.pagination.itemsPerPage;
  const currentPage:number = queryObj.reviews ? Number(queryObj.reviews) : data.pagination.currentPage;

  const totalCount:number = data.pagination.totalCount;
  const maxPaginationLength:number = data.pagination.maxPaginationLength;
  const reviewsItems:any[] = [];
  let itemsArr:any[] = [];
  let page:number = 1;
  data.items.forEach((item: any, index: number) => {
    if ((index + 1) < (itemsPerPage * page)) {
      itemsArr.push(item);
    }

    if ((index + 1) === (itemsPerPage * page)) {
      itemsArr.push(item);
      reviewsItems.push(itemsArr);
      itemsArr = [];
      page += 1;
    }
  });


  const pageCount:number = Math.ceil(totalCount / itemsPerPage);
  const visiblePagesCount = pageCount >= maxPaginationLength ? maxPaginationLength : pageCount;

  return (
    <ReviewsComponent id="reviews" className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x customer-reviews">
          <div className="cell grid-x small-12 medium-8">
            <div className="cell text-center customer-reviews__title">
              <h2>{data.title}</h2>
            </div>
            <div className="cell customer-reviews__counter">
              <p>{totalCount} { papulate(totalCount, ['отзыв', 'отзыва', 'отзывов']) } {data.description}</p>
            </div>

            <div className="cell customer-reviews__container">
              <div className="customer-reviews__list" >

                {
                  reviewsItems[currentPage - 1].map((item:any) => (<div
                    className="cell professional"
                    itemProp="review"
                    itemScope
                    itemType="https://schema.org/Review"
                  >
                    <div className="cell professional__rating">
                      <RatingStars data={{rating: item.rating, itemProp: 'reviewRating', itemType: 'https://schema.org/Rating', ratingType: 'bestRating'}} />
                    </div>
                    <div
                      className="cell professional__name"
                      itemProp="author"
                      itemScope
                      itemType="https://schema.org/Person"
                    >
                      <p itemProp="name" >{item.name}</p>
                    </div>
                    <div className="cell professional__description">
                      <p itemProp="reviewBody">{item.comment}</p>
                    </div>
                  </div>)
                  )
                }
              </div>
            </div>

            <div className="cell grid-x align-center">
              <div className="cell customer-reviews__pagination">
                <nav
                  aria-label="Pagination"
                  className="grid-x pagination_wrap"
                  data-current={currentPage}
                  data-items={JSON.stringify(reviewsItems)}
                >
                  <div className="cell small-1 pagination-previous">
                    <span className={`pagination-previous__btn ${(currentPage < 2) ? '' : '-visible'}`} />
                  </div>
                  <ul className="cell pagination text-center">
                    {
                      [...Array(visiblePagesCount)].map((_, index:number) => (
                        <li className={`pagination-item page-${index + 1} ${(currentPage === (index + 1)) ? 'current' : ''}`}>
                          <span data-page={index + 1} className="pagination-item__btn">
                            { index + 1 }
                          </span>
                        </li>
                      ))
                    }
                  </ul>
                  <div className="cell small-1 pagination-next">
                    <span className={`pagination-next__btn ${(currentPage < pageCount) ? '-visible' : ''}`} />
                  </div>
                </nav>
              </div>
            </div>

          </div>
        </div>
      </div>

    </ReviewsComponent>
  );
};

const getTurboReviewsItems = (items:any) => {
  let turboReviewsItems:string = '';
  items.forEach((item:any) => {
    turboReviewsItems = turboReviewsItems + `
      <div class="cell professional" role="listitem">
        <div class="cell professional__rating">
          <div class="rating-stars">
            <div itemscope itemtype="http://schema.org/Rating">
              <meta itemprop="ratingValue" content="${((item.rating / 100) * 5).toFixed(1)}">
              <meta itemprop="bestRating" content="5">
            </div>
          </div>
        </div>
        <div class="cell professional__name">
          <p>${item.name}</p>
        </div>
        <div class="cell professional__description">
          <p>${item.comment}</p>
        </div>
      </div>
    `
  });

  return turboReviewsItems;
};

export const turboReviews:any = (data:any, url:string) => {
  return `
    <section class="section ${data.className ? data.className : ''}">
      <div class="grid-container">
        <div class="grid-x customer-reviews">
          <div class="cell grid-x small-12 medium-8">
            <div class="cell text-center customer-reviews__title">
              <h2 id="reviews">${data.title}</h2>
            </div>
            <div class="cell customer-reviews__counter">
              <p>${data.pagination.totalCount} ${ papulate(data.pagination.totalCount, ['отзыв', 'отзыва', 'отзывов']) } ${data.description}</p>
            </div>
            <div class="cell customer-reviews__container">
              <div class="customer-reviews__list">
                ${getTurboReviewsItems(data.turboItems)}
              </div>
            </div>
            <div class="cell grid-x align-center">
              <div class="cell customer-reviews__pagination">
                <div class="grid-x pagination_wrap">
                  <div class="cell small-1 pagination-previous"></div>
                  <ul class="cell text-center">
                    <li>
                      <span
                        class="customer-reviews__pagination_btn current"
                      >
                        1
                      </span>
                    </li>
                    <li>
                      <a
                        class="customer-reviews__pagination_btn"
                        href="${url}?reviews=2"
                        data-turbo="false"
                      >
                        2
                      </a>
                    </li>
                    <li>
                      <a
                        class="customer-reviews__pagination_btn"
                        href="${url}?reviews=3"
                        data-turbo="false"
                      >
                        3
                      </a>
                    </li>
                  </ul>
                  <div class="cell small-1 pagination-next">
                    <a
                      class="pagination-next__btn"
                      href="${url}?reviews=2"
                      data-turbo="false"
                    >></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  `;
}
