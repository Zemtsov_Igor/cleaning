import styled from 'styled-components';

export const ReviewsComponent:any = styled('section')`
  margin-bottom: 5rem;

  &.with_btn {
    margin-bottom: 0;
    padding-bottom: 0;
  }

  .customer-reviews {
    justify-content: center;
  }

  .customer-reviews__title {
    h2 {
      margin-bottom: .5rem;
    }
  }

  .customer-reviews__counter {
    margin-bottom: 2rem;
    text-align: center;
  }

  .customer-reviews__container {
    display: flex;
    position: relative;
  }

  .customer-reviews__list {
    min-height: 1px;

    div[role=list] {
      position: relative;
      width: auto;
      height: auto;
    }
  }

  .professional {
    margin-bottom: 2rem;
  }

  .professional__rating {
    color: #FFB600;
  }

  .professional__name {
    margin: .5rem 0;
    font-size: larger;
  }

  .pagination_wrap {
    align-items: center;
    justify-content: center;
    margin-bottom: 2rem;

    .pagination-previous {
      text-align: left;

      .pagination-previous__btn {
        display: none;
        color: #434343;
        padding: 0.625rem;
        background: none;
        border: 0;
        outline: none;
        cursor: pointer;
        
        &.-visible {
          display: block;
        }
        
        &:before {
          content: '';
          display: block;
          width: 0.5rem;
          height: 0.9rem;
          margin-top: .25rem;
          background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMjAgNTEyIj48cGF0aCBmaWxsPSIjNDM0MzQzIiBkPSJNMzQuNTIgMjM5LjAzTDIyOC44NyA0NC42OWM5LjM3LTkuMzcgMjQuNTctOS4zNyAzMy45NCAwbDIyLjY3IDIyLjY3YzkuMzYgOS4zNiA5LjM3IDI0LjUyLjA0IDMzLjlMMTMxLjQ5IDI1NmwxNTQuMDIgMTU0Ljc1YzkuMzQgOS4zOCA5LjMyIDI0LjU0LS4wNCAzMy45bC0yMi42NyAyMi42N2MtOS4zNyA5LjM3LTI0LjU3IDkuMzctMzMuOTQgMEwzNC41MiAyNzIuOTdjLTkuMzctOS4zNy05LjM3LTI0LjU3IDAtMzMuOTR6Ii8+PC9zdmc+);
          background-repeat: no-repeat;
        }
      }
    }

    .pagination {
      margin-left: 0;
      margin-bottom: 0;
      min-width: 6.25rem;
      max-width: 15.9375rem;

      &:before, &:after {
        display: table;
        content: ' ';
        flex-basis: 0;
        order: 1;
      }

      li {
        margin-right: 0.0625rem;
        border-radius: 0.1875rem;
        font-size: 1rem;
        display: inline-block;
      }

      .pagination-item__btn {
        display: block;
        padding: 0.4375rem 0.875rem;
        border-radius: 0.1875rem;
        color: #434343;
        font-size: 1rem;
        background: 0;
        border: 0;
        outline: none;
        cursor: pointer;
      }

      .current {
        background: #00CDED;

        .pagination-item__btn {
          color: #ffffff;
          cursor: default;
        }
      }
    }

    .pagination-next {
      text-align: right;

      .pagination-next__btn {
        display: none;
        color: #434343;
        padding: 0.625rem;
        background: none;
        border: 0;
        outline: none;
        cursor: pointer;

        &.-visible {
          display: block;
        }

        &:before {
          content: '';
          display: block;
          width: 0.5rem;
          height: 0.9rem;
          margin-top: .25rem;
          background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMjAgNTEyIj48cGF0aCBmaWxsPSIjNDM0MzQzIiBkPSJNMjg1LjQ3NiAyNzIuOTcxTDkxLjEzMiA0NjcuMzE0Yy05LjM3MyA5LjM3My0yNC41NjkgOS4zNzMtMzMuOTQxIDBsLTIyLjY2Ny0yMi42NjdjLTkuMzU3LTkuMzU3LTkuMzc1LTI0LjUyMi0uMDQtMzMuOTAxTDE4OC41MDUgMjU2IDM0LjQ4NCAxMDEuMjU1Yy05LjMzNS05LjM3OS05LjMxNy0yNC41NDQuMDQtMzMuOTAxbDIyLjY2Ny0yMi42NjdjOS4zNzMtOS4zNzMgMjQuNTY5LTkuMzczIDMzLjk0MSAwTDI4NS40NzUgMjM5LjAzYzkuMzczIDkuMzcyIDkuMzczIDI0LjU2OC4wMDEgMzMuOTQxeiIvPjwvc3ZnPg==);
          background-repeat: no-repeat;
        }
      }
    }
  }

  @media only screen and (max-width: 48.0625em) {
    .pagination_wrap {
      .pagination {
        max-width: 15rem;
      }
    }
  }
`;
