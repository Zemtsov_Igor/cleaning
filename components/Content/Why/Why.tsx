import { WhyComponent } from './WhyComponent.Styles';

export default function Why({ data }:any) {

  return (
    <WhyComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center">
          <div className="cell small-12 large-10">
            <div className="cell head__title">
              <h2>{data.title}</h2>
            </div>
            <div className="cell head__description">
              <p itemProp="description" className="section-sub-header">{data.description}</p>
            </div>
          </div>
          <div className="cell small-12 large-10 capabilities">
            {
              data.items.map((item:any) => (
                <div
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  className="media-object media-card"
                >
                  <div className="media-object-section media-card__icon" />
                  <div className="media-object-section">
                    <p itemProp="description" dangerouslySetInnerHTML={{ __html: item.text }} />
                  </div>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    </WhyComponent>
  );
};

const getWhyItems = (items:any) => {
  let turboWhyItems:string = '';
  items.forEach((item:any) => {
    turboWhyItems = turboWhyItems + `
      <div class="media-object media-card">
        <div class="media-object-section media-card__icon"></div>
        <div class="media-object-section">
          <p class="media-card__description">${item.text}</p>
        </div>
      </div>
    `
  });

  return turboWhyItems;
};

export const turboWhy:any = (data:any) => {
  return `
    <section class="section row ${data.className ? data.className : ''}">
      <div class="grid-container">
        <div class="grid-x align-center">
          <div class="cell small-12 large-10">
            <div class="cell head__title">
              <h2>${data.title}</h2>
            </div>
            <div class="cell head__description">
              <p class="section-sub-header">${data.description}</p>
            </div>
          </div>
          <div class="cell small-12 large-10 capabilities">
            ${getWhyItems(data.items)}
          </div>
        </div>
      </div>
    </section>
  `;
}
