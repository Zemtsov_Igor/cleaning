import styled from 'styled-components';

export const WhyComponent:any = styled('div')`

  &.even-section {
    padding-top: 2em;
  }

  .head__title {
    margin-bottom: .5rem;
  }

  .head__description {
    margin-bottom: 2rem;

    p {
      font-size: 1.1rem;
      margin-bottom: 1em;
    }
  }

  .media-object {
    display: flex;
    margin-bottom: 1rem;
    flex-wrap: nowrap;
  }

  .media-object-section {
    flex: 0 1 auto;

    &:first-child {
      padding-right: 1rem;
    }

    > {
      &:last-child {
        margin-bottom: 0;
      }
    }

    &.media-card__icon {
      &:before {
        content: '';
        display: block;
        width: 1rem;
        height: 1.15rem;
        margin-top: 0.25rem;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjOGJjNTNlIiBkPSJNMTczLjg5OCA0MzkuNDA0bC0xNjYuNC0xNjYuNGMtOS45OTctOS45OTctOS45OTctMjYuMjA2IDAtMzYuMjA0bDM2LjIwMy0zNi4yMDRjOS45OTctOS45OTggMjYuMjA3LTkuOTk4IDM2LjIwNCAwTDE5MiAzMTIuNjkgNDMyLjA5NSA3Mi41OTZjOS45OTctOS45OTcgMjYuMjA3LTkuOTk3IDM2LjIwNCAwbDM2LjIwMyAzNi4yMDRjOS45OTcgOS45OTcgOS45OTcgMjYuMjA2IDAgMzYuMjA0bC0yOTQuNCAyOTQuNDAxYy05Ljk5OCA5Ljk5Ny0yNi4yMDcgOS45OTctMzYuMjA0LS4wMDF6Ii8+PC9zdmc+);
        background-repeat: no-repeat;
      }
    }

    .media-card__description {
      display: inline;
    }
  }
`;
