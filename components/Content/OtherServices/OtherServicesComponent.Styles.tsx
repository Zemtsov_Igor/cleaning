import styled from 'styled-components';

export const OtherServicesComponent:any = styled('section')`
  margin-bottom: 5rem;

  &.even-section {
    padding-top: 2em;
  }

  .head__title {
    margin-bottom: .5rem;
  }

  .tags {
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-wrap: wrap;
    padding-top: 25px;

    &-tag {
      font-size: 1em;
      margin: 0 5px;
      padding: 5px 0;
      border-bottom: 1px dashed #00af64;
    }
  }

`;
