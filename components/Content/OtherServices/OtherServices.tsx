import { OtherServicesComponent } from './OtherServicesComponent.Styles';

export default function Why({ data }:any) {
  return (
    <OtherServicesComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center">
          <div className="cell small-12 large-10">
            <div className="cell head__title">
              <h2>{data.title}</h2>
            </div>
          </div>
          <div className="cell small-12 large-10 tags">
            {
              data.items.map((item:any) => (
                <a
                  key={Buffer.from(Math.random().toString()).toString('base64')}
                  href={`${item.link}`}
                  className="tags-tag"
                >
                  {item.name}
                </a>
              ))
            }
          </div>
        </div>
      </div>
    </OtherServicesComponent>
  );
};

const getTurboOtherServicesItems = (items:any) => {
  let turboOtherServicesItems:string = '';
  items.forEach((item:any) => {
    turboOtherServicesItems = turboOtherServicesItems + `
      <a
        href="${item.link}"
        class="tags-tag"
      >${item.name}</a>
    `
  });

  return turboOtherServicesItems;
};

export const turboOtherServices:any = (data:any) => {
  return `
    <section class="section tags-section ${data.className ? data.className : ''}">
      <div class="grid-container">
        <div class="grid-x align-center">
          <div class="cell small-12 large-10">
            <div class="cell head__title">
              <h2>${data.title}</h2>
            </div>
          </div>
          <div class="cell small-12 large-10 tags">
            ${getTurboOtherServicesItems(data.items)}
          </div>
        </div>
      </div>
    </section>
  `;
}
