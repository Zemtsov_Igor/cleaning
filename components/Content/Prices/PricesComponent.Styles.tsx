import styled from 'styled-components';

export const PricesComponent:any = styled('section')`
  margin-bottom: 5rem;

  &.even-section {
    padding-top: 2em;
  }

  .head__title {
    margin-bottom: .5rem;
  }

  .head__description {
    margin-bottom: 2rem;

    p {
      font-size: 1.1rem;
      margin-bottom: 1em;
    }
  }

  .prices {

    &-table {
      display: flex;
      flex-direction: column;

      &__col {
        display: flex;
        justify-content: center;
        align-items: center;
        flex: 1;
        padding: 0 15px;

        &:nth-child(1) {
          flex: 3;
        }

        & b {
          font-weight: bold;
        }
      }

      &__row {
        display: flex;
        min-height: 50px;

        &.-bold {
          & .prices-table__col {
            font-weight: bold;
          }
        }

        &:nth-child(even) {
          background-color: #f9f9f9;
        }

        & .prices-table__col {
          &:nth-child(1) {
            justify-content: flex-start;
          }
        }

        &:first-child {
          & .prices-table__col {
            background-color: #0bb8e3;
            color: #ffffff;
            text-align: center;
          }
        }
      }

      &.-peoples {
        & .prices-table__col {
          &:nth-child(1) {
            flex: 1;
          }
        }
      }
    }
  }

  @media only screen and (max-width: 48.0625em) { //768px
    .prices {
      &-table {
        &__row {
          flex-direction: column;

          & .prices-table__col {
            padding: 10px;
            border-bottom: 1px solid #f9f9f9;

            &:nth-child(1) {
              justify-content: center;
              text-align: center;
            }
          }

          &:first-child {
            & .prices-table__col {
              font-weight: bold;
              border-bottom: 1px solid #ffffff;
            }
          }

          &:nth-child(even) {
            & .prices-table__col {
              border-bottom: 1px solid #ffffff;
            }
          }
        }
      }
    }
  }

`;
