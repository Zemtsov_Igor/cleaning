import { PricesComponent } from './PricesComponent.Styles';
import React from "react";

export default function Why({ data }:any) {
  return (
    <PricesComponent className={`${data.className}`}>
      <div className="grid-container">
        <div className="grid-x align-center">
          <div className="cell small-12 large-10">
            <div className="cell head__title">
              <h2>{data.title}</h2>
            </div>
            {
              !!data.description && (
                <div className="cell head__description">
                  <p className="section-sub-header">{data.description}</p>
                </div>
              )
            }
          </div>
          <div className="cell small-12 large-10 prices">

            {
              data.type === 'unit' && (
                <div className="prices-table">
                  <div className="prices-table__row">
                    <div className="prices-table__col">Услуга</div>
                    <div className="prices-table__col"><b>Ед. измерения</b></div>
                    <div className="prices-table__col"><b>Цена</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.name}</div>
                        <div className="prices-table__col">{item.unit}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'apartmentPeoples' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col"><b>Площадь квартиры</b></div>
                    <div className="prices-table__col"><b>Цена</b></div>
                    <div className="prices-table__col"><b>Количество клинеров</b></div>
                    <div className="prices-table__col"><b>Продолжительность уборки</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.unit}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <div className="prices-table__col">{item.name}</div>
                        <div className="prices-table__col">{item.hours}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'office' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col">Вид клининга</div>
                    <div className="prices-table__col"><b>до 100 м²</b></div>
                    <div className="prices-table__col"><b>до 200 м²</b></div>
                    <div className="prices-table__col"><b>до 300 м²</b></div>
                    <div className="prices-table__col"><b>более 300 м²</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.name}</div>
                        <div className="prices-table__col">{item.from100}</div>
                        <div className="prices-table__col">{item.till200}</div>
                        <div className="prices-table__col">{item.till300}</div>
                        <div className="prices-table__col">{item.above300}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.from100.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'window' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col">Название услуги</div>
                    <div className="prices-table__col"><b>Цена</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.name}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'') || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'premises' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col"><b>Площадь помещения</b></div>
                    <div className="prices-table__col"><b>Поддерживающая уборка</b></div>
                    <div className="prices-table__col"><b>Генеральная уборка</b></div>
                    <div className="prices-table__col"><b>Уборка после ремонта</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.unit}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <div className="prices-table__col">{item.price1}</div>
                        <div className="prices-table__col">{item.price2}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'apartment_disinfection' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col"><b>Площадь квартиры</b></div>
                    <div className="prices-table__col"><b>Цена дезинфекции</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.unit}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'home_disinfection' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col"><b>Площадь помещения</b></div>
                    <div className="prices-table__col"><b>Цена дезинфекции</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.unit}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'dry_cleanin' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col">Услуги химчистки</div>
                    <div className="prices-table__col"><b>Цена</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.name}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'apartment_unit_price' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col">Площадь квартиры</div>
                    <div className="prices-table__col"><b>Цена</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.unit}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'home_rem' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col">Площадь коттеджа</div>
                    <div className="prices-table__col"><b>Уборка после ремонта</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.unit}</div>
                        <div className="prices-table__col">{item.price}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="url" content={`https://my-cleaning.ru${data.url}`} />
                        <meta itemProp="priceValidUntil" content="2030-11-05" />
                        <meta itemProp="price" content={`${item.price.replace(/\D/g,'')  || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

            {
              data.type === 'alpinistami' && (
                <div className="prices-table -peoples">
                  <div className="prices-table__row">
                    <div className="prices-table__col"/>
                    <div className="prices-table__col"><b>до 1000 м²</b></div>
                    <div className="prices-table__col"><b>1001 – 8000 м²</b></div>
                    <div className="prices-table__col"><b>более 8000 м²</b></div>
                    <div className="prices-table__col"><b>Зимний период</b></div>
                  </div>
                  {
                    data.items.map((item:any) => (
                      <div
                        key={Buffer.from(Math.random().toString()).toString('base64')}
                        className={`prices-table__row -${item.style}`}
                        itemProp="offers"
                        itemScope
                        itemType="http://schema.org/Offer"
                      >
                        <div className="prices-table__col">{item.name}</div>
                        <div className="prices-table__col">{item.from100}</div>
                        <div className="prices-table__col">{item.till200}</div>
                        <div className="prices-table__col">{item.till300}</div>
                        <div className="prices-table__col">{item.above300}</div>
                        <meta itemProp="priceCurrency" content="RUB" />
                        <meta itemProp="price" content={`${item.from100.replace(/\D/g,'') || 0}`} />
                        <meta itemProp="url" content={data.url} />
                        <meta itemProp="priceValidUntil" content="2031-09-13" />
                        <link itemProp="availability" href="http://schema.org/InStock" />
                      </div>
                    ))
                  }
                </div>
              )
            }

          </div>
        </div>
      </div>
    </PricesComponent>
  );
};

const getPricesItems = (data:any) => {
  let turboPricesItems:string = '';

  if(data.type === 'unit') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table">
        <div class="prices-table__row">
          <div class="prices-table__col">Услуга</div>
          <div class="prices-table__col"><b>Ед. измерения</b></div>
          <div class="prices-table__col"><b>Цена</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.name}</div>
          <div class="prices-table__col">${item.unit}</div>
          <div class="prices-table__col">${item.price}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'apartmentPeoples') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col"><b>Площадь квартиры</b></div>
          <div class="prices-table__col"><b>Цена</b></div>
          <div class="prices-table__col"><b>Количество клинеров</b></div>
          <div class="prices-table__col"><b>Продолжительность уборки</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.unit}}</div>
          <div class="prices-table__col">${item.price}</div>
          <div class="prices-table__col">${item.name}</div>
          <div class="prices-table__col">${item.hours}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'office') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col">Вид клининга</div>
          <div class="prices-table__col"><b>до 100 м²</b></div>
          <div class="prices-table__col"><b>до 200 м²</b></div>
          <div class="prices-table__col"><b>до 300 м²</b></div>
          <div class="prices-table__col"><b>более 300 м²</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.name}}</div>
          <div class="prices-table__col">${item.from100}</div>
          <div class="prices-table__col">${item.till200}</div>
          <div class="prices-table__col">${item.till300}</div>
          <div class="prices-table__col">${item.above300}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'window') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col"><b>Название услуги</b></div>
          <div class="prices-table__col"><b>Цена</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.name}}</div>
          <div class="prices-table__col">${item.price}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'premises') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col"><b>Площадь помещения</b></div>
          <div class="prices-table__col"><b>Поддерживающая уборка</b></div>
          <div class="prices-table__col"><b>Генеральная уборка</b></div>
          <div class="prices-table__col"><b>Уборка после ремонта</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.unit}}</div>
          <div class="prices-table__col">${item.price}</div>
          <div class="prices-table__col">${item.price1}}</div>
          <div class="prices-table__col">${item.price2}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'apartment_disinfection') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col"><b>Площадь квартиры</b></div>
          <div class="prices-table__col"><b>Цена дезинфекции</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.unit}}</div>
          <div class="prices-table__col">${item.price}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'home_disinfection') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col"><b>Площадь помещения</b></div>
          <div class="prices-table__col"><b>Цена дезинфекци</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.unit}}</div>
          <div class="prices-table__col">${item.price}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'dry_cleanin') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col">Услуги химчистки</div>
          <div class="prices-table__col"><b>Цена</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.name}}</div>
          <div class="prices-table__col">${item.price}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'apartment_unit_price') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col">Площадь квартиры</div>
          <div class="prices-table__col"><b>Цена</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.unit}}</div>
          <div class="prices-table__col">${item.price}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'home_rem') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col">Площадь коттеджа</div>
          <div class="prices-table__col"><b>Уборка после ремонта</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.unit}}</div>
          <div class="prices-table__col">${item.price}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  if(data.type === 'alpinistami') {
    turboPricesItems = turboPricesItems + `
      <div class="prices-table -peoples">
        <div class="prices-table__row">
          <div class="prices-table__col"></div>
          <div class="prices-table__col"><b>до 1000 м²</b></div>
          <div class="prices-table__col"><b>1001 – 8000 м²</b></div>
          <div class="prices-table__col"><b>более 8000 м²</b></div>
          <div class="prices-table__col"><b>Зимний период</b></div>
        </div>
    `;
    data.items.forEach((item:any) => {
      turboPricesItems = turboPricesItems + `
        <div class="prices-table__row">
          <div class="prices-table__col">${item.name}}</div>
          <div class="prices-table__col">${item.from100}</div>
          <div class="prices-table__col">${item.till200}</div>
          <div class="prices-table__col">${item.till300}</div>
          <div class="prices-table__col">${item.above300}</div>
        </div>
      `
    });
    turboPricesItems = turboPricesItems + `</div>`;
  }

  return turboPricesItems;
};

export const turboPrices:any = (data:any) => {
  return `
    <section class="section prices-section ${data.className ? data.className : ''}">
      <div class="grid-container">
        <div class="grid-x align-center">
          <div class="cell small-12 large-10">
            <div class="cell head__title">
              <h2>${data.title}</h2>
            </div>
          </div>
          <div class="cell small-12 large-10 prices">
            ${getPricesItems(data)}
          </div>
        </div>
      </div>
    </section>
  `;
}
