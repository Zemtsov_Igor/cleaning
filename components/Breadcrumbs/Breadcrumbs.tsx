import { BreadcrumbsComponent } from './BreadcrumbsComponent.Styles';

export default function Breadcrumbs({ path }:any) {
  return (
    <BreadcrumbsComponent>
      <div className="grid-container">
        <div className="grid-x align-center crumbs">
          <div className="cell crumbs__content">
            <ul
              className="breadcrumbs"
              itemScope
              itemType="http://schema.org/BreadcrumbList"
            >
              <li className="crumbs-item crumbs-item__home">
                <a href="/">Главная</a>
              </li>

              {
                path.map((pathObj:any, index: number) => (
                  <li
                    itemProp="itemListElement"
                    itemScope
                    itemType="http://schema.org/ListItem"
                    className="crumbs-item"
                  >
                    {
                      pathObj.url ? (
                        <a itemProp="item" href={pathObj.url}>
                          <span itemProp="name">{pathObj.title}</span>
                        </a>
                      ) : (
                        <span itemProp="name">{pathObj.title}</span>
                      )
                    }
                    <meta itemProp="position" content={`${index + 1}`}/>
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
      </div>
    </BreadcrumbsComponent>
  );

};

const getBreadcrumbsItems = (items:any) => {
  let turboBreadcrumbsItems:string = '';
  items.forEach((item:any, index:number) => {
    turboBreadcrumbsItems = turboBreadcrumbsItems + `
      <li
        itemProp="itemListElement"
        itemScope
        itemType="http://schema.org/ListItem"
      >
        ${
          item.url ? `
            <a itemProp="item" data-turbo="false" href="${item.url}">
              <span itemProp="name">${item.title}</span>
            </a>
          ` : `
            <span itemProp="name">${item.title}</span>
          `
        }
        <meta itemProp="position" content="${index + 1}"/>
      </li>
    `
  });

  return turboBreadcrumbsItems;
};

export const turboBreadcrumbs:any = (data:any) => {
  return `
    <div class="breadcrumbs">
      <div class="grid-container">
        <div class="grid-x align-center crumbs">
          <div class="cell crumbs__content">
            <ul itemtype="http://schema.org/BreadcrumbList">
              <li>
                <a href="/" data-turbo="false">Главная</a>
              </li>
              ${getBreadcrumbsItems(data)}
            </ul>
          </div>
        </div>
      </div>
    </div>
  `;
}
