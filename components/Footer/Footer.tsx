import React, { Component } from 'react';

import { FooterComponent } from './FooterComponent.Styles';

export default class Footer extends Component {

  render() {

    return (
      <FooterComponent>
        <div className="grid-container">
          <div className="grid-x align-center footer">
            <div className="cell grid-x footer__navigation">
              <nav className="cell grid-x small-5 hide-for-small">
                <ul className="menu vertical">
                  <li>
                    <a href="/uslugi">Все услуги</a>
                  </li>
                  <li>
                    <a href="/uslugi/uborka-kvartir">Уборка квартир</a>
                  </li>
                  <li>
                    <a href="/uslugi/uborka-kottedzhej">Уборка коттеджей</a>
                  </li>
                </ul>
              </nav>
              <nav className="cell grid-x small-4 hide-for-small">
                <ul className="menu vertical">
                  <li>
                    <a href="/uslugi/uborka-ofisov">Уборка офисов</a>
                  </li>
                  <li>
                    <a href="/uslugi/himchistka">Химчистка</a>
                  </li>
                  <li>
                    <a href="/uslugi/moyka-okon">Мойка окон</a>
                  </li>
                </ul>
              </nav>
              <nav className="cell grid-x small-3 hide-for-small">
                <ul className="menu vertical">
                  <li>
                    <a href="/uslugi/dezinfekciya">Дезинфекция</a>
                  </li>
                  <li>
                    <a href="/uslugi/uborka-pomeshchenij">Уборка помещений</a>
                  </li>
                </ul>
              </nav>
              <div className="cell grid-x small-12 show-for-small">
                <ul
                  className="cell small-12"
                >
                  <li className="footer-mobile-menu-col -hidden" >
                    <h4 className="footer-mobile-menu">
                      <span className="cell grid-x align-middle footer-mobile-menu_wrap">
                        <span className="cell small-11 footer-mobile">Уборка квартир</span>
                        <i className="cell icon" />
                      </span>
                    </h4>
                    <div className="footer-mobile-answer">
                      <ul className="footer-mobile-answer-menu">
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kvartir/generalnaya">Генеральная уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kvartir/podderzhivayushchaya">Поддерживающая уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kvartir/posle-remonta">Уборка после ремонта</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kvartir/ezhednevnaya">Ежедневная уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kvartir/srochnaya">Срочная уборка</a>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li className="footer-mobile-menu-col -hidden" >
                    <h4 className="footer-mobile-menu">
                      <span className="cell grid-x align-middle footer-mobile-menu_wrap">
                        <span className="cell small-11 footer-mobile">Уборка коттеджей</span>
                        <i className="cell icon" />
                      </span>
                    </h4>
                    <div className="footer-mobile-answer">
                      <ul className="footer-mobile-answer-menu">
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kottedzhej/generalnaya">Генеральная уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kottedzhej/podderzhivayushchaya">Поддерживающая уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kottedzhej/posle-remonta">Уборка после ремонта</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-kottedzhej/srochnaya">Срочная уборка</a>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li className="footer-mobile-menu-col -hidden" >
                    <h4 className="footer-mobile-menu">
                      <span className="cell grid-x align-middle footer-mobile-menu_wrap">
                        <span className="cell small-11 footer-mobile">Мойка окон</span>
                        <i className="cell icon" />
                      </span>
                    </h4>
                    <div className="footer-mobile-answer">
                      <ul className="footer-mobile-answer-menu">
                        <li>
                          <a className="trackable" href="/uslugi/moyka-okon/vitrin">Мойка витрин</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/moyka-okon/fasadov">Мойка фасадов</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/moyka-okon/alpinistami">Мойка альпинистами</a>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li className="footer-mobile-menu-col -hidden" >
                    <h4 className="footer-mobile-menu">
                      <span className="cell grid-x align-middle footer-mobile-menu_wrap">
                        <span className="cell small-11 footer-mobile">Уборка офисов</span>
                        <i className="cell icon" />
                      </span>
                    </h4>
                    <div className="footer-mobile-answer">
                      <ul className="footer-mobile-answer-menu">
                        <li>
                          <a className="trackable" href="/uslugi/uborka-ofisov/ezhednevnaya">Ежедневная уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-ofisov/podderzhivayushchaya">Поддерживающая уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-ofisov/generalnaya">Генеральная уборка</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/uborka-ofisov/posle-remonta">Уборка после ремонта</a>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li className="footer-mobile-menu-col -hidden" >
                    <h4 className="footer-mobile-menu">
                      <span className="cell grid-x align-middle footer-mobile-menu_wrap">
                        <span className="cell small-11 footer-mobile">Химчистка</span>
                        <i className="cell icon" />
                      </span>
                    </h4>
                    <div className="footer-mobile-answer">
                      <ul className="footer-mobile-answer-menu">
                        <li>
                          <a className="trackable" href="/uslugi/himchistka/s-vyezdom">Химчистка с выездом</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/himchistka/myagkoj-mebeli">Химчистка мягкой мебели</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/himchistka/shtor">Химчистка штор</a>
                        </li>
                        <li>
                          <a className="trackable" href="/uslugi/himchistka/kovrov">Химчистка ковров</a>
                        </li>
                      </ul>
                    </div>
                  </li>

                </ul>
              </div>
            </div>
            {/*<div className="cell grid-x footer__social">
              <a target="_blank" href="#">
                <i className="fa fa-facebook"></i>
              </a>
              <a target="_blank" href="#">
                <i className="fa fa-vk"></i>
              </a>
              <a target="_blank" href="#">
                <i className="fa fa-instagram"></i>
              </a>
            </div>*/}
            <div className="cell grid-x footer__interlinks">
              <div className="cell grid-x headline">Другие услуги</div>
              <div className="cell grid-x medium-5">
                <ul className="menu vertical">
                  <li>
                    <a href="/uslugi/uborka-kvartir/odnokomnatnoj">Уборка однокомнатной квартиры</a>
                  </li>
                  <li>
                    <a href="/uslugi/uborka-kvartir/dvuhkomnatnoj">Уборка двухкомнатной квартиры</a>
                  </li>
                </ul>
              </div>
              <div className="cell grid-x medium-4">
                <ul className="menu vertical">
                  <li>
                    <a href="/uslugi/uborka-kvartir/trekhkomnatnoj">Уборка трехкомнатной квартиры</a>
                  </li>
                  <li>
                    <a href="/uslugi/uborka-kottedzhej/chastnyh-domov">Уборка домов</a>
                  </li>
                </ul>
              </div>
              <div className="cell grid-x medium-3">
                <ul className="menu vertical">
                  <li>
                    <a href="/uslugi/uborka-kottedzhej/taunhausov">Уборка таунхаусов</a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="cell grid-x footer__lower-footer">
              <div className="cell grid-x medium-shrink lower-menu">
                <ul className="menu">
                  {/*<li>
                    <a className="trackable" href="/o-nas">О нас</a>
                  </li>*/}
                  <li>
                    <a className="trackable" href="/kontakty">Контакты</a>
                  </li>
                  <li>
                    <a className="trackable" href="/privacy">Политика конфиденциальности</a>
                  </li>
                  <li>
                    <a className="trackable" href="/terms">Условия использования</a>
                  </li>
                  <li>
                    <a className="trackable" href="/sitemap">Карта сайта</a>
                  </li>
                </ul>
              </div>
              <div className="cell grid-x medium-auto align-right copy">
                2021 my-cleaning. Все права защищены.
              </div>
            </div>

            <div className="cell footer__term_description">
              <p>Данный сайт носит информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 ГК РФ.</p>
            </div>

          </div>
        </div>
      </FooterComponent>
    );
  }
};

export const turboFooter:any = `
  <footer class="footer-container">
    <div class="grid-container">
      <div class="grid-x align-center footer">
        <div class="cell grid-x footer__navigation">
          <div class="cell grid-x small-5 nav hide-for-small">
            <ul class="menu vertical">
              <li><a data-turbo="false" href="/uslugi">Все услуги</a></li>
              <li><a data-turbo="false" href="/uslugi/uborka-kvartir">Уборка квартир</a></li>
              <li><a data-turbo="false" href="/uslugi/uborka-kottedzhej">Уборка коттеджей</a></li>
            </ul>
          </div>
          <div class="cell grid-x small-4 nav hide-for-small">
            <ul class="menu vertical">
              <li><a data-turbo="false" href="/uslugi/uborka-ofisov">Уборка офисов</a></li>
              <li><a data-turbo="false" href="/uslugi/himchistka">Химчистка</a></li>
              <li><a data-turbo="false" href="/uslugi/moyka-okon">Мойка окон</a></li>
            </ul>
          </div>
          <div class="cell grid-x small-3 nav hide-for-small">
            <ul class="menu vertical">
              <li><a data-turbo="false" href="/uslugi/dezinfekciya">Дезинфекция</a></li>
              <li><a data-turbo="false" href="/uslugi/uborka-pomeshchenij">Уборка помещений</a></li>
            </ul>
          </div>
          <div class="cell grid-x small-12 nav show-for-small">

            <div data-block="accordion">
              <div data-block="item" data-title="Уборка квартир">
                <ul class="footer-mobile-answer-menu">
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kvartir/generalnaya">Генеральная уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kvartir/podderzhivayushchaya">Поддерживающая уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kvartir/posle-remonta">Уборка после ремонта</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kvartir/ezhednevnaya">Ежедневная уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kvartir/srochnaya">Срочная уборка</a></li>
                </ul>
              </div>
              <div data-block="item" data-title="Уборка коттеджей">
                <ul class="footer-mobile-answer-menu">
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kottedzhej/generalnaya">Генеральная уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kottedzhej/podderzhivayushchaya">Поддерживающая уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kottedzhej/posle-remonta">Уборка после ремонта</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-kottedzhej/srochnaya">Срочная уборка</a></li>
                </ul>
              </div>
              <div data-block="item" data-title="Мойка окон">
                <ul class="footer-mobile-answer-menu">
                  <li><a data-turbo="false" class="trackable" href="/uslugi/moyka-okon/vitrin">Мойка витрин</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/moyka-okon/fasadov">Мойка фасадов</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/moyka-okon/alpinistami">Мойка альпинистами</a></li>
                </ul>
              </div>
              <div data-block="item" data-title="Уборка офисов">
                <ul class="footer-mobile-answer-menu">
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-ofisov/ezhednevnaya">Ежедневная уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-ofisov/podderzhivayushchaya">Поддерживающая уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-ofisov/generalnaya">Генеральная уборка</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/uborka-ofisov/posle-remonta">Уборка после ремонта</a></li>
                </ul>
              </div>
              <div data-block="item" data-title="Химчистка">
                <ul class="footer-mobile-answer-menu">
                  <li><a data-turbo="false" class="trackable" href="/uslugi/himchistka/s-vyezdom">Химчистка с выездом</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/himchistka/myagkoj-mebeli">Химчистка мягкой мебели</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/himchistka/shtor">Химчистка штор</a></li>
                  <li><a data-turbo="false" class="trackable" href="/uslugi/himchistka/kovrov">Химчистка ковров</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="cell grid-x footer__interlinks">
          <div class="cell grid-x headline">Другие услуги</div>
          <div class="cell grid-x medium-5">
            <ul class="menu vertical">
              <li><a data-turbo="false" href="/uslugi/uborka-kvartir/odnokomnatnoj">Уборка однокомнатной квартиры</a></li>
              <li><a data-turbo="false" href="/uslugi/uborka-kvartir/dvuhkomnatnoj">Уборка двухкомнатной квартиры</a></li>
            </ul>
          </div>
          <div class="cell grid-x medium-4">
            <ul class="menu vertical">
              <li><a data-turbo="false" href="/uslugi/uborka-kvartir/trekhkomnatnoj">Уборка трехкомнатной квартиры</a></li>
              <li><a data-turbo="false" href="/uslugi/uborka-kottedzhej/chastnyh-domov">Уборка домов</a></li>
            </ul>
          </div>
          <div class="cell grid-x medium-3">
            <ul class="menu vertical">
              <li><a data-turbo="false" href="/uslugi/uborka-kottedzhej/taunhausov">Уборка таунхаусов</a></li>
            </ul>
          </div>
        </div>
        <div class="cell grid-x footer__lower-footer">
          <div class="cell grid-x medium-shrink lower-menu">
            <ul class="menu">
              <li><a data-turbo="false" class="trackable" href="/kontakty">Контакты</a></li>
              <li><a data-turbo="false" class="trackable" href="/privacy">Политика конфиденциальности</a></li>
              <li><a data-turbo="false" class="trackable" href="/terms">Условия использования</a></li>
              <li><a data-turbo="false" class="trackable" href="/sitemap">Карта сайта</a></li>
            </ul>
          </div>
          <div class="cell grid-x medium-auto align-right copy">2021 my-cleaning. Все права защищены.</div>
        </div>
        <div class="cell footer__term_description"><p>Данный сайт носит информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями Статьи 437 ГК РФ.</p></div>
      </div>
    </div>
  </footer>
`;
