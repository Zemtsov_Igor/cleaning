import styled from 'styled-components';

export const FooterComponent:any = styled('footer')`
  display: block;
  position: relative;
  padding: 3rem 0;
  background-color: #1D1D1D;
  color: #888;
  font-size: 0.875rem;
  z-index: 9999;

  .menu {
    padding: 0;
    margin: 0;
    list-style: none;
    position: relative;
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;

    &.vertical {
      flex-wrap: nowrap;
      flex-direction: column;
    }

    li {
      font-size: inherit;

      &.menu-text {
        padding: .4rem 0;
        font-weight: bold;
        line-height: 1;
        color: inherit;
        text-transform: uppercase;
      }

      a {
        line-height: 1;
        text-decoration: none;
        display: block;
        margin-bottom: 0;
        padding: .4rem 0;
        color: #888888;
      }
    }
  }

  .footer__navigation {
    margin-bottom: 2rem;

    nav {
      &.hide-for-small {
        display: none;
      }

      li {
        a {
          color: #fff;
        }
      }
    }
  }

  .footer__social {
    margin-bottom: 2rem;

    a {
      font-size: 1.125rem;
      color: #888;
      height: 2.5rem;
      width: 2.5rem;
      display: inline-block;
      border-radius: 1.75rem;
      border: 1px solid #888;
      line-height: 2.5rem;
      text-align: center;
      margin-right: .85rem;

      i {
        &.fa-vk {
          &:before {
            content: "\f189";
          }
        }

        &.fa-facebook {
          &:before {
            content: "\f099";
          }
        }

        &.fa-instagram {
          &:before {
            content: "\f16d";
          }
        }
      }
    }
  }

  .footer__interlinks {
    padding-top: 1.875rem;
    margin-bottom: 1.875rem;
    align-items: baseline;
    border-top: 1px solid;

    .headline {
      color: #fff;
      margin-bottom: 1rem;
      font-weight: bold;
    }
  }

  .footer__lower-footer {
    padding-top: 1rem;
    align-items: baseline;
    border-top: 1px solid;

    .lower-menu {
      justify-content: center;

      li {
        a {
          padding: 0 1rem 0 0;
          color: #888;
        }
      }
    }

    .copy {
      color: #fff;
    }
  }

  .footer__term_description {
    color: #9cabb5;
    text-align: center;
    font-size: .75rem;
    font-weight: 300;
    line-height: .9375rem;
    margin-top: 2rem;
  }

  .footer-mobile-menu-col {
    padding: 0;
    margin: 0;

    &::marker {
      font-size: 0;
    }

    .footer-mobile-menu {
      background: 0;
      border: 0;
      cursor: pointer;
      font-size: 1.25rem;
      margin-bottom: 1rem;
      border-bottom: 1px solid #888;
      justify-content: space-between;
      flex-wrap: nowrap;
      outline: 0;
      padding-right: 0;
      margin-top: 0;

      .footer-mobile-menu_wrap {
        padding-right: 0;
        justify-content: space-between;
        flex-wrap: nowrap;
      }

      .footer-mobile {
        padding-bottom: 1rem;
        text-align: left;
        font-size: .8em;
        font-weight: 700;
      }

      .icon {
        position: relative;
        width: 1rem;
        height: 1rem;
        cursor: pointer;

        &:before, :after {
          content: "";
          position: absolute;
          background-color: #D1D1D1;
        }

        &:before {
          visibility: hidden;
          top: 0;
          left: 50%;
          width: 0.125rem;
          height: 100%;
          margin-left: -1px;
          transition: all 0.5s ease-in-out;
        }

        &:after {
          top: 50%;
          left: 0;
          width: 100%;
          height: 0.125rem;
          margin-top: -1px;
        }
      }
    }

    .footer-mobile-answer {
      height: auto;
      overflow: hidden;
      font-size: initial;
      padding-left: 1rem;
      margin-bottom: 1rem;
      padding-bottom: 1em;
      border-bottom: 1px solid #888;
      transition: all 0.5s ease-in-out;

      .footer-mobile-answer-menu {
        li {
          font-size: .85em;
          line-height: 2em;
          vertical-align: middle;
          list-style-type: none;

          a {
            color: #fff;
          }
        }
      }

      p {
        margin-bottom: 0;
        font-size: inherit;
        line-height: 1.6;
        text-rendering: optimizeLegibility;
      }
    }

    &:first-child {
      .footer-mobile-menu {
        border-top: 1px solid #888;
        padding-top: .6em;
      }
    }

    &:last-child {
      .footer-mobile-menu {
        margin-bottom: 1rem;
      }
    }

    &.-hidden {
      &:last-child {
        .footer-mobile-menu {
          margin-bottom: 0;
        }
      }

      .icon {
        &:before {
          visibility: visible;
        }
      }

      .footer-mobile-answer {
        height: 0;
        margin-bottom: 0;
        padding-bottom: 0;
        border-bottom: 0;
      }
    }
    
    
  }

  @media only screen and (max-width: 64.0625em) {
    .footer__social {
      display: block;
      text-align: center;
    }

    .footer__interlinks {
      border-top: 0;
    }
  }

  @media only screen and (max-width: 40.0625em) {
    .footer__lower-footer {
      flex-direction: column-reverse;

      div {
        margin-bottom: 1rem;
        justify-content: center;
      }

      li {
        margin-bottom: 1rem;
      }

    }
  }

  @media only screen and (min-width: 40.0625em) {

  }

  @media only screen and (min-width: 64.0625em) {
    .footer__navigation {
      nav {
        &.hide-for-small {
          display: flex;
        }
      }

      .show-for-small {
        display: none
      }
    }
  }

  @media only screen and (min-width: 80em) {

  }
`;
