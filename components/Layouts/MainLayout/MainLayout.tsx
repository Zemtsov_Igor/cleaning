export default function MainLayout(props:any) {
  return (
    <>
      <div id="mobileMenu" className="off-canvas position-right is-transition-push is-closed">
        <div className="is-drilldown">
          <ul
            className="mobile-nav-menu menu vertical drilldown"
            role="menubar"
          >
            <li className="nav-link" role="menuitem">
              <a href="/uslugi">Все услуги</a>
            </li>
            <li className="nav-link is-drilldown-submenu-parent" role="menuitem">
              <a tabIndex={0} >Категории услуг</a>
              <ul className="menu vertical nested submenu is-drilldown-submenu drilldown-submenu-cover-previous" role="group">
                <li className="drilldown-back">
                  <a className="back-title">Назад</a>
                </li>
                <li className="nav-link is-drilldown-submenu-parent" role="menuitem" >
                  <a tabIndex={0}>Уборка квартир</a>
                  <ul className="menu vertical nested submenu is-drilldown-submenu drilldown-submenu-cover-previous" role="group">
                    <li className="drilldown-back">
                      <a className="back-title">Категории услуг</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kvartir">Уборка квартир</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kvartir/generalnaya">Генеральная уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kvartir/podderzhivayushchaya">Поддерживающая уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kvartir/posle-remonta">Уборка после ремонта</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kvartir/ezhednevnaya">Ежедневная уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kvartir/srochnaya">Срочная уборка</a>
                    </li>
                  </ul>
                </li>
                <li className="nav-link is-drilldown-submenu-parent" role="menuitem" >
                  <a tabIndex={0}>Уборка коттеджей</a>
                  <ul className="menu vertical nested submenu is-drilldown-submenu drilldown-submenu-cover-previous" role="group">
                    <li className="drilldown-back">
                      <a className="back-title">Категории услуг</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kottedzhej">Уборка коттеджей</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kottedzhej/generalnaya">Генеральная уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kottedzhej/podderzhivayushchaya">Поддерживающая уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kottedzhej/posle-remonta">Уборка после ремонта</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-kottedzhej/srochnaya">Срочная уборка</a>
                    </li>
                  </ul>
                </li>
                <li className="nav-link is-drilldown-submenu-parent" role="menuitem" >
                  <a tabIndex={0}>Уборка офисов</a>
                  <ul className="menu vertical nested submenu is-drilldown-submenu drilldown-submenu-cover-previous" role="group">
                    <li className="drilldown-back">
                      <a className="back-title">Категории услуг</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-ofisov">Уборка офисов</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-ofisov/ezhednevnaya">Ежедневная уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-ofisov/podderzhivayushchaya">Поддерживающая уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-ofisov/generalnaya">Генеральная уборка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/uborka-ofisov/posle-remonta">Уборка после ремонта</a>
                    </li>
                  </ul>
                </li>
                <li className="nav-link is-drilldown-submenu-parent" role="menuitem" >
                  <a tabIndex={0}>Химчистка</a>
                  <ul id="dryCleaning" className="menu vertical nested submenu is-drilldown-submenu drilldown-submenu-cover-previous" role="group">
                    <li className="drilldown-back">
                      <a className="back-title">Категории услуг</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/himchistka">Химчистка</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/himchistka/s-vyezdom">Химчистка с выездом</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/himchistka/myagkoj-mebeli">Химчистка мягкой мебели</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/himchistka/shtor">Химчистка штор</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/himchistka/kovrov">Химчистка ковров</a>
                    </li>
                  </ul>
                </li>
                <li className="nav-link is-drilldown-submenu-parent" role="menuitem" >
                  <a tabIndex={0}>Мойка окон</a>
                  <ul className="menu vertical nested submenu is-drilldown-submenu drilldown-submenu-cover-previous" role="group">
                    <li className="drilldown-back">
                      <a className="back-title">Категории услуг</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/moyka-okon">Мойка окон</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/moyka-okon/vitrin">Мойка витрин</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/moyka-okon/fasadov">Мойка фасадов</a>
                    </li>
                    <li className="nav-link">
                      <a href="/uslugi/moyka-okon/alpinistami">Мойка альпинистами</a>
                    </li>
                  </ul>
                </li>
                <li className="nav-link" role="menuitem">
                  <a href="/uslugi/dezinfekciya">Дезинфекция</a>
                </li>
                <li className="nav-link" role="menuitem">
                  <a className="link-title" href="/uslugi/uborka-pomeshchenij">Уборка помещений</a>
                </li>
              </ul>
            </li>
            <li className="nav__line"></li>
            <li className="nav-link">
              <a href="/kontakty">Контакты</a>
            </li>
            <li className="nav__line"></li>
            <li className="nav-link">
              <a className="nav-link__phone" href="tel:+74958857279">
                +7 (495) 885-72-79
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div
        id="mobileMenuOverlay"
        className="off-canvas-overlay is-overlay-fixed is-closable"
        role="banner"
        tabIndex={0}
      />

      <div
        id="pageContent"
        className="off-canvas-content has-transition-push has-position-right"
        itemScope
        itemType={props.itemtype}
      >
        {props.children}
      </div>
    </>
  );
};
