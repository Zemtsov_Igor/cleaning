import styled from 'styled-components';

export const HeaderComponent:any = styled('header')`
  section {
    padding-bottom: 0;
  }
  .title-bar {
    padding: 0.5rem 0;
    background: #000000;
    color: #ffffff;
    justify-content: flex-start;
    align-items: center;

    .menu-icon {
      position: relative;
      display: inline-block;
      vertical-align: middle;
      width: 20px;
      height: 16px;
      background: none;
      border: 0;
      cursor: pointer;
      margin-left: 0.25rem;
      margin-right: 0.25rem;
      outline: none;

      &:after {
        position: absolute;
        top: 0;
        left: 0;
        display: block;
        width: 100%;
        height: 2px;
        background: #434343;
        box-shadow: 0 7px 0 #434343, 0 14px 0 #434343;
        content: '';
      }
    }
  }

  .top-bar {
    flex-wrap: nowrap;
    justify-content: space-between;
    align-items: center;
    padding: 0 0;
    height: 5rem;
    flex-wrap: wrap;
    background-color: #fff;

    &-title {
      flex: 0 0 auto;
      margin: 0.5rem 1rem 0.5rem 0;
    }

    .input-group-field {
      width: 100%;
      margin-right: 0;
    }

    .top-bar-left {
      min-height: 100%;
    }
    
    .top-bar-right {
      min-height: 100%;
    }

    .top-bar-left, .top-bar-right {
      flex: 0 0 auto;
      max-width: 100%;
    }

    &.stacked-for-xxlarge {
      flex-wrap: wrap;

      .top-bar-left, .top-bar-right {
        flex: 0 0 100%;
        max-width: 100%;
      }
    }

    ul {
      background-color: #e6e6e6;
    }

    input {
      max-width: 200px;
      margin-right: 1rem;

      &.button {
        width: auto;
      }
    }
  }

  .navigation {

    &--collapsed {
      display: flex;
      justify-content: space-between;
      background-color: #fff;

      ul {
        background-color: #fff;
      }
    }

    &--expanded {
      display: none;
      background-color: #fff;

      ul {
        background-color: #fff;
      }
    }

    &__logo {
      width: 8.3125rem;
      height: 3.25rem;

      & .img {
        max-width: 100%;
        max-height: 100%;
        width: 8.3125rem;
        height: 3.25rem;
      }
    }

    a {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 100%;
      color: #434343;
    }

    &-phone {
      outline: 0;
      margin-left: auto;
      margin-right: 1.25rem;

      &__icon {
        &:before {
          content: "";
          position: relative;
          display: block;
          width: 1.25rem;
          height: 1.25rem;
          background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjNDM0MzQzIiBkPSJNNDkzLjQgMjQuNmwtMTA0LTI0Yy0xMS4zLTIuNi0yMi45IDMuMy0yNy41IDEzLjlsLTQ4IDExMmMtNC4yIDkuOC0xLjQgMjEuMyA2LjkgMjhsNjAuNiA0OS42Yy0zNiA3Ni43LTk4LjkgMTQwLjUtMTc3LjIgMTc3LjJsLTQ5LjYtNjAuNmMtNi44LTguMy0xOC4yLTExLjEtMjgtNi45bC0xMTIgNDhDMy45IDM2Ni41LTIgMzc4LjEuNiAzODkuNGwyNCAxMDRDMjcuMSA1MDQuMiAzNi43IDUxMiA0OCA1MTJjMjU2LjEgMCA0NjQtMjA3LjUgNDY0LTQ2NCAwLTExLjItNy43LTIwLjktMTguNi0yMy40eiIvPjwvc3ZnPg==);
          background-repeat: no-repeat;
        }
      }
    }
  }

  .menu {
    padding: 0;
    margin: 0;
    list-style: none;
    position: relative;
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;

    a, .button {
      line-height: 1;
      text-decoration: none;
      display: block;
      padding: 0.7rem 1rem;
      transition: 0.5s;
    }

    .menu-phone {
      padding: 0.7rem 1rem;
      color: #434343;

      &:before {
        content: "";
        position: relative;
        display: inline-block;
        width: 1.25rem;
        height: 1rem;
        margin-right: 4px;
        background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjNDM0MzQzIiBkPSJNNDkzLjQgMjQuNmwtMTA0LTI0Yy0xMS4zLTIuNi0yMi45IDMuMy0yNy41IDEzLjlsLTQ4IDExMmMtNC4yIDkuOC0xLjQgMjEuMyA2LjkgMjhsNjAuNiA0OS42Yy0zNiA3Ni43LTk4LjkgMTQwLjUtMTc3LjIgMTc3LjJsLTQ5LjYtNjAuNmMtNi44LTguMy0xOC4yLTExLjEtMjgtNi45bC0xMTIgNDhDMy45IDM2Ni41LTIgMzc4LjEuNiAzODkuNGwyNCAxMDRDMjcuMSA1MDQuMiAzNi43IDUxMiA0OCA1MTJjMjU2LjEgMCA0NjQtMjA3LjUgNDY0LTQ2NCAwLTExLjItNy43LTIwLjktMTguNi0yMy40eiIvPjwvc3ZnPg==);
        background-repeat: no-repeat;
      }
    }

    &.dropdown {
      height: 4rem;
      align-items: center;

      > li {
        margin-left: 1rem;
        padding: 0.8125em 0;

        &:last-child {
          a {
            padding-right: 0;
          }
        }
      }

      .has-submenu {
        .link {
          &:after {
            margin-left: 4px;
            content: '';
            font-weight: 300;
            -moz-osx-font-smoothing: grayscale;
            -webkit-font-smoothing: antialiased;
            display: inline-block;
            font-style: normal;
            font-variant: normal;
            text-rendering: auto;
            transition: 0.5s;
            position: relative;
            width: 0.5rem;
            height: 0.75rem;
            background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMjAgNTEyIj48cGF0aCBmaWxsPSIjNDM0MzQzIiBkPSJNMTQzIDM1Mi4zTDcgMjE2LjNjLTkuNC05LjQtOS40LTI0LjYgMC0zMy45bDIyLjYtMjIuNmM5LjQtOS40IDI0LjYtOS40IDMzLjkgMGw5Ni40IDk2LjQgOTYuNC05Ni40YzkuNC05LjQgMjQuNi05LjQgMzMuOSAwbDIyLjYgMjIuNmM5LjQgOS40IDkuNCAyNC42IDAgMzMuOWwtMTM2IDEzNmMtOS4yIDkuNC0yNC40IDkuNC0zMy44IDB6Ii8+PC9zdmc+);
            background-repeat: no-repeat;
          }
        }

        &:hover {
          .link {
            &:after {
              transform: rotate(180deg);
            }
          }
        }

        &:hover .services-panel {
          opacity: 1;
          visibility: visible;
          transform: translateY(0px);
          pointer-events: auto;
        }
      }
    }

    .services-panel {
      overflow-y: auto;
      overflow-x: hidden;
      opacity: 0;
      visibility: hidden;
      box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);
      position: absolute;
      background-color: #fff;
      z-index: 2;
      padding-top: 28px;
      padding-bottom: 26px;
      transition: transform .4s ease;
      transform: translateY(-10px);

      &:hover {
        opacity: 1;
        visibility: visible;
      }

      .services-columns {
        -webkit-column-gap: 2em;
        column-gap: 2em;
        font-size: 14px;
        line-height: 2em;
        list-style: none;
        margin-left: 1.25rem;
        margin-right: 1.25rem;

        &.four-columns {
          columns: 4;
        }

        > li {
          -webkit-column-break-inside: avoid;
          break-inside: avoid-column;
          page-break-inside: avoid;
          padding-bottom: 2em;

          ul {
            list-style: none;
            margin-left: 0;
          }

          a {
            color: #667884;
            font-size: 14px;
            line-height: 2em;
            padding: 0;

            &:hover {
              color: #434343;
            }
          }

          .link-title {
            font-size: 14px;
            font-weight: bold;
            color: #036da1;
          }

          &:last-child {
            padding-bottom: 0;
          }
        }
      }

      .button-row {
        text-align: center;
        align-items: center;
        flex-direction: column;

        .button {
          padding: 0;
          width: 217px;
          height: 55px;
          margin-bottom: 0;
          line-height: 3em;
          font-size: 18px;
          background-color: #0BB8E3;
          border-color: #0BB8E3;
          border-radius: 5px;
          color: #ffffff;
          display: inline-block;
          vertical-align: middle;
          text-decoration: none;
          transition: 0.5s;

          &:hover {
            background-color: #036da1;
            border-color: #036da1;
          }
        }
      }
    }

  }
  
  @media print, screen and (min-width: 40em) {
     .top-bar {
      flex-wrap: nowrap;

      .top-bar-left {
        flex: 1 1 auto;
        margin-right: auto;
      }

      .top-bar-right {
        flex: 0 1 auto;
        margin-left: auto;
      }
    }
  }

  @media only screen and (min-width: 64.0625em) {
    .navigation {

      &--collapsed {
        display: none;
      }

      &--expanded {
        display: flex;
      }

      &__logo {
        width: 9.3125rem;
        height: 5rem;
        
        & .img {
          width: 9.3125rem;
          height: 5rem;
        }
      }

      a {
        color: #434343;
      }
    }

    .menu {
      .services-panel {
        width: 53rem;
      }
    }
  }

  @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
    .services-panel {
      margin-left: -9em;
    }
  }
  
`;
