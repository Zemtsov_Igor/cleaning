import React, { Component } from 'react';

import { HeaderComponent } from './HeaderComponent.Styles';

export default class Header extends Component {
  render() {
    return (
      <HeaderComponent>
        <div className="grid-container">
          <div className="navigation">
            {/* Mobiile */}
            <nav className="grid-x align-middle navigation--collapsed title-bar">
              <div className="navigation__logo left">
                <a href="/">
                  <img className="img" src="/static/images/logo.svg" alt="My cleaning" />
                </a>
              </div>
              <div className="navigation-phone">
                <a className="navigation-phone__icon" href="tel:+74958857279" />
              </div>
              <button
                type="button"
                id="mobileMenuBtn"
                className="menu-icon right"
                onClick={() => this.context.toggleOpenMenu()}
              />
            </nav>
            {/* Desctop */}
            <nav className="align-middle stacked-for-medium navigation--expanded top-bar" id="navigation--expanded">
              <div className="navigation__logo show-for-large">
                <a href="/">
                  <img className="img" src="/static/images/logo.svg" alt="My cleaning" />
                </a>
              </div>
              <div className="navigation__left top-bar-left">
                <ul className="menu dropdown" role="menubar">
                  <li className="has-submenu" role="menuitem">
                    <a className="link" href="/uslugi">Все услуги</a>

                    <div className="grid-container services-panel">
                      <div className="grid-x">
                        <div className="cell">
                          <ul className="services-columns four-columns">

                            <li role="menuitem">
                              <a className="link-title" href="/uslugi/uborka-kvartir">Уборка квартир</a>
                              <ul>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kvartir/generalnaya">Генеральная уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kvartir/podderzhivayushchaya">Поддерживающая уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kvartir/posle-remonta">Уборка после ремонта</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kvartir/ezhednevnaya">Ежедневная уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kvartir/srochnaya">Срочная уборка</a>
                                </li>
                              </ul>
                            </li>

                            <li role="menuitem">
                              <a className="link-title" href="/uslugi/uborka-kottedzhej">Уборка коттеджей</a>
                              <ul>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kottedzhej/generalnaya">Генеральная уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kottedzhej/podderzhivayushchaya">Поддерживающая уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kottedzhej/posle-remonta">Уборка после ремонта</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-kottedzhej/srochnaya">Срочная уборка</a>
                                </li>
                              </ul>
                            </li>

                            <li role="menuitem">
                              <a className="link-title" href="/uslugi/moyka-okon">Мойка окон</a>
                              <ul>
                                <li role="menuitem">
                                  <a href="/uslugi/moyka-okon/vitrin">Мойка витрин</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/moyka-okon/fasadov">Мойка фасадов</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/moyka-okon/alpinistami">Мойка альпинистами</a>
                                </li>
                              </ul>
                            </li>

                            <li role="menuitem">
                              <a className="link-title" href="/uslugi/uborka-ofisov">Уборка офисов</a>
                              <ul>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-ofisov/ezhednevnaya">Ежедневная уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-ofisov/podderzhivayushchaya">Поддерживающая уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-ofisov/generalnaya">Генеральная уборка</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/uborka-ofisov/posle-remonta">Уборка после ремонта</a>
                                </li>
                              </ul>
                            </li>

                            <li role="menuitem">
                              <a className="link-title" href="/uslugi/himchistka">Химчистка</a>
                              <ul>
                                <li role="menuitem">
                                  <a href="/uslugi/himchistka/s-vyezdom">Химчистка с выездом</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/himchistka/myagkoj-mebeli">Химчистка мягкой мебели</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/himchistka/shtor">Химчистка штор</a>
                                </li>
                                <li role="menuitem">
                                  <a href="/uslugi/himchistka/kovrov">Химчистка ковров</a>
                                </li>
                              </ul>
                            </li>

                            <li role="menuitem">
                              <a className="link-title" href="/uslugi/dezinfekciya">Дезинфекция</a>
                            </li>

                            <li role="menuitem">
                              <a className="link-title" href="/uslugi/uborka-pomeshchenij">Уборка помещений</a>
                            </li>



                          </ul>
                        </div>
                      </div>

                      <div className="grid-x button-row">
                        <div className="row all-services__button">
                          <a className="button primary trackable" href="/uslugi">
                            Все услуги
                          </a>
                        </div>
                      </div>

                    </div>
                  </li>
                </ul>
              </div>
              <div className="navigation__right top-bar-right">
                <ul className="menu dropdown" role="menubar">
                  <li role="menuitem">
                    <a className="menu-phone" href="tel:+74958857279">
                      +7 (495) 885-72-79
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </HeaderComponent>
    );
  }
};
